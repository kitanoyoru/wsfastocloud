FROM cirrusci/flutter:latest AS build

LABEL maintainer="Alexandr Topilski <support@fastogt.com>"

RUN mkdir /app/
COPY . /app/
WORKDIR /app/

RUN python3 scripts/build_version.py

FROM nginx:latest

ENV USER fastocloud
RUN useradd -m -U -d /home/$USER $USER -s /bin/bash

COPY --from=build /app/nginx/wsfastocloud /etc/nginx/conf.d/default.conf
COPY --from=build /app/build/web /home/$USER/wsfastocloud_build

RUN chown -R fastocloud:fastocloud /home/$USER/wsfastocloud_build && chmod -R 755 /home/$USER/wsfastocloud_build && \
        chown -R fastocloud:fastocloud /var/cache/nginx && \
        chown -R fastocloud:fastocloud /var/log/nginx && \
        chown -R fastocloud:fastocloud /etc/nginx/conf.d
RUN touch /var/run/nginx.pid && chown -R fastocloud:fastocloud /var/run/nginx.pid

USER $USER 

WORKDIR /home/$USER

EXPOSE 80

