# FastoCloud ONE

[![Join the chat at https://discord.com/invite/cnUXsws](https://img.shields.io/discord/584773460585086977?label=discord)](https://discord.com/invite/cnUXsws)

### Description:
UI for FastoCloud Media Services, backend you can find [here](https://gitlab.com/fastogt/gofastocloud_backend)

### Features:
- Upload media files
- Uplaod m3u file
- VODs/Restream/Transcode/External streams
- Realtime statistics
- Streams managment

### Demo servers:
- https://ws.fastocloud.com

### Android version:
- https://play.google.com/store/apps/details?id=com.wsfastocloud

![UI on Web](https://gitlab.com/fastogt/wsfastocloud/raw/main/docs/images/fastocloud_one_web.png) 
