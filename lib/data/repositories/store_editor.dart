import 'package:fastocloud_dart_models/models.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_store_bloc/streams_store_bloc.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/base_page.dart';

class StoreEditor extends IStreamEditor {
  final StreamsStoreBloc fetcher;

  StoreEditor(this.fetcher);

  @override
  Defaults defaults() {
    return fetcher.defaults();
  }

  @override
  Future<bool> add(IStream stream) {
    return fetcher.addStream(stream);
  }

  @override
  Future<bool> edit(IStream stream) {
    return fetcher.editStream(stream);
  }

  @override
  Future<String> probeOutUrl(OutputUrl url) {
    return fetcher.probeOutUrl(url);
  }

  @override
  Future<String> probeInUrl(InputUrl url) {
    return fetcher.probeInUrl(url);
  }

  @override
  Future<bool> uploadM3uFile(StreamType type, bool isSeries, double imageCheckTime,
      final bool skipGroups, final List<String> groups, Map<String, List<int>> data) {
    return fetcher.uploadM3uFile(type, isSeries, imageCheckTime, skipGroups, groups, data);
  }
}
