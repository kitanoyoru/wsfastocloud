import 'dart:convert';

import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:flutter_common/http_response.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/data/services/api/models/upload_m3u.dart';
import 'package:wsfastocloud/domain/repositories/stream_db_repository.dart';

class StreamDBRepositoryImpl implements StreamDBRepository {
  final Fetcher fetcher;

  StreamDBRepositoryImpl(this.fetcher);

  @override
  Future<List<IStream>> getDBStreams() async {
    final response = await fetcher.get('/server/db/stream/list');
    final respData = httpDataResponseFromString(response.body);
    final streamsData = respData!.contentMap()!;
    final streams = <IStream>[];
    for (var streamData in streamsData['streams']) {
      final stream = makeStream(streamData);
      if (stream != null) {
        streams.add(stream);
      }
    }

    final vods = <IStream>[];
    for (var streamData in streamsData['vods']) {
      final stream = makeStream(streamData);
      if (stream != null) {
        vods.add(stream);
      }
    }
    final series = <IStream>[];
    for (var streamData in streamsData['series']) {
      final stream = makeStream(streamData);
      if (stream != null) {
        series.add(stream);
      }
    }

    return streams + vods + series;
  }

  @override
  Future<bool> addStream(IStream stream) async {
    final requestData = stream.toJson();
    final resp = await fetcher.post('/server/db/stream/add', requestData);
    if (resp.statusCode == 200) {
      return true;
    }
    return false;
  }

  @override
  Future<bool> uploadM3uFile(StreamType type, bool isSeries, double imageCheckTime,
      final bool skipGroups, final List<String> groups, Map<String, List<int>> data) async {
    final def = defaults();
    final m3u = UploadM3u(
        type: type,
        streamLogoIcon: def.streamLogoIcon,
        vodTrailerUrl: def.vodTrailerUrl,
        backgroundUrl: def.backgroundUrl,
        imageCheckTime: imageCheckTime,
        skipGroups: skipGroups,
        groups: groups,
        isSeries: isSeries);

    final requestData = m3u.toJson();
    final resp = await fetcher.sendFiles('/server/db/stream/upload_file', data, requestData);
    if (resp.statusCode == 200) {
      return true;
    }
    return false;
  }

  @override
  Future<bool> editStream(IStream stream) async {
    final requestData = stream.toJson();
    final resp = await fetcher.patch('/server/db/stream/edit', requestData);
    if (resp.statusCode == 200) {
      return true;
    }
    return false;
  }

  @override
  Future<String> getTokenUrl(String ip) async {
    final data = {'ip': ip};
    final response = await fetcher.post('/server/stream/token', data);
    final respData = httpDataResponseFromString(response.body);
    if (respData == null) {
      throw 'Fetch error: invalid response format, code: ${response.statusCode}';
    }

    final err = respData.error();
    if (err != null) {
      throw 'Response error: ${err.message}';
    }

    final token = respData.data['data'];
    return token;
  }

  @override
  Future<bool> removeStream(String id) async {
    final resp = await fetcher.delete('/server/db/stream/remove/${id}');
    if (resp.statusCode == 200) {
      return true;
    }
    return false;
  }

  @override
  Future<String> probeOutUrl(OutputUrl url) async {
    final data = {'url': url.toJson()};
    final response = await fetcher.post('/media/probe_out', data);
    final respData = httpDataResponseFromString(response.body);
    if (respData == null) {
      throw 'Fetch error: invalid response format, code: ${response.statusCode}';
    }

    final err = respData.error();
    if (err != null) {
      throw 'Response error: ${err.message}';
    }

    final content = respData.contentMap()!;
    final probeData = content['probe'];
    return json.encode(probeData);
  }

  @override
  Future<String> embedOutput(OutputUrl url) async {
    final resp = await fetcher.get('/server/stream/embed?src=${url.uri}');
    if (resp.statusCode == 200) {
      return resp.body;
    }

    throw 'Wrong response code ${resp.statusCode}';
  }

  @override
  Future<String> probeInUrl(InputUrl url) async {
    final data = {'url': url.toJson()};
    final response = await fetcher.post('/media/probe_in', data);
    final respData = httpDataResponseFromString(response.body);
    if (respData == null) {
      throw 'Fetch error: invalid response format, code: ${response.statusCode}';
    }

    final err = respData.error();
    if (err != null) {
      throw 'Response error: ${err.message}';
    }

    final content = respData.contentMap()!;
    final probeData = content['probe'];
    return json.encode(probeData);
  }

  @override
  Future<void> startStream(String id) {
    return fetcher.get('/media/db/stream/start/${id}');
  }

  @override
  Defaults defaults() {
    return fetcher.defaults();
  }

  @override
  Future<List<IStream>> loadEpisodes() async {
    final response = await fetcher.get('/server/db/stream/episode/list');
    final respData = httpDataResponseFromString(response.body);
    final streamsData = respData!.contentMap()!;

    final series = <IStream>[];
    for (var streamData in streamsData['series']) {
      final stream = makeStream(streamData);
      if (stream != null) {
        series.add(stream);
      }
    }

    return series;
  }

  @override
  Future<List<ServerSeason>> loadSeasons() async {
    final response = await fetcher.get('/server/db/season/list');
    final data = json.decode(response.body);
    final List<ServerSeason> result = [];
    data['data']['seasons'].forEach((s) {
      final res = ServerSeason.fromJson(s);
      result.add(res);
    });
    return result;
  }

  @override
  Future<void> addSeason(ServerSeason season) {
    final resp = fetcher.post('/server/db/season/add', season.toJson());
    return resp;
  }

  @override
  Future<void> editSeason(ServerSeason season) {
    final resp = fetcher.patch('/server/db/season/edit', season.toJson());
    return resp;
  }

  @override
  Future<void> removeSeason(ServerSeason season) {
    final resp = fetcher.delete('/server/db/season/remove/${season.id}');
    return resp;
  }

  @override
  Future<List<ServerSerial>> loadSerials() async {
    final response = await fetcher.get('/server/db/serial/list');
    final data = json.decode(response.body);
    final List<ServerSerial> result = [];
    data['data']['serials'].forEach((s) {
      final res = ServerSerial.fromJson(s);
      result.add(res);
    });
    return result;
  }

  @override
  Future<void> addSerial(ServerSerial serial) {
    final resp = fetcher.post('/server/db/serial/add', serial.toJson());
    return resp;
  }

  @override
  Future<void> editSerial(ServerSerial serial) {
    final resp = fetcher.patch('/server/db/serial/edit', serial.toJson());
    return resp;
  }

  @override
  Future<void> removeSerial(ServerSerial serial) {
    final resp = fetcher.delete('/server/db/serial/remove/${serial.id}');
    return resp;
  }
}
