import 'dart:convert';

import 'package:flutter_common/http_response.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/data/services/api/models/stream_statistics.dart';
import 'package:wsfastocloud/domain/repositories/stream_repository.dart';

class StreamRepositoryImpl implements StreamRepository {
  final Fetcher fetcher;

  StreamRepositoryImpl(this.fetcher);

  @override
  Future<List<StreamStatistics>> getStreamsStatistics() async {
    final response = await fetcher.get('/media/streams_stats');
    final respData = httpDataResponseFromString(response.body);
    final content = respData!.contentMap()!;
    final streamsData = content['statistics'];
    final result = <StreamStatistics>[];

    for (var streamData in streamsData) {
      result.add(StreamStatistics.fromJson(streamData));
    }

    return result;
  }

  @override
  Future<void> stopStream(String id, [bool force = false]) {
    final requestData = {'id': id, 'force': force};

    return fetcher.post('/media/stream/stop', requestData);
  }

  @override
  Future<void> restartStream(String id) {
    final requestData = {'id': id};

    return fetcher.post('/media/stream/restart', requestData);
  }

  @override
  Future<void> getLogStream(String id) {
    return fetcher.launchUrlEx('/media/stream/logs/${id}');
  }

  @override
  Future<void> changeSource(String id, int index) {
    final requestData = {'id': id, 'channel_id': index};

    return fetcher.post('/media/stream/change_source', requestData);
  }

  @override
  Future<void> getPipeLineStream(String id) {
    return fetcher.launchUrlEx('/media/stream/pipeline/${id}');
  }

  @override
  Future<String> getConfigStreamString(String id) async {
    final probeData = await getConfigStreamMap(id);
    return json.encode(probeData);
  }

  @override
  Future<Map<String, dynamic>> getConfigStreamMap(String id) async {
    final response = await fetcher.get('/media/stream/config/${id}');

    final respData = httpDataResponseFromString(response.body);
    if (respData == null) {
      throw 'Fetch error: invalid response format, code: ${response.statusCode}';
    }

    final err = respData.error();
    if (err != null) {
      throw 'Response error: ${err.message}';
    }

    final content = respData.contentMap()!;
    final probeData = content['config'];
    return probeData;
  }
}
