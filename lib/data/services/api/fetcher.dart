import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:wsfastocloud/presenter/connection_bloc/connection_bloc.dart';

class Defaults {
  final String streamLogoIcon;
  final String vodTrailerUrl;
  final String backgroundUrl;

  Defaults(this.streamLogoIcon, this.vodTrailerUrl, this.backgroundUrl);
}

class Fetcher {
  final ConnectionBloc connectionBloc;
  late final StreamSubscription _connectionSubscription;
  String? _backendServerUrl;
  String? _authorization;

  Fetcher(this.connectionBloc) {
    _connectionSubscription = connectionBloc.stream.listen((state) {
      {
        final info = state.info;

        if (info == null) {
          _backendServerUrl = null;
          _authorization = null;
        } else {
          _setBackendUrl(info.restApiUrl);
          _setAuthorizationHeader(info.encodedAuth);
        }
      }
    });
  }

  Defaults defaults() {
    //final unk = _generateBackEndEndpoint('/install/assets/unknown_channel.png');
    return Defaults('https://api.fastocloud.com/install/assets/unknown_channel.png',
        INVALID_TRAILER_URL, DEFAULT_BACKGROUND_URL);
  }

  String? get backendServerUrl => _backendServerUrl;

  void _setBackendUrl(String url) {
    _backendServerUrl = url;
  }

  void _setAuthorizationHeader(String? encodedAuth) {
    if (encodedAuth != null) {
      _authorization = 'Basic ${encodedAuth}';
    }
  }

  Future<http.Response> getMediaServiceStatistics() async {
    return get('/media/stats');
  }

  Future<http.Response> getServerStatistics() async {
    return get('/server/stats');
  }

  Future<http.Response> getServiceConfig() async {
    return get('/server/config');
  }

  Future<http.Response> getServiceInfo() async {
    return get('/server/info');
  }

  Uri getBackendEndpoint(String path) {
    return _generateBackEndEndpoint(path);
  }

  Future<http.Response> get(String endpoint) async {
    assert(_backendServerUrl != null); // TODO: Exception

    final url = _generateBackEndEndpoint(endpoint);

    return http.get(url, headers: _getJsonHeaders());
  }

  Future<http.Response> delete(String endpoint) async {
    assert(_backendServerUrl != null); // TODO: Exception

    final url = _generateBackEndEndpoint(endpoint);

    return http.delete(url, headers: _getJsonHeaders());
  }

  Future<http.Response> patch(String endpoint, Map<String, dynamic> data) async {
    assert(_backendServerUrl != null); // TODO: Exception

    final url = _generateBackEndEndpoint(endpoint);

    final body = json.encode(data);
    return http.patch(url, headers: _getJsonHeaders(), body: body);
  }

  Future<bool> launchUrlEx(String endpoint) async {
    assert(_backendServerUrl != null); // TODO: Exception

    final url = _generateBackEndEndpoint(endpoint);

    return launchUrl(url, webViewConfiguration: WebViewConfiguration(headers: _getJsonHeaders()));
  }

  Future<http.Response> sendFiles(
      String path, Map<String, List<int>> data, Map<String, dynamic> fields) async {
    final http.MultipartRequest request =
        http.MultipartRequest('POST', _generateBackEndEndpoint(path));
    if (_authorization != null) {
      request.headers[HttpHeaders.authorizationHeader] = _authorization!;
    }

    data.forEach((key, data) {
      final mf = http.MultipartFile.fromBytes('file', data,
          contentType: MediaType('application', 'octet-stream'), filename: key);
      request.files.add(mf);
    });

    final body = json.encode(fields);
    request.fields.addAll({'params': body});
    final sent = await request.send();
    return http.Response.fromStream(sent);
  }

  Future<http.Response> post(String endpoint, Map<String, dynamic> data) async {
    assert(_backendServerUrl != null); // TODO: Exception

    final url = _generateBackEndEndpoint(endpoint);

    final body = json.encode(data);
    return http.post(url, headers: _getJsonHeaders(), body: body);
  }

  Map<String, String> _getJsonHeaders() {
    if (_authorization == null) {
      return {'content-type': 'application/json', 'accept': 'application/json'};
    }
    return {
      'content-type': 'application/json',
      'accept': 'application/json',
      'authorization': _authorization!,
    };
  }

  // private
  Uri _generateBackEndEndpoint(String path) {
    return Uri.parse('$_backendServerUrl$path');
  }

  void dispose() {
    _connectionSubscription.cancel();
  }
}
