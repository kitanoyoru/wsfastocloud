import 'package:fastocloud_dart_models/models.dart';

class ConnectionInfo {
  final String originalUrl;
  final String? encodedAuth;

  ConnectionInfo(this.originalUrl, this.encodedAuth);

  factory ConnectionInfo.fromUserInput(WSServer server) {
    return ConnectionInfo(server.url, server.encodedAuth());
  }

  String get socketUrl => _generateUrlWithProtocol('ws', 'wss');

  String get restApiUrl => _generateUrlWithProtocol('http', 'https');

  String _generateUrlWithProtocol(String unsecure, String secure) {
    final uri = Uri.parse(originalUrl);
    var host = uri.host;

    if (uri.hasPort) {
      host += ':${uri.port}';
    }

    final path = uri.pathSegments.where((e) => e.isNotEmpty).join('/');

    if (path.isNotEmpty) {
      host += '/$path';
    }

    final useSecureProtocols = uri.isScheme('https') || uri.isScheme('wss');

    return '${useSecureProtocols ? secure : unsecure}://$host';
  }
}
