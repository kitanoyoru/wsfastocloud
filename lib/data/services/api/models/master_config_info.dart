import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';

class IpAddress {
  static const _IP = 'ip';

  String ip;

  IpAddress({required this.ip});

  factory IpAddress.fromJson(Map<String, dynamic> json) {
    return IpAddress(ip: json[_IP]);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {_IP: ip};
    return data;
  }
}

class HostAndAlias {
  static const _HOST = 'host';
  static const _ALIAS = 'alias';

  HostAndPort host;
  String? alias;

  HostAndAlias.createDefault()
      : host = HostAndPort.createLocalHostV4(port: 6317),
        alias = '0.0.0.0';

  HostAndAlias({required this.host, required this.alias});

  HostAndAlias copy() {
    return HostAndAlias(host: host, alias: alias);
  }

  factory HostAndAlias.fromJson(Map<String, dynamic> json) {
    final host = HostAndPort.fromString(json[_HOST]);
    String? alias;
    if (json.containsKey(_ALIAS)) {
      alias = json[_ALIAS];
    }
    return HostAndAlias(host: host, alias: alias);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data[_HOST] = this.host.toString();
    if (alias != null) {
      data[_ALIAS] = this.alias;
    }
    return data;
  }
}

class Node {
  static const _HOST = 'host';
  static const _KEY = 'key';

  HostAndPort host;
  String key;

  Node({required this.host, required this.key});

  Node.createDefault()
      : host = HostAndPort.createLocalHostV4(port: 6317),
        key =
            '0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000';

  factory Node.fromJson(Map<String, dynamic> json) {
    return Node(host: HostAndPort.fromString(json[_HOST]), key: json[_KEY]);
  }

  Map<String, dynamic> toJson() {
    return {_HOST: host.toString(), _KEY: key};
  }
}

class Webrtc {
  static const _STUN = 'stun';
  static const _TURN = 'turn';

  String stun;
  String turn;

  Webrtc({required this.stun, required this.turn});

  Webrtc.createDefault()
      : stun = 'stun://stun.fastocloud.com:34789',
        turn = 'turn://fastocloud:fastocloud@turn.fastocloud.com:53499';

  factory Webrtc.fromJson(Map<String, dynamic> json) {
    return Webrtc(stun: json[_STUN], turn: json[_TURN]);
  }

  Map<String, dynamic> toJson() {
    return {_STUN: stun, _TURN: turn};
  }

  Webrtc copy() {
    return Webrtc(stun: stun, turn: turn);
  }
}

class Auth {
  static const LOGIN_FIELD = 'login';
  static const PASSWORD_FIELD = 'password';

  String login;
  String password;

  Auth({required this.login, required this.password});

  Auth.createDefault()
      : login = 'fastocloud',
        password = 'fastocloud';

  factory Auth.fromString(String data) {
    final split = data.split(':');
    return Auth(login: split[0], password: split[1]);
  }

  factory Auth.fromJson(Map<String, dynamic> json) {
    return Auth(login: json[LOGIN_FIELD], password: json[PASSWORD_FIELD]);
  }

  Map<String, dynamic> toJson() {
    return {LOGIN_FIELD: login, PASSWORD_FIELD: password};
  }

  String toString() {
    return '$login:$password';
  }

  Auth copy() {
    return Auth(login: login, password: password);
  }
}

class TypeVodService {
  static const int _TMDB = 1;

  final int _value;

  const TypeVodService._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    assert(_value == 1);
    return 'TMDB';
  }

  factory TypeVodService.fromInt(int type) {
    if (type == _TMDB) {
      return TypeVodService.TMDB;
    }

    throw 'Unknown Vod service type: $type';
  }

  static List<TypeVodService> get values => [TMDB];

  static const TypeVodService TMDB = TypeVodService._(_TMDB);
}

class TmdbSettings {
  static const KEY = 'key';

  String key;

  TmdbSettings(this.key);

  Map<String, dynamic> toJson() {
    return {KEY: key};
  }

  factory TmdbSettings.fromJson(Map<String, dynamic> json) {
    return TmdbSettings(json[KEY]);
  }
}

class VodInfoSettings {
  static const _TYPE = 'type';
  static const _TMDB = 'tmdb';

  TypeVodService service;
  TmdbSettings tmdb;

  VodInfoSettings({required this.service, required this.tmdb});

  VodInfoSettings.createDefault()
      : service = TypeVodService.TMDB,
        tmdb = TmdbSettings('06dc198084fe47aa0dfd8986a665ec76');

  Map<String, dynamic> toJson() {
    return {
      _TYPE: service.toInt(),
      _TMDB: tmdb.toJson(),
    };
  }

  factory VodInfoSettings.fromJson(Map<String, dynamic> json) {
    final service = TypeVodService.fromInt(json[_TYPE]);
    final tmdb = TmdbSettings.fromJson(json[_TMDB]);
    return VodInfoSettings(service: service, tmdb: tmdb);
  }
}

class AuthContentType {
  static const _JWT_CONTENT_AUTH = 0;
  static const _BASE_CONTENT_AUTH = 1;

  final int _value;

  const AuthContentType._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == _JWT_CONTENT_AUTH) {
      return 'JWT';
    }

    assert(_value == _BASE_CONTENT_AUTH);
    return 'Base';
  }

  factory AuthContentType.fromInt(int type) {
    if (type == _JWT_CONTENT_AUTH) {
      return AuthContentType.JWT;
    } else if (type == _BASE_CONTENT_AUTH) {
      return AuthContentType.BASE;
    }

    throw 'Unknown Auth content type: $type';
  }

  static List<AuthContentType> get values => [JWT, BASE];

  static const AuthContentType JWT = AuthContentType._(_JWT_CONTENT_AUTH);
  static const AuthContentType BASE = AuthContentType._(_BASE_CONTENT_AUTH);
}

class MasterConfigInfo {
  static const _HOST = 'host';
  static const _HLS_HOST = 'hls_host';
  static const _VODS_HOST = 'vods_host';
  static const _CODS_HOST = 'cods_host';
  static const _PREFERRED_URL_SCHEME = 'preferred_url_scheme'; // enum (http, https)
  static const _BLACK_LIST = 'blacklist';
  static const _NODE = 'node';
  static const _LOG_LEVEL = 'log_level'; // enum (TRACE, DEBUG, INFO, WARNING, ERROR, FATAL, PANIC)
  static const _LOG_PATH = 'log_path';
  static const _CORS = 'cors';
  static const _AUTH = 'auth';
  static const _MAX_UPLOAD_FILE_SIZE = 'max_upload_file_size';
  static const _WEBRTC = 'webrtc';
  static const _AUTH_CONTENT = 'auth_content';
  static const _VOD_INFO = 'vod_info';
  static const _STREAM_NOTIFICATION = 'alarm';

  HostAndPort host;
  HostAndAlias hlsHost;
  HostAndAlias vodsHost;
  HostAndAlias codsHost;
  String preferredUrlScheme;

  List<IpAddress> blacklist;
  Node? node;
  String logLevel;
  String logPath;
  bool cors;
  Auth? auth;
  INotification? streamNotification;
  int maxUploadFileSize;
  Webrtc? webrtc;
  AuthContentType authContent;
  VodInfoSettings? vodInfo;

  MasterConfigInfo(
      {required this.host,
      required this.hlsHost,
      required this.vodsHost,
      required this.codsHost,
      required this.preferredUrlScheme,
      required this.blacklist,
      this.node,
      required this.logLevel,
      required this.logPath,
      required this.cors,
      this.auth,
      this.streamNotification,
      required this.maxUploadFileSize,
      this.webrtc,
      required this.authContent,
      this.vodInfo});

  factory MasterConfigInfo.fromJson(Map<String, dynamic> json) {
    Webrtc? webrtc;
    if (json.containsKey(_WEBRTC)) {
      webrtc = Webrtc.fromJson(json[_WEBRTC]);
    }
    Auth? auth;
    if (json.containsKey(_AUTH)) {
      auth = Auth.fromString(json[_AUTH]);
    }
    INotification? streamNotification;
    if (json.containsKey(_STREAM_NOTIFICATION)) {
      streamNotification = INotification.fromJson(json[_STREAM_NOTIFICATION]);
    }
    List<IpAddress> black = [];
    json[_BLACK_LIST].forEach((b) {
      black.add(IpAddress.fromJson(b));
    });
    Node? node;
    if (json.containsKey(_NODE)) {
      node = Node.fromJson(json[_NODE]);
    }
    VodInfoSettings? vodInfo;
    if (json.containsKey(_VOD_INFO)) {
      vodInfo = VodInfoSettings.fromJson(json[_VOD_INFO]);
    }
    final host = HostAndPort.fromString(json[_HOST]);
    final hlsHost = HostAndAlias.fromJson(json[_HLS_HOST]);
    final vodsHost = HostAndAlias.fromJson(json[_VODS_HOST]);
    final codsHost = HostAndAlias.fromJson(json[_CODS_HOST]);
    final upload = json[_MAX_UPLOAD_FILE_SIZE];
    final cors = json[_CORS];
    final path = json[_LOG_PATH];
    final authContent = json[_AUTH_CONTENT];
    return MasterConfigInfo(
      host: host,
      hlsHost: hlsHost,
      vodsHost: vodsHost,
      codsHost: codsHost,
      preferredUrlScheme: json[_PREFERRED_URL_SCHEME],
      blacklist: black,
      node: node,
      logLevel: json[_LOG_LEVEL],
      logPath: path,
      cors: cors,
      auth: auth,
      streamNotification: streamNotification,
      maxUploadFileSize: upload,
      webrtc: webrtc,
      authContent: AuthContentType.fromInt(authContent),
      vodInfo: vodInfo,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data[_HOST] = host.toString();
    data[_HLS_HOST] = hlsHost.toJson();
    data[_VODS_HOST] = vodsHost.toJson();
    data[_CODS_HOST] = codsHost.toJson();
    data[_PREFERRED_URL_SCHEME] = preferredUrlScheme;
    List<Map<String, dynamic>> black = [];
    blacklist.forEach((element) {
      black.add(element.toJson());
    });
    data[_BLACK_LIST] = black;
    if (node != null) {
      data[_NODE] = node!.toJson();
    }
    data[_LOG_LEVEL] = logLevel;
    data[_LOG_PATH] = logPath;
    data[_CORS] = cors;
    if (auth != null) {
      data[_AUTH] = auth!.toString();
    }
    if (streamNotification != null) {
      data[_STREAM_NOTIFICATION] = streamNotification!.toJson();
    }
    data[_MAX_UPLOAD_FILE_SIZE] = maxUploadFileSize;
    if (webrtc != null) {
      data[_WEBRTC] = webrtc!.toJson();
    }
    data[_AUTH_CONTENT] = authContent.toInt();
    if (vodInfo != null) {
      data[_VOD_INFO] = vodInfo!.toJson();
    }
    return data;
  }

  MasterConfigInfo copy() {
    return MasterConfigInfo(
        host: host.copy(),
        hlsHost: hlsHost.copy(),
        vodsHost: vodsHost.copy(),
        codsHost: codsHost.copy(),
        preferredUrlScheme: preferredUrlScheme,
        blacklist: blacklist,
        node: node,
        logLevel: logLevel,
        logPath: logPath,
        cors: cors,
        webrtc: webrtc,
        auth: auth,
        streamNotification: streamNotification,
        authContent: authContent,
        vodInfo: vodInfo,
        maxUploadFileSize: maxUploadFileSize);
  }
}
