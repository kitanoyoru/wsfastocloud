class ServerInfo {
  static const _HLS_HOST_FIELD = 'hls_host';
  static const _VODS_HOST_FIELD = 'vods_host';
  static const _CODS_HOST_FIELD = 'cods_host';

  String hlsHost;
  String vodsHost;
  String codsHost;

  ServerInfo({required this.hlsHost, required this.vodsHost, required this.codsHost});

  factory ServerInfo.fromJson(Map<String, dynamic> json) {
    return ServerInfo(
        hlsHost: json[_HLS_HOST_FIELD],
        vodsHost: json[_VODS_HOST_FIELD],
        codsHost: json[_CODS_HOST_FIELD]);
  }
}
