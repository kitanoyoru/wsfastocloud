import 'package:fastotv_dart/commands_info/client_info.dart';

class ServerStatistics {
  static const _BANDWIDTH_IN_FIELD = 'bandwidth_in';
  static const _BANDWIDTH_OUT_FIELD = 'bandwidth_out';
  static const _PROJECT_FIELD = 'project';
  static const _VERSION_FIELD = 'version';
  static const _TOTAL_BYTES_IN_FIELD = 'total_bytes_in';
  static const _TOTAL_BYTES_OUT_FIELD = 'total_bytes_out';

  double? cpu;
  double? gpu;
  int? memoryTotal;
  int? memoryFree;
  int? hddTotal;
  int? uptime;
  int? hddFree;
  int bandwidthIn;
  int bandwidthOut;
  int? timestamp;
  OperationSystem? os;
  int? expirationTime;
  String? version;
  String? project;
  int totalBytesIn;
  int totalBytesOut;

  ServerStatistics(
      {this.cpu,
      this.gpu,
      this.memoryTotal,
      this.memoryFree,
      this.hddTotal,
      this.hddFree,
      required this.bandwidthIn,
      required this.bandwidthOut,
      required this.totalBytesIn,
      required this.totalBytesOut,
      this.uptime,
      this.timestamp,
      this.os,
      this.expirationTime,
      this.version,
      this.project});

  ServerStatistics.createInit()
      : cpu = 0,
        gpu = 0,
        memoryTotal = 0,
        memoryFree = 0,
        uptime = 0,
        hddTotal = 0,
        timestamp = 0,
        hddFree = 0,
        totalBytesIn = 0,
        totalBytesOut = 0,
        bandwidthIn = 0,
        bandwidthOut = 0,
        expirationTime = 0,
        version = null,
        project = null,
        os = null;

  factory ServerStatistics.fromJson(Map<String, dynamic> json) {
    final cpu = (json['cpu'] as num).toDouble();
    final gpu = (json['gpu'] as num).toDouble();
    final memoryTotal = json['memory_total'];
    final memoryFree = json['memory_free'];
    final hddTotal = json['hdd_total'];
    final hddFree = json['hdd_free'];
    final uptime = json['uptime'];
    final timestamp = json['timestamp'];
    final os = json['os'] != null ? OperationSystem.fromJson(json['os']) : null;
    final expirationTime = json['expiration_time'];
    final bandwidthIn = json[_BANDWIDTH_IN_FIELD];
    final bandwidthOut = json[_BANDWIDTH_OUT_FIELD];
    final totalBytesIn = json[_TOTAL_BYTES_IN_FIELD];
    final totalBytesOut = json[_TOTAL_BYTES_OUT_FIELD];
    String? version; // from media available on active state
    if (json[_VERSION_FIELD] != null) {
      version = json[_VERSION_FIELD];
    }
    String? project; // from media available on active state
    if (json[_PROJECT_FIELD] != null) {
      project = json[_PROJECT_FIELD];
    }

    return ServerStatistics(
        gpu: gpu,
        cpu: cpu,
        memoryTotal: memoryTotal,
        memoryFree: memoryFree,
        hddTotal: hddTotal,
        hddFree: hddFree,
        bandwidthIn: bandwidthIn,
        bandwidthOut: bandwidthOut,
        totalBytesIn: totalBytesIn,
        totalBytesOut: totalBytesOut,
        uptime: uptime,
        timestamp: timestamp,
        version: version,
        project: project,
        expirationTime: expirationTime,
        os: os);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cpu'] = this.cpu ?? 0;
    data['gpu'] = this.gpu ?? 0;
    data['memory_total'] = this.memoryTotal ?? 0;
    data['memory_free'] = this.memoryFree ?? 0;
    data['hdd_total'] = this.hddTotal ?? 0;
    data['hdd_free'] = this.hddFree ?? 0;
    data['uptime'] = this.uptime;
    data['timestamp'] = this.timestamp;
    if (this.os != null) {
      data['os'] = this.os!.toJson();
    }
    data['expiration_time'] = this.expirationTime;
    data[_VERSION_FIELD] = this.version;
    data[_PROJECT_FIELD] = this.project;
    data[_BANDWIDTH_IN_FIELD] = this.bandwidthIn;
    data[_BANDWIDTH_OUT_FIELD] = this.bandwidthOut;
    data[_TOTAL_BYTES_IN_FIELD] = this.totalBytesIn;
    data[_TOTAL_BYTES_OUT_FIELD] = this.totalBytesOut;
    return data;
  }
}
