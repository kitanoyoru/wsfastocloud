import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';

class UploadM3u {
  static const _TYPE_FIELD = 'type';
  static const _STREAM_LOGO_URL_FIELD = 'stream_logo_url';
  static const _TRAILER_URL_FIELD = 'trailer_url';
  static const _BACKGROUND_URL_FIELD = 'background_url';
  static const _IMAGE_TIME_TO_CHECK_FIELD = 'image_time_to_check';
  static const _SKIP_GROUPS_FIELD = 'skip_groups';
  static const _GROUPS_FIELD = 'groups';
  static const _IS_SERIES_FIELD = 'is_series';

  final StreamType type;
  final String streamLogoIcon;
  final String vodTrailerUrl;
  final String backgroundUrl;
  final double imageCheckTime;
  final bool skipGroups;
  final List<String> groups;
  final bool isSeries;

  UploadM3u(
      {required this.type,
      required this.streamLogoIcon,
      required this.vodTrailerUrl,
      required this.backgroundUrl,
      required this.imageCheckTime,
      required this.skipGroups,
      required this.groups,
      required this.isSeries});

  factory UploadM3u.fromJson(Map<String, dynamic> json) {
    return UploadM3u(
        type: StreamType.fromInt(json[_TYPE_FIELD]),
        streamLogoIcon: json[_STREAM_LOGO_URL_FIELD],
        vodTrailerUrl: json[_TRAILER_URL_FIELD],
        backgroundUrl: json[_BACKGROUND_URL_FIELD],
        imageCheckTime: json[_IMAGE_TIME_TO_CHECK_FIELD],
        skipGroups: json[_SKIP_GROUPS_FIELD],
        groups: json[_GROUPS_FIELD],
        isSeries: json[_IS_SERIES_FIELD]);
  }

  Map<String, dynamic> toJson() {
    return {
      _TYPE_FIELD: type.toInt(),
      _STREAM_LOGO_URL_FIELD: streamLogoIcon,
      _TRAILER_URL_FIELD: vodTrailerUrl,
      _BACKGROUND_URL_FIELD: backgroundUrl,
      _IMAGE_TIME_TO_CHECK_FIELD: imageCheckTime,
      _SKIP_GROUPS_FIELD: skipGroups,
      _GROUPS_FIELD: groups,
      _IS_SERIES_FIELD: isSeries
    };
  }
}
