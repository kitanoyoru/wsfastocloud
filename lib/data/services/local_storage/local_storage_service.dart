import 'package:shared_preferences/shared_preferences.dart';

class LocalStorageService {
  static LocalStorageService? _instance;
  SharedPreferences? _preferences;

  static Future<LocalStorageService> getInstance() async {
    _instance ??= LocalStorageService();
    _instance!._preferences ??= await SharedPreferences.getInstance();
    return _instance!;
  }

  static const String _connectedUrl = 'connected_url';
  static const String _auth = 'auth';
  static const String _encodedAuth = 'encoded_auth';
  static const String _mode = 'mode';

  String? connectedUrl() {
    return _preferences!.getString(_connectedUrl);
  }

  void setConnectedUrl(String url) {
    _preferences!.setString(_connectedUrl, url);
  }

  String? auth() {
    return _preferences!.getString(_auth);
  }

  void setAuth(String url) {
    _preferences!.setString(_auth, url);
  }

  String? encodedAuth() {
    return _preferences!.getString(_encodedAuth);
  }

  void setEncodedAuth(String url) {
    _preferences!.setString(_encodedAuth, url);
  }

  String? mode() {
    return _preferences!.getString(_mode);
  }

  void setMode(String mode) {
    _preferences!.setString(_mode, mode);
  }
}
