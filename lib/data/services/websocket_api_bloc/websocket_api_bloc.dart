import 'dart:async';
import 'dart:convert';
import 'dart:developer' as developer;

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:wsfastocloud/data/services/api/models/connection_info.dart';
import 'package:wsfastocloud/domain/websocket/websocket/websocket_delegate_native.dart';
import 'package:wsfastocloud/presenter/connection_bloc/connection_bloc.dart';

part 'websocket_api_event.dart';
part 'websocket_api_state.dart';

class WebSocketApiBloc extends Bloc<WebSocketApiEvent, WebSocketState> {
  WebSocketApiBloc(this.connectionBloc) : super(WebSocketDisconnected()) {
    _connectionSubscription = connectionBloc.stream.listen((state) async {
      if (state.info != null) {
        _connect(_generateUrl(state.info!));
      } else {
        add(onDisconnectEvent());
      }
    });
    on<onOpenEvent>(_onOpenWebSocket);
    on<onMessageEvent>(_onMessageWebSocket);
    on<onErrorEvent>(_onErrorWebSocket);
    on<onClosedEvent>(_onClosedWebSocket);
    on<onDisconnectEvent>(_disconnect);
  }

  final ConnectionBloc connectionBloc;
  late final StreamSubscription _connectionSubscription;
  WebSocketDelegate? _delegate;

  bool isConnected() {
    return !(state is WebSocketDisconnected);
  }

  String _generateUrl(ConnectionInfo info) {
    var url = '${info.socketUrl}/updates';

    if (info.encodedAuth != null) {
      url += '?token=${info.encodedAuth}';
    }

    return url;
  }

  void _connect(String url) {
    _delegate?.close();

    _delegate = WebSocketDelegate(
      url,
      onOpen: () => add(onOpenEvent()),
      onMessage: (message) => add(onMessageEvent(message)),
      onError: (error) => add(onErrorEvent(error)),
      onClose: (code, reason) => add(onClosedEvent(code, reason)),
    );

    _delegate!.connect();
  }

  void _disconnect(onDisconnectEvent event, Emitter<WebSocketState> emit) {
    _delegate?.close();
    emit(WebSocketDisconnected());
  }

  void dispose() {
    add(onDisconnectEvent());
    _connectionSubscription.cancel();
  }

  void _onOpenWebSocket(onOpenEvent event, Emitter<WebSocketState> emit) {
    developer.log('Websocket connected');
    emit(WebSocketConnected());
  }

  void _onMessageWebSocket(onMessageEvent event, Emitter<WebSocketState> emit) {
    developer.log('Websocket message: ${event.message}');
    final data = json.decode(event.message);
    if (data['type'] != null) {
      emit(WebSocketApiMessage.fromJson(data));
    }
  }

  void _onErrorWebSocket(onErrorEvent event, Emitter<WebSocketState> emit) {
    developer.log('Websocket error: ${event.error}');
    emit(WebSocketFailure.fromError(event.error));
  }

  void _onClosedWebSocket(onClosedEvent event, Emitter<WebSocketState> emit) {
    developer.log('Websocket closed by server: ${event.reason} (code: ${event.code})');
    emit(WebSocketDisconnected());
  }
}
