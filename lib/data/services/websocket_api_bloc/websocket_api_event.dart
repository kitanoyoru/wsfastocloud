part of 'websocket_api_bloc.dart';

abstract class WebSocketApiEvent extends Equatable {
  WebSocketApiEvent();

  @override
  List<Object?> get props => [];
}

class onOpenEvent extends WebSocketApiEvent {
  onOpenEvent();

  @override
  List<Object?> get props => [];
}

class onDisconnectEvent extends WebSocketApiEvent {
  onDisconnectEvent();

  @override
  List<Object?> get props => [];
}

class onMessageEvent extends WebSocketApiEvent {
  final dynamic message;

  onMessageEvent(this.message);

  @override
  List<Object?> get props => [message];
}

class onErrorEvent extends WebSocketApiEvent {
  final dynamic error;

  onErrorEvent(this.error);

  @override
  List<Object?> get props => [error];
}

class onClosedEvent extends WebSocketApiEvent {
  final int? code;
  final String? reason;

  onClosedEvent(
    this.code,
    this.reason,
  );

  @override
  List<Object?> get props => [code, reason];
}
