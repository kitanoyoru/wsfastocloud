import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';

abstract class StreamDBRepository {
  Future<List<IStream>> getDBStreams();

  Future<bool> addStream(IStream stream);

  Future<bool> uploadM3uFile(StreamType type, bool isSeries, double imageCheckTime,
      final bool skipGroups, final List<String> groups, Map<String, List<int>> data);

  Future<bool> editStream(IStream stream);

  Future<bool> removeStream(String id);

  Future<String> probeOutUrl(OutputUrl url);

  Future<String> getTokenUrl(String ip);

  Future<String> embedOutput(OutputUrl url);

  Future<String> probeInUrl(InputUrl url);

  Future<List<IStream>> loadEpisodes();

  // seasons
  Future<List<ServerSeason>> loadSeasons();

  Future<void> addSeason(ServerSeason season);

  Future<void> editSeason(ServerSeason season);

  Future<void> removeSeason(ServerSeason season);

  // serials
  Future<List<ServerSerial>> loadSerials();

  Future<void> addSerial(ServerSerial serial);

  Future<void> editSerial(ServerSerial serial);

  Future<void> removeSerial(ServerSerial serial);

  Future<void> startStream(String id);

  Defaults defaults();
}
