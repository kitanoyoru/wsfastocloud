import 'package:web_socket_channel/web_socket_channel.dart';

import 'websocket_delegate_base.dart';

class WebSocketDelegate extends WebSocketDelegateBase {
  WebSocketChannel? _socket;

  WebSocketDelegate(
    String url, {
    OnOpenCallback? onOpen,
    OnMessageCallback? onMessage,
    OnCloseCallback? onClose,
    OnErrorCallback? onError,
  }) : super(url, onOpen, onMessage, onClose, onError) {}

  @override
  Future<void> connect() async {
    try {
      _socket = await WebSocketChannel.connect(Uri.parse(url));
      onOpen?.call();
      _socket!.stream.listen(
        onMessage,
        onDone: () => onClose?.call(_socket!.closeCode, _socket!.closeReason),
        onError: onError,
      );
    } catch (e) {
      onClose?.call(500, e.toString());
    }
  }

  @override
  void send(data) {
    _socket?.sink.add(data);
  }

  @override
  void close() {
    _socket?.sink.close();
  }
}
