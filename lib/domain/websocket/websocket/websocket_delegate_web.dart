// ignore: avoid_web_libraries_in_flutter
import 'dart:html';

import 'websocket_delegate_base.dart';

class WebSocketDelegate extends WebSocketDelegateBase {
  WebSocket? _socket;

  WebSocketDelegate(
    String url, {
    OnOpenCallback? onOpen,
    OnMessageCallback? onMessage,
    OnCloseCallback? onClose,
    OnErrorCallback? onError,
  }) : super(url, onOpen, onMessage, onClose, onError);

  @override
  Future<void> connect() async {
    try {
      _socket = WebSocket(url);
      _socket!.onOpen.listen((_) => onOpen?.call());
      _socket!.onMessage.listen((e) => onMessage?.call(e.data));
      _socket!.onError.listen((e) => onError?.call(e));
      _socket!.onClose.listen((e) => onClose?.call(e.code, e.reason));
    } catch (e) {
      onClose?.call(500, e.toString());
    }
  }

  @override
  void send(data) {
    _socket?.send(data);
  }

  @override
  void close() {
    _socket?.close();
  }
}
