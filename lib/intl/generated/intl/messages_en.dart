// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);

  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "about": MessageLookupByLibrary.simpleMessage("About"),
        "actions": MessageLookupByLibrary.simpleMessage("Actions"),
        "activate": MessageLookupByLibrary.simpleMessage("Activate"),
        "add": MessageLookupByLibrary.simpleMessage("Add"),
        "arch": MessageLookupByLibrary.simpleMessage("Arch"),
        "averageLoad": MessageLookupByLibrary.simpleMessage("Average load"),
        "cancel": MessageLookupByLibrary.simpleMessage("Cancel"),
        "chooseLanguage": MessageLookupByLibrary.simpleMessage("Choose Language"),
        "close": MessageLookupByLibrary.simpleMessage("Close"),
        "connect": MessageLookupByLibrary.simpleMessage("Connect"),
        "contentRequests": MessageLookupByLibrary.simpleMessage("Content Requests"),
        "createdDate": MessageLookupByLibrary.simpleMessage("Created date"),
        "developers": MessageLookupByLibrary.simpleMessage("Developers"),
        "disconnect": MessageLookupByLibrary.simpleMessage("Disconnect"),
        "downloads": MessageLookupByLibrary.simpleMessage("Downloads"),
        "ePG": MessageLookupByLibrary.simpleMessage("EPG"),
        "edit": MessageLookupByLibrary.simpleMessage("Edit"),
        "educationAndSupport": MessageLookupByLibrary.simpleMessage("Education and support"),
        "email": MessageLookupByLibrary.simpleMessage("Email"),
        "expirationDate": MessageLookupByLibrary.simpleMessage("Expiration date"),
        "extension": MessageLookupByLibrary.simpleMessage("Extension"),
        "followUs": MessageLookupByLibrary.simpleMessage("Follow us"),
        "free": MessageLookupByLibrary.simpleMessage("Free"),
        "hdd": MessageLookupByLibrary.simpleMessage("HDD"),
        "home": MessageLookupByLibrary.simpleMessage("Home"),
        "id": MessageLookupByLibrary.simpleMessage("Id"),
        "language": MessageLookupByLibrary.simpleMessage("Language"),
        "languageName": MessageLookupByLibrary.simpleMessage("Language name"),
        "loadBalancers": MessageLookupByLibrary.simpleMessage("Load Balancers"),
        "localization": MessageLookupByLibrary.simpleMessage("Localization"),
        "logout": MessageLookupByLibrary.simpleMessage("Logout"),
        "maxDeviceCount": MessageLookupByLibrary.simpleMessage("Max device count"),
        "name": MessageLookupByLibrary.simpleMessage("Name"),
        "network": MessageLookupByLibrary.simpleMessage("Network"),
        "ok": MessageLookupByLibrary.simpleMessage("OK"),
        "onlineUsers": MessageLookupByLibrary.simpleMessage("Online users"),
        "os": MessageLookupByLibrary.simpleMessage("OS"),
        "pricing": MessageLookupByLibrary.simpleMessage("Pricing"),
        "profile": MessageLookupByLibrary.simpleMessage("Profile"),
        "providers": MessageLookupByLibrary.simpleMessage("Providers"),
        "remove": MessageLookupByLibrary.simpleMessage("Remove"),
        "save": MessageLookupByLibrary.simpleMessage("Save"),
        "series": MessageLookupByLibrary.simpleMessage("Series"),
        "servers": MessageLookupByLibrary.simpleMessage("Servers"),
        "signIn": MessageLookupByLibrary.simpleMessage("Sign in"),
        "signUp": MessageLookupByLibrary.simpleMessage("Sign up"),
        "status": MessageLookupByLibrary.simpleMessage("Status"),
        "subscribers": MessageLookupByLibrary.simpleMessage("Subscribers"),
        "sync": MessageLookupByLibrary.simpleMessage("Sync"),
        "synctime": MessageLookupByLibrary.simpleMessage("Synctime"),
        "timestamp": MessageLookupByLibrary.simpleMessage("Timestamp"),
        "title": MessageLookupByLibrary.simpleMessage("Title"),
        "total": MessageLookupByLibrary.simpleMessage("Total"),
        "type": MessageLookupByLibrary.simpleMessage("Type"),
        "uptime": MessageLookupByLibrary.simpleMessage("Uptime"),
        "url": MessageLookupByLibrary.simpleMessage("Url"),
        "used": MessageLookupByLibrary.simpleMessage("Used"),
        "version": MessageLookupByLibrary.simpleMessage("Version")
      };
}
