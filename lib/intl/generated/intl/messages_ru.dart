// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru';

  final messages = _notInlinedMessages(_notInlinedMessages);

  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "about": MessageLookupByLibrary.simpleMessage("О нас"),
        "actions": MessageLookupByLibrary.simpleMessage("Действия"),
        "activate": MessageLookupByLibrary.simpleMessage("Активировать"),
        "add": MessageLookupByLibrary.simpleMessage("Добавить"),
        "arch": MessageLookupByLibrary.simpleMessage("Архитектура"),
        "averageLoad": MessageLookupByLibrary.simpleMessage("Средняя нагрузка"),
        "cancel": MessageLookupByLibrary.simpleMessage("Отмена"),
        "chooseLanguage": MessageLookupByLibrary.simpleMessage("Выберите язык"),
        "close": MessageLookupByLibrary.simpleMessage("Закрыть"),
        "connect": MessageLookupByLibrary.simpleMessage("Подключиться"),
        "contentRequests": MessageLookupByLibrary.simpleMessage("Запросы контента"),
        "createdDate": MessageLookupByLibrary.simpleMessage("Дата создания"),
        "developers": MessageLookupByLibrary.simpleMessage("Разработчики"),
        "disconnect": MessageLookupByLibrary.simpleMessage("Отключиться"),
        "downloads": MessageLookupByLibrary.simpleMessage("Загрузки"),
        "ePG": MessageLookupByLibrary.simpleMessage("EPG"),
        "edit": MessageLookupByLibrary.simpleMessage("Изменить"),
        "educationAndSupport": MessageLookupByLibrary.simpleMessage("Обучение и поддержка"),
        "email": MessageLookupByLibrary.simpleMessage("Почта"),
        "expirationDate": MessageLookupByLibrary.simpleMessage("Срок годности"),
        "extension": MessageLookupByLibrary.simpleMessage("Расширение"),
        "followUs": MessageLookupByLibrary.simpleMessage("Подписывайтесь на нас"),
        "free": MessageLookupByLibrary.simpleMessage("Свободно"),
        "hdd": MessageLookupByLibrary.simpleMessage("HDD"),
        "home": MessageLookupByLibrary.simpleMessage("Домой"),
        "id": MessageLookupByLibrary.simpleMessage("Id"),
        "language": MessageLookupByLibrary.simpleMessage("Язык"),
        "languageName": MessageLookupByLibrary.simpleMessage("Язык"),
        "loadBalancers": MessageLookupByLibrary.simpleMessage("Балансировщики нагрузки"),
        "localization": MessageLookupByLibrary.simpleMessage("Локализация"),
        "logout": MessageLookupByLibrary.simpleMessage("Выход"),
        "maxDeviceCount": MessageLookupByLibrary.simpleMessage("Максимальное количество устройств"),
        "name": MessageLookupByLibrary.simpleMessage("Название"),
        "network": MessageLookupByLibrary.simpleMessage("Соединение"),
        "ok": MessageLookupByLibrary.simpleMessage("OK"),
        "onlineUsers": MessageLookupByLibrary.simpleMessage("Пользователи онлайн"),
        "os": MessageLookupByLibrary.simpleMessage("ОС"),
        "pricing": MessageLookupByLibrary.simpleMessage("Цены"),
        "profile": MessageLookupByLibrary.simpleMessage("Профиль"),
        "providers": MessageLookupByLibrary.simpleMessage("Провайдеры"),
        "remove": MessageLookupByLibrary.simpleMessage("Удалить"),
        "save": MessageLookupByLibrary.simpleMessage("Сохранить"),
        "series": MessageLookupByLibrary.simpleMessage("Серии"),
        "servers": MessageLookupByLibrary.simpleMessage("Серверы"),
        "signIn": MessageLookupByLibrary.simpleMessage("Вход"),
        "signUp": MessageLookupByLibrary.simpleMessage("Регистрация"),
        "status": MessageLookupByLibrary.simpleMessage("Статус"),
        "subscribers": MessageLookupByLibrary.simpleMessage("Подписчики"),
        "sync": MessageLookupByLibrary.simpleMessage("Sync"),
        "synctime": MessageLookupByLibrary.simpleMessage("Время синхронизации"),
        "timestamp": MessageLookupByLibrary.simpleMessage("Отметка времени"),
        "title": MessageLookupByLibrary.simpleMessage("Название"),
        "total": MessageLookupByLibrary.simpleMessage("Всего"),
        "type": MessageLookupByLibrary.simpleMessage("Тип"),
        "uptime": MessageLookupByLibrary.simpleMessage("Время работы"),
        "url": MessageLookupByLibrary.simpleMessage("Url"),
        "used": MessageLookupByLibrary.simpleMessage("Используется"),
        "version": MessageLookupByLibrary.simpleMessage("Версия")
      };
}
