import 'dart:convert';

import 'package:fastocloud_dart_models/fastocloud_dart_models.dart' hide Provider;
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:wsfastocloud/constants.dart';
import 'package:wsfastocloud/data/repositories/stream_db_repository.dart';
import 'package:wsfastocloud/data/repositories/stream_repository.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/data/services/api/models/connection_info.dart';
import 'package:wsfastocloud/data/services/local_storage/local_storage_service.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';
import 'package:wsfastocloud/domain/repositories/stream_db_repository.dart';
import 'package:wsfastocloud/domain/repositories/stream_repository.dart';
import 'package:wsfastocloud/intl/l10n/l10n.dart';
import 'package:wsfastocloud/locator.dart';
import 'package:wsfastocloud/presenter/connection_bloc/connection_bloc.dart';
import 'package:wsfastocloud/presenter/server_details.dart';
import 'package:wsfastocloud/presenter/service_info_bloc/service_info_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/media_statistics_bloc/media_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/on_air_tabs_bloc/on_air_tab_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/server_statistics_bloc/server_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/store_tabs_bloc/store_tabs_bloc.dart';
import 'package:wsfastocloud/presenter/streams/on_air/stream_statistics_bloc/stream_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/streams/store/seasons_store_bloc/seasons_store_bloc.dart';
import 'package:wsfastocloud/presenter/streams/store/serials_store_bloc/serials_store_bloc.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_store_bloc/streams_store_bloc.dart';
import 'package:wsfastocloud/theme.dart';
import 'package:wsfastocloud/utils/uri_host_and_port.dart';

void main() async {
  await WidgetsFlutterBinding.ensureInitialized();
  await setupLocator();
  runApp(MyApp());
}

Uri? tryParseConnect(String uri) {
  try {
    final decoded = base64Decode(uri);
    final parsed = utf8.decode(decoded);
    return Uri.tryParse(parsed);
  } on FormatException {
    return null;
  }
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  final storage = locator<LocalStorageService>();
  static ThemeData theme = AppTheme.lightTheme;
  static const Locale _locale = Locale('en');
  static const supportedLocales = [Locale('en'), Locale('ru')];
  final hostAndPort = UriHostAndPort().hostAndPort();

  Route? onGenerateRoute(RouteSettings settings) {
    if (settings.name == null) {
      return null;
    }
    final url = settings.name!;
    final parsedUri = Uri.parse(url);
    return PageRouteBuilder(
        pageBuilder: (ctx, _, __) {
          final encArgs = parsedUri.queryParameters;
          WsMode mode = WsMode.OTT;
          WSServer? server;
          if (encArgs.containsKey('url')) {
            final base = encArgs['url']!;
            final parsedUri = tryParseConnect(base);
            if (parsedUri != null) {
              final args = parsedUri.queryParameters;
              if (args.containsKey('url')) {
                final wsUrl = args['url']!;
                if (args.containsKey('secret')) {
                  server = makeWSServerFromEncodedAuth(wsUrl, args['secret']!);
                } else {
                  server = WSServer(url: wsUrl);
                }
                if (args.containsKey('mode')) {
                  final modeInt = int.tryParse(args['mode']!);
                  if (modeInt != null) {
                    mode = WsMode.fromInt(modeInt);
                  }
                }
                // if (args.containsKey('theme')) {
                //   final value = args['theme']!;
                //   Future.delayed(Duration.zero, () {
                //     setState(() {
                //       final parsed = int.parse(value);
                //       if (parsed == GlobalTheme.DARK.toInt()) {
                //         theme = AppTheme.darkTheme;
                //       } else {
                //         theme = AppTheme.lightTheme;
                //       }
                //     });
                //   });
                // }
                // if (args.containsKey('locale')) {
                //   final value = args['locale']!;
                //   Future.delayed(Duration.zero, () {
                //     if (value == 'ru') {
                //       setState(() {
                //         _locale = Locale(value);
                //       });
                //     }
                //   });
                // }
                if (server != null) {
                  ctx
                      .read<ConnectionBloc>()
                      .add(ConnectRequestedEvent(ConnectionInfo.fromUserInput(server), mode));
                }
              }
            }
          } else {
            mode = WsMode.fromInt(int.parse(storage.mode() ?? '0'));
            ctx.read<ConnectionBloc>().add(ConnectFromLocalStorage());
          }
          return ServerDetailsPage(mode: mode);
        },
        transitionDuration: Duration.zero,
        settings: settings);
  }

  @override
  Widget build(BuildContext context) {
    final providers = _setupProviders();
    return MultiProvider(
        providers: providers,
        child: MaterialApp(
            localizationsDelegates: AppLocalizations.localizationsDelegates,
            supportedLocales: supportedLocales,
            locale: _locale,
            title: PROJECT,
            theme: theme,
            onGenerateRoute: onGenerateRoute));
  }

  List<SingleChildWidget> _setupProviders() {
    return [
      Provider<ConnectionBloc>(create: (_) => ConnectionBloc()),
      ProxyProvider<ConnectionBloc, Fetcher>(
          update: (_, connection, __) => Fetcher(connection),
          dispose: (_, fetcher) => fetcher.dispose()),
      ProxyProvider<ConnectionBloc, WebSocketApiBloc>(
          update: (_, connection, __) => WebSocketApiBloc(connection),
          dispose: (_, bloc) => bloc.dispose()),
      ProxyProvider<Fetcher, StreamRepository>(
          update: (_, fetcher, __) => StreamRepositoryImpl(fetcher)),
      ProxyProvider<WebSocketApiBloc, StoreTabsBloc>(update: (_, ws, __) {
        return StoreTabsBloc(ws);
      }, dispose: (_, bloc) {
        bloc.dispose();
      }),
      ProxyProvider<WebSocketApiBloc, OnAirTabBloc>(update: (_, ws, __) {
        return OnAirTabBloc(ws);
      }, dispose: (_, bloc) {
        bloc.dispose();
      }),
      ProxyProvider<Fetcher, StreamDBRepository>(
          update: (_, fetcher, __) => StreamDBRepositoryImpl(fetcher)),
      ProxyProvider2<WebSocketApiBloc, Fetcher, ServiceInfoBloc>(update: (_, ws, fetcher, __) {
        return ServiceInfoBloc(ws, fetcher);
      }, dispose: (_, bloc) {
        return bloc.dispose();
      }),
      ProxyProvider2<WebSocketApiBloc, Fetcher, MediaStatisticsBloc>(update: (_, ws, fetcher, __) {
        return MediaStatisticsBloc(ws, fetcher);
      }, dispose: (_, bloc) {
        return bloc.dispose();
      }),
      ProxyProvider2<WebSocketApiBloc, Fetcher, ServiceStatisticsBloc>(
          update: (_, ws, fetcher, __) {
        return ServiceStatisticsBloc(ws, fetcher);
      }, dispose: (_, bloc) {
        return bloc.dispose();
      }),
      ProxyProvider2<WebSocketApiBloc, StreamRepository, StreamsStatisticsBloc>(
          update: (_, ws, repo, __) {
        return StreamsStatisticsBloc(ws, repo);
      }, dispose: (_, bloc) {
        return bloc.dispose();
      }),
      ProxyProvider2<WebSocketApiBloc, StreamDBRepository, StreamsStoreBloc>(
          update: (_, ws, repo, __) {
        return StreamsStoreBloc(ws, repo);
      }, dispose: (_, bloc) {
        return bloc.dispose();
      }),
      ProxyProvider2<WebSocketApiBloc, StreamDBRepository, SeasonsBloc>(update: (_, ws, repo, __) {
        return SeasonsBloc(ws, repo);
      }, dispose: (_, bloc) {
        return bloc.dispose();
      }),
      ProxyProvider2<WebSocketApiBloc, StreamDBRepository, SerialsBloc>(update: (_, ws, repo, __) {
        return SerialsBloc(ws, repo);
      }, dispose: (_, bloc) {
        return bloc.dispose();
      })
    ];
  }
}
