import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/errors.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/data/services/api/models/connection_info.dart';
import 'package:wsfastocloud/intl/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/connection_bloc/connection_bloc.dart';
import 'package:wsfastocloud/presenter/main_tab_bar.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/store_tabs_bloc/store_tabs_bloc.dart';
import 'package:wsfastocloud/presenter/stats_header.dart';
import 'package:wsfastocloud/presenter/widgets/connect_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/master_config/master_config_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/master_config/master_connect_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/master_config/master_fetcher.dart';

class LayoutWidget extends StatelessWidget {
  final WsMode mode;

  const LayoutWidget({Key? key, required this.mode}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<StoreTabsBloc, StoreTabsState>(
      builder: (context, state) {
        final connected = state is StoreTabsConnectedState;
        return Column(children: [
          _StatsHeader(
            connected: connected,
            mode: mode,
          ),
          OnAirStoreTabBar(connected: connected, mode: mode)
        ]);
      },
    );
  }
}

class _StatsHeader extends StatelessWidget {
  final bool connected;
  final WsMode mode;

  const _StatsHeader({Key? key, required this.connected, required this.mode}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StatsButtons(buttons: _buttonsRow(context), showConnectInfo: true);
  }

  Widget _buttonsRow(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [connectionButton(context), getConfig(context)]);
  }

  StatsButton connectionButton(BuildContext context) {
    return StatsButton(
        title: connected ? context.l10n.disconnect : context.l10n.connect,
        onPressed: () => connected ? _disconnect(context) : _connect(context));
  }

  IconButton getConfig(BuildContext context) {
    final onPressed = connected ? () => _getConfig(context) : null;
    return IconButton(icon: const Icon(Icons.settings), tooltip: 'Config', onPressed: onPressed);
  }

  // buttons
  void _connect(BuildContext context) async {
    final info = await showDialog<ConnectionInfo>(
        context: context,
        builder: (_) {
          return ConnectDialog(value: WSServer.createDefault());
        });

    if (info != null) {
      context.read<ConnectionBloc>().add(ConnectRequestedEvent(info, mode));
    }
  }

  void _disconnect(BuildContext context) {
    context.read<ConnectionBloc>().add(DisconnectRequestedEvent());
  }

  void _getConfig(BuildContext context) async {
    final info = await showDialog<Credentials>(
        context: context,
        builder: (context) {
          return MasterConnectDialog();
        });

    if (info != null) {
      final fetcher = context.read<Fetcher>();
      if (fetcher.backendServerUrl == null) {
        return;
      }
      final master = MasterFetcher(fetcher.backendServerUrl!, info);
      final config = await master.getConfig();
      final updated = await showDialog(
          context: context,
          builder: (context) {
            return MasterConfigDialog.edit(config, (term) {
              final resp = master.getHardwareHash(term);
              resp.then((body) {
                TextEditingController _textController = TextEditingController(text: body);
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return SimpleDialog(
                          title: const Text('Get hardware hash'),
                          children: <Widget>[
                            Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Column(children: [
                                  TextField(
                                      keyboardType: TextInputType.multiline,
                                      maxLines: null,
                                      controller: _textController),
                                  SizedBox(height: 5.0),
                                  FlatButtonEx.filled(
                                      text: 'Copy',
                                      onPressed: () {
                                        Clipboard.setData(
                                            ClipboardData(text: _textController.text));
                                        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                                          content: Text('Copied'),
                                        ));
                                      }),
                                  SizedBox(height: 5.0),
                                  FlatButtonEx.filled(
                                      text: 'Close',
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      })
                                ]))
                          ]);
                    });
              }, onError: (error) {
                showError(context, error);
              });
            });
          });
      if (updated != null) {
        master.setConfig(updated);
      }
    } //master:master
  }
}
