import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:fastotv_dart/commands_info/ott_server_info.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:flutter/foundation.dart';
import 'package:wsfastocloud/data/services/api/models/connection_info.dart';
import 'package:wsfastocloud/data/services/local_storage/local_storage_service.dart';
import 'package:wsfastocloud/locator.dart';
import 'package:wsfastocloud/utils/uri_host_and_port.dart';

part 'connection_event.dart';
part 'connection_state.dart';

class ConnectionBloc extends Bloc<ConnectionEvent, ConnectionState> {
  ConnectionBloc() : super(ConnectionState(null)) {
    on<InitializeEvent>(_initialize);
    on<ConnectRequestedEvent>(connect);
    on<ConnectFromLocalStorage>(connectLocalStorage);
    on<DisconnectRequestedEvent>(disconnect);
  }

  final hostAndPort = UriHostAndPort().hostAndPort();
  WSServer? server;

  void _initialize(InitializeEvent event, Emitter<ConnectionState> emit) async {
    emit(ConnectionState(null));
  }

  void connect(ConnectRequestedEvent event, Emitter<ConnectionState> emit) async {
    emit(ConnectionState(null));
    final storage = locator<LocalStorageService>();
    storage.setMode('${event.mode.toInt()}');
    storage.setAuth(event.info!.originalUrl);
    if (event.info?.encodedAuth != null) {
      storage.setEncodedAuth(event.info!.encodedAuth!);
      server = makeWSServerFromEncodedAuth(event.info!.originalUrl, event.info!.encodedAuth!);
    } else {
      server = WSServer(url: event.info!.originalUrl);
    }
    if (kIsWeb) {
      storage.setConnectedUrl(
          server!.makeWsUrlWithHost(hostAndPort, GlobalTheme.LIGHT, event.mode, 'en'));
    } else {
      storage.setConnectedUrl(server!.makeWsUrl('latest', GlobalTheme.LIGHT, event.mode, 'en')!);
    }
    emit(ConnectionState(event.info));
  }

  void connectLocalStorage(ConnectFromLocalStorage event, Emitter<ConnectionState> emit) async {
    emit(ConnectionState(null));
    final storage = locator<LocalStorageService>();
    final _originalUrl = storage.auth();
    final _encodedAuth = storage.encodedAuth();
    if (_originalUrl != null) {
      final _mode = int.parse(storage.mode()!);
      if (_encodedAuth != null) {
        server = makeWSServerFromEncodedAuth(_originalUrl, _encodedAuth);
      } else {
        server = WSServer(url: _originalUrl);
      }
      if (kIsWeb) {
        storage.setConnectedUrl(
            server!.makeWsUrlWithHost(hostAndPort, GlobalTheme.LIGHT, WsMode.fromInt(_mode), 'en'));
      } else {
        storage.setConnectedUrl(
            server!.makeWsUrl('latest', GlobalTheme.LIGHT, WsMode.fromInt(_mode), 'en')!);
      }
      emit(ConnectionState(ConnectionInfo.fromUserInput(server!)));
    }
  }

  void disconnect(DisconnectRequestedEvent event, Emitter<ConnectionState> emit) async {
    emit(ConnectionState(null));
  }
}
