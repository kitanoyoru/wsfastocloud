import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wsfastocloud/data/services/local_storage/local_storage_service.dart';
import 'package:wsfastocloud/locator.dart';
import 'package:wsfastocloud/presenter/stats_header.dart';

class CopyLinkButton extends StatelessWidget {
  const CopyLinkButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final storage = locator<LocalStorageService>();
    final url = storage.connectedUrl();
    return StatsButton(
        title: 'Copy',
        onPressed: () {
          Clipboard.setData(ClipboardData(text: url)).then((_) {
            ScaffoldMessenger.of(context)
                .showSnackBar(SnackBar(content: Text('Url copied to clipboard')));
          });
        });
  }
}
