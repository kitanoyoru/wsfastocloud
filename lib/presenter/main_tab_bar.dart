import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/media_statistics_bloc/media_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/service_statistics_air.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/service_statistics_air_mob.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/service_statistics_store.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/service_statistics_store_mob.dart';
import 'package:wsfastocloud/presenter/streams/store/server_streams.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_on_air.dart';
import 'package:wsfastocloud/presenter/widgets/server_details_builder.dart';

class OnAirStoreTabBar extends StatefulWidget {
  const OnAirStoreTabBar({Key? key, required this.connected, required this.mode}) : super(key: key);
  final bool connected;
  final WsMode? mode;

  @override
  State<OnAirStoreTabBar> createState() {
    return _onAirStoreTabBarState();
  }
}

class _onAirStoreTabBarState extends State<OnAirStoreTabBar> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this, initialIndex: 1);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<MediaStatisticsBloc, MediaStatisticsState>(listener: (context, state) {
      if (state is MediaStatisticsData) {
        if (state.mediaInfo.status == null) {
          context.read<MediaStatisticsBloc>().add(FetchMediaStatisticsEvent());
        }
      }
    }, builder: (context, state) {
      if (state is MediaStatisticsData) {
        final tabs = TabBar(
            controller: _tabController,
            tabs: [const Tab(text: 'On Air'), const Tab(text: 'Store')],
            labelColor: Colors.black);
        final content = SizedBox(
            height: kIsWeb
                ? MediaQuery.of(context).size.height * 0.8
                : MediaQuery.of(context).size.height * 0.82,
            child: TabBarView(physics: ScrollPhysics(), controller: _tabController, children: [
              SingleChildScrollView(
                  child: ServerDetailsBuilder(
                      stats: ServerStatsOnAirWidget(),
                      table: OnAirStreamsTabs(),
                      desc: ServerStatsOnAirDescWidget())),
              SingleChildScrollView(
                  child: ServerDetailsBuilder(
                      desc: ServerStatsStoreDescWidget(
                          mediaServerInfo: state.mediaInfo, mode: widget.mode),
                      table: ServerStreamsTabs(
                          connected: widget.connected,
                          mediaServerInfo: state.mediaInfo,
                          mode: widget.mode),
                      stats: ServerStatsStoreWidget()))
            ]));
        return Column(children: [tabs, content]);
      }
      return OnlyStoreTabBar(connected: widget.connected, mode: widget.mode);
    });
  }
}

class OnlyStoreTabBar extends StatefulWidget {
  const OnlyStoreTabBar({Key? key, required this.connected, required this.mode}) : super(key: key);
  final bool connected;
  final WsMode? mode;

  @override
  State<OnlyStoreTabBar> createState() {
    return _OnlyStoreTabBarState();
  }
}

class _OnlyStoreTabBarState extends State<OnlyStoreTabBar> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 1, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final init = MediaServerInfo.createInit();
    final tabs = TabBar(
        controller: _tabController, tabs: [const Tab(text: 'Store')], labelColor: Colors.black);
    final content = SizedBox(
        height: kIsWeb
            ? MediaQuery.of(context).size.height * 0.8
            : MediaQuery.of(context).size.height * 0.82,
        child: TabBarView(physics: ScrollPhysics(), controller: _tabController, children: [
          SingleChildScrollView(
              child: ServerDetailsBuilder(
                  desc: ServerStatsStoreDescWidget(mode: widget.mode, mediaServerInfo: init),
                  table: ServerStreamsTabs(
                      connected: widget.connected, mediaServerInfo: init, mode: widget.mode),
                  stats: ServerStatsStoreWidget()))
        ]));
    return Column(children: [tabs, content]);
  }
}
