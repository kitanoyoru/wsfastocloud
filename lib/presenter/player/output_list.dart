import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';

class OutputUrlsList extends StatefulWidget {
  final int current;
  final bool shrinkWrap;
  final List<OutputUrl> list;
  final Color textColor;
  final void Function(int) playLink;

  const OutputUrlsList(this.list, this.current, this.textColor, this.playLink, this.shrinkWrap);

  @override
  _OutputUrlsListState createState() {
    return _OutputUrlsListState();
  }
}

class _OutputUrlsListState extends State<OutputUrlsList> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: widget.shrinkWrap,
        itemCount: widget.list.length,
        itemBuilder: (context, index) => ListTile(
            trailing: index == widget.current
                ? Icon(Icons.play_arrow, color: Theme.of(context).colorScheme.secondary)
                : null,
            title: Text('Link ${index + 1}', style: TextStyle(color: widget.textColor)),
            onTap: () => widget.playLink(index)));
  }
}
