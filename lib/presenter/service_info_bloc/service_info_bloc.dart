import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:flutter_common/http_response.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/data/services/api/models/server_info.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';

part 'service_info_event.dart';
part 'service_info_state.dart';

class ServiceInfoBloc extends Bloc<ServiceInfoEvent, ServiceInfoState> {
  ServiceInfoBloc(this.webSocketApiBloc, this.fetcher) : super(ServiceInfoInitial()) {
    on<ListenEvent>(_listen);
    on<FetchEvent>(_fetch);
    on<InitialEvent>(_init);
    on<FailureEvent>(_failure);
    add(ListenEvent());
  }

  late final StreamSubscription _subscription;

  final WebSocketApiBloc webSocketApiBloc;
  final Fetcher fetcher;

  void dispose() {
    _subscription.cancel();
  }

  void _init(InitialEvent event, Emitter<ServiceInfoState> emit) {
    emit(ServiceInfoInitial());
  }

  void _listen(ListenEvent event, Emitter<ServiceInfoState> emit) async {
    _subscription = webSocketApiBloc.stream.listen((event) {
      if (event is WebSocketApiMessage) {
      } else if (event is WebSocketFailure) {
        add(FailureEvent(event.description));
      } else if (event is WebSocketConnected) {
        add(FetchEvent());
      } else {
        add(InitialEvent());
      }
    });
    if (webSocketApiBloc.isConnected()) {
      add(FetchEvent());
    }
  }

  void _fetch(FetchEvent event, Emitter<ServiceInfoState> emit) async {
    try {
      final response = await fetcher.getServiceInfo();
      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        emit(ServiceInfoFailure(
            'Backend error: invalid response format, code: ${response.statusCode}'));
        return;
      }

      final err = respData.error();
      if (err != null) {
        emit(ServiceInfoFailure('Backend error: ${err.message}'));
        return;
      }

      final serverInfo = ServerInfo.fromJson(respData.contentMap()!);
      final live = LiveServer.createDefault();
      live.hlsHost = makeHostAndPort(serverInfo.hlsHost)!;
      live.vodsHost = makeHostAndPort(serverInfo.vodsHost)!;
      live.codsHost = makeHostAndPort(serverInfo.codsHost)!;
      emit(ServiceInfoData(live));
    } catch (e) {
      emit(ServiceInfoFailure('Backend error: $e'));
    }
  }

  void _failure(FailureEvent event, Emitter<ServiceInfoState> emit) {
    emit(ServiceInfoFailure(event.description));
  }
}
