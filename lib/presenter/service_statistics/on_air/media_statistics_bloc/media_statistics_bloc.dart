import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:flutter_common/http_response.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';

part 'media_statistics_event.dart';
part 'media_statistics_state.dart';

class MediaStatisticsBloc extends Bloc<MediaStatisticsEvent, MediaStatisticsState> {
  MediaStatisticsBloc(this.webSocketApiBloc, this.fetcher) : super(MediaStatisticsInitial()) {
    on<ListenEvent>(_listen);
    on<FetchMediaStatisticsEvent>(_fetchMediaStatistics);
    on<UpdateMediaStatsEvent>(_updateMediaStats);
    on<FailureEvent>(_failure);
    on<InitialEvent>(_init);
    add(ListenEvent());
  }

  late final StreamSubscription _subscription;
  final WebSocketApiBloc webSocketApiBloc;
  final Fetcher fetcher;

  void dispose() {
    _subscription.cancel();
  }

  void _listen(ListenEvent event, Emitter<MediaStatisticsState> emit) async {
    _subscription = webSocketApiBloc.stream.listen((state) {
      if (state is WebSocketApiMessage) {
        if (state.type == 'media_statistic_service') {
          final serverStats = MediaServerInfo.fromJson(state.data);
          add(UpdateMediaStatsEvent(serverStats));
        }
      } else if (state is WebSocketFailure) {
        emit(MediaStatisticsFailure(state.description));
      } else if (state is WebSocketConnected) {
        add(FetchMediaStatisticsEvent());
      } else {
        add(InitialEvent());
      }
    });
    if (webSocketApiBloc.isConnected()) {
      add(FetchMediaStatisticsEvent());
    }
  }

  void _fetchMediaStatistics(
      FetchMediaStatisticsEvent event, Emitter<MediaStatisticsState> emit) async {
    try {
      final response = await fetcher.getMediaServiceStatistics();
      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        emit(MediaStatisticsFailure(
            'Backend error: invalid response format, code: ${response.statusCode}'));
        return;
      }

      final err = respData.error();
      if (err != null) {
        add(FailureEvent('Backend error: ${err.message}'));
        return;
      }

      final content = respData.contentMap();
      final mediaInfo = MediaServerInfo.fromJson(content!);
      emit(MediaStatisticsData(mediaInfo: mediaInfo));
    } catch (e) {
      emit(MediaStatisticsFailure('Backend error: $e'));
    }
  }

  void _updateMediaStats(UpdateMediaStatsEvent event, Emitter<MediaStatisticsState> emit) async {
    if (state is MediaStatisticsData) {
      final oldMedia = (state as MediaStatisticsData).mediaInfo;
      emit(MediaStatisticsData(mediaInfo: _mergeMediaStats(oldMedia, event.mediaStats)));
    } else {
      emit(MediaStatisticsData(mediaInfo: event.mediaStats));
    }
  }

  void _init(InitialEvent event, Emitter<MediaStatisticsState> emit) {
    emit(MediaStatisticsInitial());
  }

  MediaServerInfo _mergeMediaStats(MediaServerInfo? old, MediaServerInfo value) {
    return MediaServerInfo(
        cpu: value.cpu,
        gpu: value.gpu,
        loadAverage: value.loadAverage,
        memoryTotal: value.memoryTotal,
        memoryFree: value.memoryFree,
        hddTotal: value.hddTotal,
        hddFree: value.hddFree,
        bandwidthIn: value.bandwidthIn,
        bandwidthOut: value.bandwidthOut,
        uptime: value.uptime,
        timestamp: value.timestamp,
        totalBytesIn: value.totalBytesIn,
        totalBytesOut: value.totalBytesOut,
        syncTime: value.syncTime ?? old?.syncTime,
        version: value.version ?? old?.version,
        project: value.project ?? old?.project,
        status: value.status ?? old?.status,
        expirationTime: value.expirationTime ?? old?.expirationTime,
        os: value.os ?? old?.os,
        onlineUsers: value.onlineUsers ?? old?.onlineUsers);
  }

  void _failure(FailureEvent event, Emitter<MediaStatisticsState> emit) {
    emit(MediaStatisticsFailure(event.description));
  }
}
