part of 'on_air_tab_bloc.dart';

abstract class OnAirTabState extends Equatable {
  const OnAirTabState();

  @override
  List<Object?> get props => [];
}

class OnAirTabInitial extends OnAirTabState {
  const OnAirTabInitial();

  @override
  List<Object> get props => [];
}

class OnAirTabConnectedState extends OnAirTabState {
  const OnAirTabConnectedState(this.show);

  final Tab show;

  @override
  List<Object?> get props => [show];
}

class OnAirTabFailure extends OnAirTabState {
  const OnAirTabFailure(this.description);

  final String description;

  @override
  List<Object?> get props => [description];
}
