import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:fastocloud_dart_models/models.dart' as models;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/errors.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/media_statistics_bloc/media_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/media_stats_layout.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/on_air_tabs_bloc/on_air_tab_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/tab_enum.dart';
import 'package:wsfastocloud/presenter/stats_header.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_on_air.dart';
import 'package:wsfastocloud/presenter/widgets/error_dialog.dart';

class ServerStatsOnAirDescWidget extends StatelessWidget {
  const ServerStatsOnAirDescWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OnAirTabBloc, OnAirTabState>(builder: (context, state) {
      final init = MediaServerInfo.createInit();
      if (state is OnAirTabConnectedState) {
        if (state.show.isStats) {
          return SizedBox(
              height: 460,
              child: BlocBuilder<MediaStatisticsBloc, MediaStatisticsState>(
                builder: (context, state) {
                  if (state is MediaStatisticsData) {
                    return Column(children: [
                      _StatsHeader(server: state.mediaInfo, connected: true),
                      MediaStatsLayout(state.mediaInfo)
                    ]);
                  } else if (state is MediaStatisticsFailure) {
                    return Column(children: [
                      _StatsHeader(server: init, connected: false),
                      ErrorDialog(error: state.description)
                    ]);
                  }
                  return Column(children: [
                    _StatsHeader(server: init, connected: false),
                    MediaStatsLayout(init)
                  ]);
                },
              ));
        }
        if (state.show.isStreams) {
          return Column(children: [
            BlocBuilder<MediaStatisticsBloc, MediaStatisticsState>(
              builder: (context, state) {
                if (state is MediaStatisticsData) {
                  return _StatsHeader(server: state.mediaInfo, connected: true);
                }
                return _StatsHeader(server: init, connected: false);
              },
            ),
            OnAirStreamsTabs(),
          ]);
        }
      }
      if (state is OnAirTabFailure) {
        final init = MediaServerInfo.createInit();
        return Column(children: [
          _StatsHeader(server: init, connected: false),
          ErrorDialog(error: state.description),
        ]);
      }
      return Column(
          children: [_StatsHeader(server: init, connected: false), MediaStatsLayout(init)]);
    });
  }
}

class _StatsHeader extends StatefulWidget {
  final models.MediaServerInfo? server;
  final bool connected;

  const _StatsHeader({Key? key, this.server, required this.connected}) : super(key: key);

  @override
  State<_StatsHeader> createState() {
    return _StatsHeaderState();
  }
}

class _StatsHeaderState extends State<_StatsHeader> {
  @override
  Widget build(BuildContext context) {
    return StatsButtons(buttons: _buttonsRow(context), showConnectInfo: false);
  }

  Widget _buttonsRow(BuildContext context) {
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        child: BlocBuilder<OnAirTabBloc, OnAirTabState>(builder: (context, state) {
          if (state is OnAirTabConnectedState) {
            return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              showStatsButton(context, !state.show.isStats),
              showStreamsButton(context, !state.show.isStreams),
              Row(children: [getLogButton(context)])
            ]);
          }
          return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            showStatsButton(context, false),
            showStreamsButton(context, false),
            Row(children: [getLogButton(context)])
          ]);
        }));
  }

  StatsButton getLogButton(BuildContext context) {
    return StatsButton(
        title: 'Get log',
        onPressed: widget.server?.status != ServerStatus.ACTIVE ? null : () => _getLog(context));
  }

  StatsButton showStatsButton(BuildContext context, bool active) {
    return StatsButton(
        title: 'Show statistics', onPressed: active ? () => _showStats(context) : null);
  }

  StatsButton showStreamsButton(BuildContext context, bool active) {
    return StatsButton(
        title: 'Show streams', onPressed: active ? () => _showStreams(context) : null);
  }

  void _getLog(BuildContext context) {
    context.read<Fetcher>().launchUrlEx('/media/logs').then((resp) {}, onError: (error) {
      showError(context, error);
    });
  }

  void _showStats(BuildContext context) {
    context.read<OnAirTabBloc>().add(ShowStatsEvent());
  }

  void _showStreams(BuildContext context) {
    context.read<OnAirTabBloc>().add(ShowStreamsEvent());
  }
}
