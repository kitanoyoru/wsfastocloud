import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:fastocloud_dart_models/models.dart' as models;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/errors.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/media_statistics_bloc/media_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/on_air/media_stats_layout.dart';
import 'package:wsfastocloud/presenter/stats_header.dart';

class ServerStatsOnAirWidget extends StatelessWidget {
  const ServerStatsOnAirWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MediaStatisticsBloc, MediaStatisticsState>(builder: (context, state) {
      if (state is MediaStatisticsData) {
        return Column(children: [
          _StatsHeader(server: state.mediaInfo, connected: true),
          MediaStatsLayout(state.mediaInfo)
        ]);
      }
      if (state is MediaStatisticsFailure) {
        return Column(children: [
          _StatsHeader(server: MediaServerInfo.createInit(), connected: false),
          Text(state.description),
          MediaStatsLayout(MediaServerInfo.createInit())
        ]);
      }
      return Column(children: [
        _StatsHeader(server: MediaServerInfo.createInit(), connected: false),
        MediaStatsLayout(MediaServerInfo.createInit())
      ]);
    });
  }
}

class _StatsHeader extends StatelessWidget {
  final models.MediaServerInfo? server;
  final bool connected;

  const _StatsHeader({Key? key, this.server, required this.connected}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StatsButtons(buttons: _buttonsRow(context), showConnectInfo: false);
  }

  Widget _buttonsRow(BuildContext context) {
    return SizedBox(
        child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [getLogButton(context)]));
  }

  StatsButton getLogButton(BuildContext context) {
    return StatsButton(
        title: 'Get log',
        onPressed: server?.status != ServerStatus.ACTIVE ? null : () => _getLog(context));
  }

  void _getLog(BuildContext context) {
    context.read<Fetcher>().launchUrlEx('/media/logs').then((resp) {}, onError: (error) {
      showError(context, error);
    });
  }
}
