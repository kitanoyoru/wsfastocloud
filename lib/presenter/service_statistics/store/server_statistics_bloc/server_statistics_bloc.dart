import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:flutter_common/http_response.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/data/services/api/models/server_statistics.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';

part 'server_statistics_event.dart';
part 'server_statistics_state.dart';

class ServiceStatisticsBloc extends Bloc<ServiceStatisticsEvent, ServiceStatisticsState> {
  ServiceStatisticsBloc(this.webSocketApiBloc, this.fetcher) : super(ServiceStatisticsInitial()) {
    on<ListenEvent>(_listen);
    on<FetchServerStatisticsEvent>(_fetch);
    on<UpdateServerStatsEvent>(_updateStats);
    on<InitialEvent>(_init);
    on<FailureEvent>(_failure);
    add(ListenEvent());
  }

  late final StreamSubscription _subscription;
  final WebSocketApiBloc webSocketApiBloc;
  final Fetcher fetcher;

  void dispose() {
    _subscription.cancel();
  }

  Future<List<VodDetails>> tmdbRequest(String name) {
    final defaults = fetcher.defaults();
    final response = fetcher.post('/server/tmdb/search', {
      'text': name,
      'stream_logo_url': defaults.streamLogoIcon,
      'background_url': defaults.backgroundUrl
    });
    return response.then((value) {
      final data = json.decode(value.body);
      final List<VodDetails> result = [];
      data['data']['movies'].forEach((s) {
        final res = VodDetails.fromJson(s);
        result.add(res);
      });
      return result;
    });
  }

  Future<VodDetails> tmdbSecondRequest(String? id) {
    final defaults = fetcher.defaults();
    final response = fetcher.post('/server/tmdb/search/$id',
        {'stream_logo_url': defaults.streamLogoIcon, 'background_url': defaults.backgroundUrl});
    return response.then((value) {
      final data = json.decode(value.body);
      final res = VodDetails.fromJson(data['data']['movie']);
      return res;
    });
  }

  void _listen(ListenEvent event, Emitter<ServiceStatisticsState> emit) async {
    _subscription = webSocketApiBloc.stream.listen((state) {
      if (state is WebSocketApiMessage) {
        if (state.type == 'statistic_service') {
          final serverStats = ServerStatistics.fromJson(state.data);
          add(UpdateServerStatsEvent(serverStats));
        }
      } else if (state is WebSocketFailure) {
        add(FailureEvent(state.description));
      } else if (state is WebSocketConnected) {
        add(FetchServerStatisticsEvent());
      } else {
        add(InitialEvent());
      }
    });
    if (webSocketApiBloc.isConnected()) {
      add(FetchServerStatisticsEvent());
    }
  }

  void _fetch(FetchServerStatisticsEvent event, Emitter<ServiceStatisticsState> emit) async {
    try {
      final url = fetcher.backendServerUrl;
      final response = await fetcher.getServerStatistics();
      final respData = httpDataResponseFromString(response.body);
      if (respData == null) {
        emit(ServiceStatisticsFailure(
            'Backend error: invalid response format, code: ${response.statusCode}'));
        return;
      }
      final err = respData.error();
      if (err != null) {
        emit(ServiceStatisticsFailure('Backend error: ${err.message}'));
        return;
      }

      final content = respData.contentMap();
      final serverInfo = ServerStatistics.fromJson(content!);
      emit(ServiceStatisticsData(serverInfo: serverInfo, url: url));
    } catch (e) {
      emit(ServiceStatisticsFailure('Backend error: $e'));
    }
  }

  void _updateStats(UpdateServerStatsEvent event, Emitter<ServiceStatisticsState> emit) async {
    if (state is ServiceStatisticsData) {
      final old = (state as ServiceStatisticsData).serverInfo;
      final url = (state as ServiceStatisticsData).url;
      emit(ServiceStatisticsData(serverInfo: _mergeServer(old, event.serverInfo), url: url));
    } else {
      emit(ServiceStatisticsData(serverInfo: event.serverInfo));
    }
  }

  void _init(InitialEvent event, Emitter<ServiceStatisticsState> emit) {
    emit(ServiceStatisticsInitial());
  }

  ServerStatistics _mergeServer(ServerStatistics old, ServerStatistics value) {
    return ServerStatistics(
      cpu: value.cpu,
      gpu: value.gpu,
      memoryTotal: value.memoryTotal,
      memoryFree: value.memoryFree,
      uptime: value.uptime ?? old.uptime,
      timestamp: value.timestamp ?? old.timestamp,
      expirationTime: value.expirationTime ?? old.expirationTime,
      version: value.version ?? old.version,
      project: value.project ?? old.project,
      hddTotal: value.hddTotal,
      hddFree: value.hddFree,
      bandwidthIn: value.bandwidthIn,
      bandwidthOut: value.bandwidthOut,
      totalBytesIn: value.totalBytesIn,
      totalBytesOut: value.totalBytesOut,
      os: value.os ?? old.os,
    );
  }

  void _failure(FailureEvent event, Emitter<ServiceStatisticsState> emit) {
    emit(ServiceStatisticsFailure(event.description));
  }
}
