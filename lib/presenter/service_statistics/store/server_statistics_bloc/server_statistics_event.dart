part of 'server_statistics_bloc.dart';

abstract class ServiceStatisticsEvent extends Equatable {
  const ServiceStatisticsEvent();

  @override
  List<Object?> get props => [];
}

class ListenEvent extends ServiceStatisticsEvent {
  const ListenEvent();

  @override
  List<Object?> get props => [];
}

class InitialEvent extends ServiceStatisticsEvent {
  const InitialEvent();

  @override
  List<Object?> get props => [];
}

class FetchServerStatisticsEvent extends ServiceStatisticsEvent {
  const FetchServerStatisticsEvent();

  @override
  List<Object?> get props => [];
}

class UpdateServerStatsEvent extends ServiceStatisticsEvent {
  final ServerStatistics serverInfo;

  const UpdateServerStatsEvent(this.serverInfo);

  @override
  List<Object?> get props => [serverInfo];
}

class FailureEvent extends ServiceStatisticsEvent {
  final String description;

  const FailureEvent(this.description);

  @override
  List<Object?> get props => [description];
}
