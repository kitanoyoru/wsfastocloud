part of 'server_statistics_bloc.dart';

abstract class ServiceStatisticsState extends Equatable {
  const ServiceStatisticsState();

  @override
  List<Object?> get props => [];
}

class ServiceStatisticsInitial extends ServiceStatisticsState {
  const ServiceStatisticsInitial();

  @override
  List<Object?> get props => [];
}

class ServiceStatisticsData extends ServiceStatisticsState {
  final ServerStatistics serverInfo;
  final String? url;

  ServiceStatisticsData({
    this.url,
    required this.serverInfo,
  });

  @override
  List<Object?> get props => [serverInfo, url];
}

class ServiceStatisticsFailure extends ServiceStatisticsState {
  final String description;

  const ServiceStatisticsFailure(this.description);

  @override
  List<Object?> get props => [description];
}
