import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:wsfastocloud/data/services/api/models/server_statistics.dart';
import 'package:wsfastocloud/presenter/server_stats.dart';

class ServerStatsLayout extends StatelessWidget {
  final ServerStatistics serverInfo;

  const ServerStatsLayout(this.serverInfo);

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(desktop: _desktop(), mobile: _mobile());
  }

  Widget _desktop() {
    const height1 = TILE_HEIGHT_1;
    const height2 = TILE_HEIGHT_4;
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Row(children: <Widget>[
        // Expanded(child: _status(height: height1)),
        Expanded(flex: 2, child: _uptime(height: height1)),
        Expanded(flex: 3, child: _version(height: height1)),
        Expanded(flex: 3, child: _network(height: height1))
      ]),
      Row(children: <Widget>[
        Expanded(flex: 11, child: _timestamp(height: height2)),
        Expanded(flex: 20, child: _expTime(height: height2)),
        // Expanded(flex: 9, child: _onlineUsers(height: height2)),
        Expanded(flex: 20, child: _os(height: height2))
      ]),
      Row(children: <Widget>[
        Expanded(flex: 2, child: _memory()),
        Expanded(flex: 2, child: _hdd()),
        Expanded(child: _cpu()),
        Expanded(child: _gpu())
      ])
    ]);
  }

  Widget _mobile() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(children: [
              //Expanded(child: _status()),
              Expanded(flex: 2, child: _uptime())
            ]),
            Row(children: [Expanded(child: _expTime(height: CIRCLE_HEIGHT))]),
            Row(children: [Expanded(child: _network(height: TILE_HEIGHT_1))]),
            Row(children: [
              Expanded(child: _timestamp()),
              Expanded(child: _version(height: TILE_HEIGHT_4))
            ]),
            Row(children: [
              // Expanded(child: _onlineUsers()),
              Expanded(child: _os(height: TILE_HEIGHT_4))
            ]),
            Row(children: [Expanded(flex: 2, child: _memory()), Expanded(child: _cpu())]),
            Row(children: [Expanded(flex: 2, child: _hdd()), Expanded(child: _gpu())])
          ],
        ),
      ),
    );
  }

  // Widget _status({double? height}) {
  //   return StatusTile(serverInfo.status, height: height);
  // }

  Widget _timestamp({double? height}) {
    return TimestampTile(serverInfo.timestamp ?? 0, height: height);
  }

  Widget _version({double? height}) {
    return VersionTile(serverInfo.project, serverInfo.version, height: height);
  }

  // Widget _onlineUsers({double? height}) {
  //   final online = serverInfo.onlineUsers;
  //   return OnlineUsersServerStatsTile.server(
  //       online?.daemon, online?.cods, online?.vods, online?.http,
  //       height: height);
  // }

  Widget _cpu() {
    return HardwareStatsTile.cpu(serverInfo.cpu ?? 0);
  }

  Widget _gpu() {
    return HardwareStatsTile.gpu(serverInfo.gpu ?? 0);
  }

  Widget _memory() {
    return MemoryStatsTile.memory(serverInfo.memoryTotal ?? 0, serverInfo.memoryFree ?? 0);
  }

  Widget _hdd() {
    return MemoryStatsTile.hdd(serverInfo.hddTotal ?? 0, serverInfo.hddFree ?? 0);
  }

  Widget _network({double? height}) {
    return NetworkStatsTile(serverInfo.bandwidthIn, serverInfo.bandwidthOut, height: height);
  }

  Widget _uptime({double? height}) {
    return UptimeStatsTile(serverInfo.uptime ?? 0, height: height);
  }

  Widget _expTime({double? height}) {
    return ExpDateTile(serverInfo.expirationTime, height: height);
  }

  Widget _os({double? height}) {
    return OsMediaStatsTile(serverInfo.os, height: height);
  }
}
