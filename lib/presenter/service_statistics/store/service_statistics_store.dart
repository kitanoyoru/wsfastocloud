import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/errors.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/data/services/api/models/server_statistics.dart';
import 'package:wsfastocloud/presenter/service_info_bloc/service_info_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/server_statistics_bloc/server_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/server_stats_layout.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/store_tabs_bloc/store_tabs_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/tab_enum.dart';
import 'package:wsfastocloud/presenter/stats_header.dart';
import 'package:wsfastocloud/presenter/streams/store/seasons_data_source.dart';
import 'package:wsfastocloud/presenter/streams/store/seasons_store_bloc/seasons_store_bloc.dart';
import 'package:wsfastocloud/presenter/streams/store/serials_data_source.dart';
import 'package:wsfastocloud/presenter/streams/store/serials_store_bloc/serials_store_bloc.dart';
import 'package:wsfastocloud/presenter/streams/store/store_episodes_widget.dart';
import 'package:wsfastocloud/presenter/streams/store/store_live_widget.dart';
import 'package:wsfastocloud/presenter/streams/store/store_seasons_widget.dart';
import 'package:wsfastocloud/presenter/streams/store/store_serials_widget.dart';
import 'package:wsfastocloud/presenter/streams/store/store_vods_widget.dart';
import 'package:wsfastocloud/presenter/widgets/error_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/select_file_dialog.dart';

class ServerStatsStoreDescWidget extends StatefulWidget {
  final MediaServerInfo? mediaServerInfo;
  final WsMode? mode;

  ServerStatsStoreDescWidget({Key? key, required this.mediaServerInfo, required this.mode})
      : super(key: key);

  @override
  State<ServerStatsStoreDescWidget> createState() {
    return _ServerStatsStoreDescWidgetState();
  }
}

class _ServerStatsStoreDescWidgetState extends State<ServerStatsStoreDescWidget> {
  LiveServer? server;

  @override
  Widget build(BuildContext context) {
    final _init = ServerStatistics.createInit();
    return BlocBuilder<StoreTabsBloc, StoreTabsState>(builder: (context, state) {
      if (state is StoreTabsConnectedState) {
        if (state.show.isStats) {
          return SizedBox(
              height: 460,
              child: Column(children: [
                _StatsHeader(connected: true, mode: widget.mode),
                BlocBuilder<ServiceStatisticsBloc, ServiceStatisticsState>(
                    builder: (context, state) {
                  if (state is ServiceStatisticsData) {
                    return ServerStatsLayout(state.serverInfo);
                  } else if (state is ServiceStatisticsFailure) {
                    return ErrorDialog(error: state.description);
                  } else {
                    return ServerStatsLayout(_init);
                  }
                })
              ]));
        }
        if (state.show.isSeasons) {
          return Column(children: [
            _StatsHeader(connected: true, mode: widget.mode),
            BlocBuilder<SeasonsBloc, SeasonsState>(builder: (context, state) {
              if (state is SeasonsDataState) {
                return SeasonsListLayout(state.seasonsSource);
              } else if (state is SeasonsFailureState) {
                return ErrorDialog(error: state.description);
              }
              return SeasonsListLayout(SeasonsSource([]));
            })
          ]);
        }
        if (state.show.isSerials) {
          return Column(children: [
            _StatsHeader(connected: true, mode: widget.mode),
            BlocBuilder<SerialsBloc, SerialsState>(builder: (context, state) {
              if (state is SerialsDataState) {
                return SerialsListLayout(state.serialsSource);
              } else if (state is SerialsFailureState) {
                return ErrorDialog(error: state.description);
              }
              return SerialsListLayout(SerialsSource([]));
            })
          ]);
        }
        if (state.show.isStreams) {
          return Column(children: [
            _StatsHeader(connected: true, mode: widget.mode),
            BlocBuilder<ServiceInfoBloc, ServiceInfoState>(builder: (context, state) {
              if (state is ServiceInfoData) {
                return StoreLiveWidget(state.config.copyWith(widget.mediaServerInfo!),
                    mode: widget.mode);
              }
              return StoreLiveWidget(server, mode: widget.mode);
            })
          ]);
        }
        if (state.show.isEpisodes) {
          return SizedBox(
              height: 842,
              child: Column(children: [
                _StatsHeader(connected: true, mode: widget.mode),
                BlocBuilder<ServiceInfoBloc, ServiceInfoState>(builder: (context, state) {
                  if (state is ServiceInfoData) {
                    return StoreSeriesWidget(state.config.copyWith(widget.mediaServerInfo!),
                        mode: widget.mode);
                  }
                  return StoreSeriesWidget(server, mode: widget.mode);
                })
              ]));
        }
        if (state.show.isVods) {
          return SizedBox(
              height: 842,
              child: Column(children: [
                _StatsHeader(connected: true, mode: widget.mode),
                BlocBuilder<ServiceInfoBloc, ServiceInfoState>(builder: (context, state) {
                  if (state is ServiceInfoData) {
                    return StoreVodsWidget(state.config.copyWith(widget.mediaServerInfo!),
                        mode: widget.mode);
                  }
                  return StoreVodsWidget(server, mode: widget.mode);
                })
              ]));
        }
      }
      if (state is StoreTabsFailure) {
        return Column(children: [
          _StatsHeader(connected: false, mode: widget.mode),
          ErrorDialog(error: state.description),
        ]);
      }
      return Column(children: [
        _StatsHeader(connected: false, mode: widget.mode),
        ServerStatsLayout(ServerStatistics.createInit())
      ]);
    });
  }
}

class _StatsHeader extends StatefulWidget {
  final bool connected;
  final WsMode? mode;

  const _StatsHeader({Key? key, required this.connected, required this.mode}) : super(key: key);

  @override
  State<_StatsHeader> createState() {
    return _StatsHeaderState();
  }
}

class _StatsHeaderState extends State<_StatsHeader> {
  @override
  Widget build(BuildContext context) {
    return StatsButtons(buttons: _buttonsRow(context), showConnectInfo: false);
  }

  Widget _buttonsRow(BuildContext context) {
    return _desktop();
  }

  Widget _desktop() {
    return SizedBox(
        width: MediaQuery.of(context).size.width,
        child: BlocBuilder<StoreTabsBloc, StoreTabsState>(builder: (context, state) {
          if (state is StoreTabsConnectedState) {
            return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              showStatsButton(context, !state.show.isStats),
              showStreamsButton(context, !state.show.isStreams),
              showVodsButton(context, !state.show.isVods),
              if (widget.mode == WsMode.IPTV) episodesButton(context, !state.show.isEpisodes),
              if (widget.mode == WsMode.IPTV) seasonsButton(context, !state.show.isSeasons),
              if (widget.mode == WsMode.IPTV) serialsButton(context, !state.show.isSerials),
              Row(children: [playlistButton(context), uploadButton(context)])
            ]);
          }
          return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            showStatsButton(context, false),
            showStreamsButton(context, false),
            showVodsButton(context, false),
            if (widget.mode == WsMode.IPTV) episodesButton(context, false),
            if (widget.mode == WsMode.IPTV) seasonsButton(context, false),
            if (widget.mode == WsMode.IPTV) serialsButton(context, false),
            Row(children: [playlistButton(context), uploadButton(context)])
          ]);
        }));
  }

  StatsButton playlistButton(BuildContext context) {
    return StatsButton(
        title: 'Playlist', onPressed: widget.connected ? () => _getPlaylist(context) : null);
  }

  StatsButton showStatsButton(BuildContext context, bool active) {
    return StatsButton(
        title: 'Show statistics', onPressed: active ? () => _showStats(context) : null);
  }

  StatsButton showStreamsButton(BuildContext context, bool active) {
    return StatsButton(
        title: 'Show streams', onPressed: active ? () => _showStreams(context) : null);
  }

  StatsButton showVodsButton(BuildContext context, bool active) {
    return StatsButton(title: 'Show vods', onPressed: active ? () => _showVods(context) : null);
  }

  StatsButton episodesButton(BuildContext context, bool active) {
    return StatsButton(
        title: 'Show episodes', onPressed: active ? () => _showEpisodes(context) : null);
  }

  StatsButton seasonsButton(BuildContext context, bool active) {
    return StatsButton(
        title: 'Show seasons', onPressed: active ? () => _showSeasons(context) : null);
  }

  StatsButton serialsButton(BuildContext context, bool active) {
    return StatsButton(
        title: 'Show series', onPressed: active ? () => _showSerials(context) : null);
  }

  StatsButton uploadButton(BuildContext context) {
    return StatsButton(
        title: 'Upload media', onPressed: widget.connected ? () => _upload(context) : null);
  }

  // buttons
  void _getPlaylist(BuildContext context) {
    context.read<Fetcher>().launchUrlEx('/server/playlist.m3u').then((resp) {}, onError: (error) {
      showError(context, error);
    });
  }

  void _showStats(BuildContext context) {
    context.read<StoreTabsBloc>().add(ShowStatsEvent());
  }

  void _showStreams(BuildContext context) {
    context.read<StoreTabsBloc>().add(ShowStreamsEvent());
  }

  void _showVods(BuildContext context) {
    context.read<StoreTabsBloc>().add(ShowVodsEvent());
  }

  void _showSeasons(BuildContext context) {
    context.read<StoreTabsBloc>().add(ShowSeasonsEvent());
  }

  void _showEpisodes(BuildContext context) {
    context.read<StoreTabsBloc>().add(ShowEpisodesEvent());
  }

  void _showSerials(BuildContext context) {
    context.read<StoreTabsBloc>().add(ShowSerialsEvent());
  }

  void _upload(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return SelectFileDialog();
        });
  }
}
