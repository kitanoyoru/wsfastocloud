import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/errors.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/data/services/api/models/server_statistics.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/server_statistics_bloc/server_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/server_stats_layout.dart';
import 'package:wsfastocloud/presenter/stats_header.dart';
import 'package:wsfastocloud/presenter/widgets/select_file_dialog.dart';

class ServerStatsStoreWidget extends StatefulWidget {
  ServerStatsStoreWidget({Key? key}) : super(key: key);

  @override
  State<ServerStatsStoreWidget> createState() {
    return _ServerStatsStoreWidgetState();
  }
}

class _ServerStatsStoreWidgetState extends State<ServerStatsStoreWidget> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ServiceStatisticsBloc, ServiceStatisticsState>(builder: (context, state) {
      if (state is ServiceStatisticsData) {
        return Column(children: [
          _StatsHeader(server: state.serverInfo, connected: true),
          ServerStatsLayout(state.serverInfo)
        ]);
      }
      if (state is ServiceStatisticsFailure) {
        final init = ServerStatistics.createInit();
        return Column(children: [
          _StatsHeader(server: init, connected: false),
          Text(state.description),
          ServerStatsLayout(init)
        ]);
      }
      final init = ServerStatistics.createInit();
      return Column(
          children: [_StatsHeader(server: init, connected: false), ServerStatsLayout(init)]);
    });
  }
}

class _StatsHeader extends StatefulWidget {
  final ServerStatistics? server;
  final bool connected;

  const _StatsHeader({Key? key, this.server, required this.connected}) : super(key: key);

  @override
  State<_StatsHeader> createState() {
    return _StatsHeaderState();
  }
}

class _StatsHeaderState extends State<_StatsHeader> {
  @override
  Widget build(BuildContext context) {
    return StatsButtons(buttons: _buttonsRow(context), showConnectInfo: false);
  }

  Widget _buttonsRow(BuildContext context) {
    return _mobile();
  }

  _mobile() {
    return SizedBox(
        child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [playlistButton(context), uploadButton(context)]));
  }

  StatsButton playlistButton(BuildContext context) {
    return StatsButton(
        title: 'Playlist', onPressed: widget.connected ? () => _getPlaylist(context) : null);
  }

  StatsButton uploadButton(BuildContext context) {
    return StatsButton(
        title: 'Upload media', onPressed: widget.connected ? () => _upload(context) : null);
  }

  // buttons
  void _getPlaylist(BuildContext context) {
    context.read<Fetcher>().launchUrlEx('/server/playlist.m3u').then((resp) {}, onError: (error) {
      showError(context, error);
    });
  }

  void _upload(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return SelectFileDialog();
        });
  }
}
