import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';
import 'package:wsfastocloud/presenter/service_statistics/tab_enum.dart';

part 'store_tabs_event.dart';
part 'store_tabs_state.dart';

class StoreTabsBloc extends Bloc<StoreTabsEvent, StoreTabsState> {
  StoreTabsBloc(this.webSocketApiBloc) : super(StoreTabsInitial()) {
    on<ListenEvent>(_listen);
    on<InitialEvent>(_init);
    on<FailureEvent>(_failure);
    on<ConnectEvent>(_connect);
    on<ShowStatsEvent>(_stats);
    on<ShowStreamsEvent>(_streams);
    on<ShowVodsEvent>(_vods);
    on<ShowEpisodesEvent>(_episodes);
    on<ShowSeasonsEvent>(_seasons);
    on<ShowSerialsEvent>(_serials);
    add(ListenEvent());
  }

  late final StreamSubscription _subscription;
  final WebSocketApiBloc webSocketApiBloc;

  void dispose() {
    _subscription.cancel();
  }

  void _listen(ListenEvent event, Emitter<StoreTabsState> emit) async {
    _subscription = webSocketApiBloc.stream.listen((state) {
      if (state is WebSocketConnected) {
        add(ConnectEvent());
      } else if (state is WebSocketFailure) {
        add(FailureEvent(state.description));
      } else if (state is WebSocketApiMessage) {
      } else {
        add(InitialEvent());
      }
    });
    if (webSocketApiBloc.isConnected()) {
      add(ConnectEvent());
    }
  }

  void _init(InitialEvent event, Emitter<StoreTabsState> emit) {
    emit(StoreTabsInitial());
  }

  void _connect(ConnectEvent event, Emitter<StoreTabsState> emit) {
    emit(StoreTabsConnectedState(Tab.stats));
  }

  void _stats(ShowStatsEvent event, Emitter<StoreTabsState> emit) {
    emit(StoreTabsConnectedState(Tab.stats));
  }

  void _streams(ShowStreamsEvent event, Emitter<StoreTabsState> emit) {
    emit(StoreTabsConnectedState(Tab.streams));
  }

  void _vods(ShowVodsEvent event, Emitter<StoreTabsState> emit) {
    emit(StoreTabsConnectedState(Tab.vods));
  }

  void _episodes(ShowEpisodesEvent event, Emitter<StoreTabsState> emit) {
    emit(StoreTabsConnectedState(Tab.episodes));
  }

  void _seasons(ShowSeasonsEvent event, Emitter<StoreTabsState> emit) {
    emit(StoreTabsConnectedState(Tab.seasons));
  }

  void _serials(ShowSerialsEvent event, Emitter<StoreTabsState> emit) {
    emit(StoreTabsConnectedState(Tab.serials));
  }

  void _failure(FailureEvent event, Emitter<StoreTabsState> emit) {
    emit(StoreTabsFailure(event.description));
  }
}
