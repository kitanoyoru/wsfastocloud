import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:wsfastocloud/locator.dart';
import 'package:wsfastocloud/presenter/copy_link_button.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/server_statistics_bloc/server_statistics_bloc.dart';

class StatsButtons extends StatefulWidget {
  final Widget buttons;
  final bool showConnectInfo;

  const StatsButtons({Key? key, required this.showConnectInfo, required this.buttons})
      : super(key: key);

  @override
  State<StatsButtons> createState() => _StatsButtonsState();
}

class _StatsButtonsState extends State<StatsButtons> {
  String version = '';

  @override
  void initState() {
    final package = locator<PackageManager>();
    version = package.version();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(mobile: mobile(), desktop: desktop());
  }

  Widget mobile() {
    return BlocBuilder<ServiceStatisticsBloc, ServiceStatisticsState>(
      builder: (context, state) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                if (widget.showConnectInfo)
                  Text(
                    'Version: $version',
                    style: TextStyle(fontSize: 8),
                  ),
                if (state is ServiceStatisticsData && widget.showConnectInfo)
                  Text(
                    'Connected to: ${state.url}',
                    style: TextStyle(fontSize: 8),
                  ),
                if (state is ServiceStatisticsData && widget.showConnectInfo)
                  SizedBox(height: 30, child: CopyLinkButton()),
              ],
            ),
            _buttonsRow(),
          ],
        );
      },
    );
  }

  Widget desktop() {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: BlocBuilder<ServiceStatisticsBloc, ServiceStatisticsState>(
          builder: (context, state) {
            return Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (widget.showConnectInfo) Text('Version: $version'),
                  if (state is ServiceStatisticsData && widget.showConnectInfo)
                    Row(
                      children: [
                        Text('Connected to: ${state.url}'),
                        CopyLinkButton(),
                      ],
                    ),
                ],
              ),
              Expanded(child: _buttonsRow())
            ]);
          },
        ));
  }

  Widget _buttonsRow() {
    return Align(
      alignment: Alignment.centerRight,
      child: FittedBox(
          child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Row(children: _wrapButtons()))),
    );
  }

  List<Widget> _wrapButtons() {
    final List<Widget> result = [];
    result.add(Padding(padding: const EdgeInsets.symmetric(horizontal: 8), child: widget.buttons));

    return result;
  }
}

class StatsButton extends StatelessWidget {
  final String title;
  final VoidCallback? onPressed;

  const StatsButton({Key? key, required this.title, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: FlatButtonEx.filled(text: title, onPressed: onPressed));
  }
}
