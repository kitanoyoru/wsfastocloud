import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:provider/provider.dart';
import 'package:wsfastocloud/data/services/api/models/stream_statistics.dart';
import 'package:wsfastocloud/presenter/streams/on_air/stream_data_source.dart';
import 'package:wsfastocloud/presenter/streams/on_air/stream_statistics_bloc/stream_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/widgets/change_source_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/streams/actions.dart';

class LiveWidget extends StatefulWidget {
  final LiveStreamDataSource _liveDataSource;

  const LiveWidget(this._liveDataSource);

  @override
  _LiveWidgetState createState() {
    return _LiveWidgetState();
  }
}

class _LiveWidgetState extends State<LiveWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DataTableEx(widget._liveDataSource,
        DataTableSearchHeader(source: widget._liveDataSource), _liveActions);
  }

  List<Widget> _liveActions() {
    if (widget._liveDataSource.selectedRowCount == 0) {
      return [];
    }

    final bloc = context.read<StreamsStatisticsBloc>();
    final streams = widget._liveDataSource.selectedItems();
    List<StreamStatistics> changers = [];
    for (var stream in streams) {
      if (stream.type == StreamType.CHANGER_ENCODE || stream.type == StreamType.CHANGER_RELAY) {
        changers.add(stream);
      }
    }

    return [
      Row(children: [
        StreamActionIcon.stop(() => _stop(bloc, streams)),
        StreamActionIcon.kill(() => _kill(bloc, streams)),
        StreamActionIcon.restart(() => _restart(bloc, streams)),
        if (changers.isNotEmpty) StreamActionIcon.changeStream(() => _changeSource(bloc, changers)),
        StreamActionIcon.getLog(() => _getLog(bloc, streams)),
        StreamActionIcon.getConfig(() => _getConfig(bloc, streams)),
        StreamActionIcon.getPipeline(() => _getPipeLine(bloc, streams))
      ])
    ];
  }

  void _stop(StreamsStatisticsBloc bloc, List<StreamStatistics> streams) {
    for (var stream in streams) {
      bloc.stopStream(stream.id);
    }
  }

  void _kill(StreamsStatisticsBloc bloc, List<StreamStatistics> streams) {
    for (var stream in streams) {
      bloc.killStream(stream.id);
    }
  }

  void _restart(StreamsStatisticsBloc bloc, List<StreamStatistics> streams) {
    for (var stream in streams) {
      bloc.restartStream(stream.id);
    }
  }

  void _changeSource(StreamsStatisticsBloc bloc, List<StreamStatistics> streams) {
    for (var stream in streams) {
      final resp = bloc.getConfigStreamMap(stream.id);
      resp.then((config) {
        final input = config[HardwareStream.INPUT_FIELD];
        List<InputUrl> inputs = [];
        input.forEach((element) => inputs.add(makeInputUrl(element)));

        final result = showDialog<int>(
            context: context,
            builder: (context) {
              return ChangeSourceDialog(inputs);
            });
        result.then((value) {
          if (value == null) {
            return;
          }

          bloc.changeSource(stream.id, value);
        });
      });
    }
  }

  void _getLog(StreamsStatisticsBloc bloc, List<StreamStatistics> streams) {
    for (var stream in streams) {
      bloc.getLogStream(stream.id);
    }
  }

  void _getConfig(StreamsStatisticsBloc bloc, List<StreamStatistics> streams) {
    for (var stream in streams) {
      final resp = bloc.getConfigStreamString(stream.id);

      resp.then((body) {
        TextEditingController _textController = TextEditingController(text: body);
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return SimpleDialog(title: const Text('Get config'), children: <Widget>[
                Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(children: [
                      TextField(
                          keyboardType: TextInputType.multiline,
                          maxLines: null,
                          controller: _textController),
                      SizedBox(height: 5.0),
                      FlatButtonEx.filled(
                          text: 'Copy',
                          onPressed: () {
                            Clipboard.setData(ClipboardData(text: _textController.text));
                            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                              content: Text('Copied'),
                            ));
                          }),
                      SizedBox(height: 5.0),
                      FlatButtonEx.filled(
                          text: 'Close',
                          onPressed: () {
                            Navigator.of(context).pop();
                          })
                    ]))
              ]);
            });
      }, onError: (error) {
        showError(context, error);
      });
    }
  }

  void _getPipeLine(StreamsStatisticsBloc bloc, List<StreamStatistics> streams) {
    for (var stream in streams) {
      bloc.getPipeLineStream(stream.id);
    }
  }
}
