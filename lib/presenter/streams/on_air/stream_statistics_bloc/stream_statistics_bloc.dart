import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:wsfastocloud/data/services/api/models/stream_statistics.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';
import 'package:wsfastocloud/domain/repositories/stream_repository.dart';
import 'package:wsfastocloud/presenter/streams/on_air/stream_data_source.dart';

part 'stream_statistics_event.dart';
part 'stream_statistics_state.dart';

class StreamsStatisticsBloc extends Bloc<StreamsStatisticsEvent, StreamsStatisticsState> {
  StreamsStatisticsBloc(this.webSocketApiBloc, this.streamRepository)
      : super(StreamsStatisticsInitial()) {
    on<ListenStatisticsEvent>(_listen);
    on<UpdateStreamEvent>(_updateStream);
    on<RemoveStreamEvent>(_removeStream);
    on<FetchStatisticEvent>(_fetch);
    on<InitialEvent>(_init);
    add(ListenStatisticsEvent());
  }

  late final StreamSubscription _subscription;

  final WebSocketApiBloc webSocketApiBloc;
  final StreamRepository streamRepository;
  final LiveStreamDataSource _liveDataSource = LiveStreamDataSource([]);

  Future<void> stopStream(String id) {
    return streamRepository.stopStream(id);
  }

  Future<String> getConfigStreamString(String id) {
    return streamRepository.getConfigStreamString(id);
  }

  Future<Map<String, dynamic>> getConfigStreamMap(String id) {
    return streamRepository.getConfigStreamMap(id);
  }

  Future<void> changeSource(String streamId, int index) {
    return streamRepository.changeSource(streamId, index);
  }


  Future<void> getLogStream(String id) {
    return streamRepository.getLogStream(id);
  }

  Future<void> getPipeLineStream(String id) {
    return streamRepository.getPipeLineStream(id);
  }

  Future<void> killStream(String id) {
    return streamRepository.stopStream(id, true);
  }

  Future<void> restartStream(String id) {
    return streamRepository.restartStream(id);
  }

  void dispose() {
    _subscription.cancel();
  }

  void _init(InitialEvent event, Emitter<StreamsStatisticsState> emit) {
    _liveDataSource.clearItems();
    emit(StreamsStatisticsInitial());
  }

  void _listen(ListenStatisticsEvent event, Emitter<StreamsStatisticsState> emit) async {
    _subscription = webSocketApiBloc.stream.listen((event) {
      if (event is WebSocketApiMessage) {
        const wsStatStream = 'statistic_stream';
        const wsQuitStream = 'quit_status_stream';
        if (event.type == wsStatStream) {
          final stream = StreamStatistics.fromJson(event.data);
          add(UpdateStreamEvent(stream));
        } else if (event.type == wsQuitStream) {
          final streamId = event.data['id'];
          add(RemoveStreamEvent(streamId));
        }
      } else if (event is WebSocketFailure) {
        emit(StreamsStatisticsFailure(event.description));
      } else if (event is WebSocketConnected) {
        add(FetchStatisticEvent());
      } else {
        add(InitialEvent());
      }
    });

    if (webSocketApiBloc.isConnected()) {
      add(FetchStatisticEvent());
    }
  }

  void _updateStream(UpdateStreamEvent event, Emitter<StreamsStatisticsState> emit) {
    if (state is! StreamsStatisticsData) {
      _liveDataSource.addItems([event.stream]);
      emit(StreamsStatisticsData.create([event.stream], _liveDataSource));
      return;
    }

    final streams = (state as StreamsStatisticsData).streams;

    if (streams.containsKey(event.stream.id)) {
      _liveDataSource.updateItem(event.stream);
      emit(StreamsStatisticsData.update(
        streams,
        event.stream,
        _liveDataSource,
      ));
    } else {
      _liveDataSource.addItem(event.stream);
      emit(StreamsStatisticsData.add(streams, event.stream, _liveDataSource));
    }
  }

  void _removeStream(RemoveStreamEvent event, Emitter<StreamsStatisticsState> emit) {
    if (state is StreamsStatisticsData) {
      final streams = (state as StreamsStatisticsData).streams;
      final removed = streams[event.id]!;
      _liveDataSource.removeItem(removed);
      emit(StreamsStatisticsData.remove(streams, removed, _liveDataSource));
    }
  }

  void _fetch(FetchStatisticEvent event, Emitter<StreamsStatisticsState> emit) async {
    try {
      final streams = await streamRepository.getStreamsStatistics();
      _liveDataSource.addItems(streams);

      emit(StreamsStatisticsInitial());
      emit(StreamsStatisticsData.create(streams, _liveDataSource));
    } catch (e) {
      emit(StreamsStatisticsFailure('Backend error: $e'));
    }
  }
}
