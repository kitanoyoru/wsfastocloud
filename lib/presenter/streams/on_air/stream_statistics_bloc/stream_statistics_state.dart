part of 'stream_statistics_bloc.dart';

abstract class StreamsStatisticsState extends Equatable {
  const StreamsStatisticsState();

  @override
  List<Object?> get props => [];
}

class StreamsStatisticsInitial extends StreamsStatisticsState {
  const StreamsStatisticsInitial();

  @override
  List<Object?> get props => [];
}

class StreamsStatisticsData extends StreamsStatisticsState {
  final Map<String, StreamStatistics> streams;
  final LiveStreamDataSource dataSource;
  final List<StreamStatistics>? addedItems;
  final List<StreamStatistics>? updatedItems;
  final List<StreamStatistics>? removedItems;

  const StreamsStatisticsData(this.streams, this.dataSource,
      {this.addedItems, this.updatedItems, this.removedItems});

  factory StreamsStatisticsData.create(List<StreamStatistics> items, LiveStreamDataSource data) {
    final streamsMap =
        Map<String, StreamStatistics>.fromIterable(items, key: (stream) => stream.id);

    return StreamsStatisticsData(streamsMap, data, addedItems: items);
  }

  factory StreamsStatisticsData.add(
      Map<String, dynamic> old, StreamStatistics item, LiveStreamDataSource data) {
    final streamsMap = Map<String, StreamStatistics>.from(old);

    streamsMap[item.id] = item;

    return StreamsStatisticsData(streamsMap, data, addedItems: [item]);
  }

  factory StreamsStatisticsData.update(
      Map<String, dynamic> old, StreamStatistics item, LiveStreamDataSource data) {
    final streamsMap = Map<String, StreamStatistics>.from(old);

    streamsMap[item.id] = item;

    return StreamsStatisticsData(streamsMap, data, updatedItems: [item]);
  }

  factory StreamsStatisticsData.remove(
      Map<String, dynamic> old, StreamStatistics item, LiveStreamDataSource data) {
    final streamsMap = Map<String, StreamStatistics>.from(old);

    streamsMap.remove(item.id);

    return StreamsStatisticsData(streamsMap, data, removedItems: [item]);
  }

  @override
  List<Object?> get props => [streams, dataSource, addedItems, updatedItems, removedItems];
}

class StreamsStatisticsFailure extends StreamsStatisticsState {
  final String description;

  const StreamsStatisticsFailure(this.description);

  @override
  List<Object?> get props => [description];
}
