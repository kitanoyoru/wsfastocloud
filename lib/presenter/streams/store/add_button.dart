import 'package:fastocloud_dart_models/models.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:wsfastocloud/intl/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/streams/store/make_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/add_edit_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/base_page.dart';

class AddStreamButton extends StatelessWidget {
  final LiveServer? server;
  final IStreamEditor editor;
  final List<StreamType> types;
  final WsMode? mode;
  final bool isSerial;

  const AddStreamButton(this.server, this.editor, this.types,
      {required this.mode, required this.isSerial});

  @override
  Widget build(BuildContext context) {
    return FlatButtonEx.filled(
        text: 'Add',
        onPressed: server != null
            ? () {
                _onTap(context);
              }
            : null);
  }

  void _onTap(BuildContext context) {
    showDialog(
        context: context,
        builder: (context) {
          return StreamTypeDialog(types, isSerial);
        }).then((_type) {
      if (_type == null) {
        return;
      }
      showDialog(
          context: context,
          builder: (context) {
            return makeAddStreamDialog(server!, editor, _type, mode, isSerial);
          });
    });
  }
}

class StreamTypeDialog extends StatefulWidget {
  final List<StreamType> types;
  final bool isSerial;

  const StreamTypeDialog(this.types, this.isSerial);

  @override
  _StreamTypeDialogState createState() {
    return _StreamTypeDialogState();
  }
}

class _StreamTypeDialogState extends State<StreamTypeDialog> {
  late StreamType _type = widget.types[0];

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      final cbConvertEpisode = (StreamType type) {
        if (type == StreamType.VOD_PROXY) {
          return 'Episode External';
        } else if (type == StreamType.VOD_RELAY) {
          return 'Episode Restream';
        }
        return 'Episode Encode';
      };
      return AlertDialog(
          title: const Text('Stream type'),
          contentPadding: const EdgeInsets.symmetric(vertical: 8),
          content: SizedBox(
              width: AddEditDialog.getNarrow(sizingInformation.isMobile),
              child: SingleChildScrollView(
                  child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: List<Widget>.generate(widget.types.length, (index) {
                        final current = widget.types[index];
                        return RadioListTile<StreamType>(
                            title: widget.isSerial
                                ? Text(cbConvertEpisode(current))
                                : Text(current.toHumanReadableWS()),
                            value: current,
                            groupValue: _type,
                            onChanged: (value) {
                              setState(() {
                                _type = value!;
                              });
                            });
                      })))),
          actions: <Widget>[
            FlatButtonEx.notFilled(
                text: context.l10n.cancel, onPressed: () => Navigator.of(context).pop()),
            FlatButtonEx.filled(text: 'Submit', onPressed: () => Navigator.of(context).pop(_type))
          ]);
    });
  }
}
