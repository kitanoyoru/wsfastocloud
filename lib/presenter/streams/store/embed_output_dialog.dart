import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/intl/l10n/l10n.dart';

class EmbedOutputDialog extends StatefulWidget {
  final List<OutputUrl> outputs;

  const EmbedOutputDialog(this.outputs);

  @override
  _EmbedOutputDialogState createState() {
    return _EmbedOutputDialogState();
  }
}

class _EmbedOutputDialogState extends State<EmbedOutputDialog> {
  int _index = 0;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: const Text('Embed output'),
        contentPadding: const EdgeInsets.all(8),
        content: SingleChildScrollView(
            child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: DropdownButtonEx<int>(
                    hint: 'Embed source',
                    value: _index,
                    values: List<int>.generate(widget.outputs.length, (index) => index),
                    onChanged: (t) {
                      _index = t;
                    },
                    itemBuilder: (index) {
                      final OutputUrl input = widget.outputs[index];
                      return DropdownMenuItem(child: Text(input.uri), value: index);
                    }))),
        actions: <Widget>[
          FlatButtonEx.notFilled(text: context.l10n.cancel, onPressed: _cancel),
          FlatButtonEx.filled(text: 'Get', onPressed: _save)
        ]);
  }

  void _cancel() {
    Navigator.of(context).pop();
  }

  void _save() {
    Navigator.of(context).pop(_index);
  }
}
