import 'package:fastocloud_dart_models/models.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:flutter/material.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/base_page.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/hardware_page.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/proxy_page.dart';

Widget makeAddStreamDialog(
    LiveServer server, IStreamEditor editor, StreamType type, WsMode? mode, bool isSerial) {
  switch (type) {
    case StreamType.PROXY:
      return ProxyStreamPage.add(server, editor, mode);
    case StreamType.VOD_PROXY:
      return VodProxyStreamPage.add(server, editor, mode, isSerial);
    case StreamType.RELAY:
      return RelayStreamPage.add(server, editor, mode);
    case StreamType.ENCODE:
      return EncodeStreamPage.add(server, editor, mode);
    case StreamType.TIMESHIFT_PLAYER:
      return TimeshiftRecorderStreamPage.add(server, editor, mode);
    case StreamType.TIMESHIFT_RECORDER:
      return TimeshiftRecorderStreamPage.add(server, editor, mode);
    case StreamType.CATCHUP:
      return CatchupStreamPage.add(server, editor, mode);
    case StreamType.TEST_LIFE:
      return TestLifeStreamPage.add(server, editor, mode);
    case StreamType.VOD_RELAY:
      return VodRelayStreamPage.add(server, editor, mode, isSerial);
    case StreamType.VOD_ENCODE:
      return VodEncodeStreamPage.add(server, editor, mode, isSerial);
    case StreamType.COD_RELAY:
      return CodRelayStreamPage.add(server, editor, mode);
    case StreamType.COD_ENCODE:
      return CodEncodeStreamPage.add(server, editor, mode);
    case StreamType.EVENT:
      return EventStreamPage.add(server, editor, mode);
    case StreamType.CV_DATA:
      return CvDataStreamPage.add(server, editor, mode);
    case StreamType.CHANGER_RELAY:
      return ChangerRelayStreamPage.add(server, editor, mode);
    case StreamType.CHANGER_ENCODE:
      return ChangerEncoderStreamPage.add(server, editor, mode);
    default:
      throw 'Un implemented type';
  }
}

Widget makeEditStreamDialog(LiveServer server, IStreamEditor editor, StreamType type,
    IStream stream, WsMode? mode, bool isSerial) {
  switch (type) {
    case StreamType.PROXY:
      return ProxyStreamPage.edit(server, editor, stream as ProxyStream, mode);
    case StreamType.VOD_PROXY:
      return VodProxyStreamPage.edit(server, editor, stream as VodProxyStream, mode, isSerial);
    case StreamType.RELAY:
      return RelayStreamPage.edit(server, editor, stream as RelayStream, mode);
    case StreamType.ENCODE:
      return EncodeStreamPage.edit(server, editor, stream as EncodeStream, mode);
    case StreamType.TIMESHIFT_PLAYER:
      return TimeshiftRecorderStreamPage.edit(
          server, editor, stream as TimeshiftRecorderStream, mode);
    case StreamType.TIMESHIFT_RECORDER:
      return TimeshiftRecorderStreamPage.edit(
          server, editor, stream as TimeshiftRecorderStream, mode);
    case StreamType.CATCHUP:
      return CatchupStreamPage.edit(server, editor, stream as CatchupStream, mode);
    case StreamType.TEST_LIFE:
      return TestLifeStreamPage.edit(server, editor, stream as TestLifeStream, mode);
    case StreamType.VOD_RELAY:
      return VodRelayStreamPage.edit(server, editor, stream as VodRelayStream, mode, isSerial);
    case StreamType.VOD_ENCODE:
      return VodEncodeStreamPage.edit(server, editor, stream as VodEncodeStream, mode, isSerial);
    case StreamType.COD_RELAY:
      return CodRelayStreamPage.edit(server, editor, stream as CodRelayStream, mode);
    case StreamType.COD_ENCODE:
      return CodEncodeStreamPage.edit(server, editor, stream as CodEncodeStream, mode);
    case StreamType.EVENT:
      return EventStreamPage.edit(server, editor, stream as EventStream, mode);
    case StreamType.CV_DATA:
      return CvDataStreamPage.edit(server, editor, stream as CvDataStream, mode);
    case StreamType.CHANGER_RELAY:
      return ChangerRelayStreamPage.edit(server, editor, stream as ChangerRelayStream, mode);
    case StreamType.CHANGER_ENCODE:
      return ChangerEncoderStreamPage.edit(server, editor, stream as ChangerEncodeStream, mode);
    default:
      throw 'Un implemented type';
  }
}
