import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/data/repositories/store_editor.dart';
import 'package:wsfastocloud/presenter/streams/store/embed_output_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/make_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/output_player_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_store_bloc/streams_store_bloc.dart';

class OptionMenu extends StatelessWidget {
  final StreamsStoreBloc bloc;
  final LiveServer? getServer;
  final IStream stream;
  final WsMode? mode;
  final bool canRefresh;
  final bool isSerial;

  const OptionMenu(
      {Key? key,
      required this.bloc,
      required this.stream,
      required this.mode,
      required this.canRefresh,
      required this.getServer,
      required this.isSerial})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    void _startStoreStream(StreamsStoreBloc bloc, IStream stream) {
      bloc.startStream(stream.id!);
    }

    void _editStoreStream(StreamsStoreBloc bloc, IStream stream, WsMode? mode) {
      final cur = getServer;
      if (cur == null) {
        return;
      }

      showDialog(
          context: context,
          builder: (context) {
            return makeEditStreamDialog(
                cur, StoreEditor(bloc), stream.type(), stream, mode, isSerial);
          });
    }

    void _embedOutput(StreamsStoreBloc bloc, IStream stream) {
      final result = showDialog(
          context: context,
          builder: (context) {
            return EmbedOutputDialog(stream.output);
          });
      result.then((value) {
        if (value == null) {
          return;
        }

        final resp = bloc.embedOutput(stream.output[value]);
        resp.then((result) {
          TextEditingController _textController = TextEditingController(text: result);
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return SimpleDialog(title: const Text('Get embed output'), children: <Widget>[
                  Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(children: [
                        TextField(
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            controller: _textController),
                        const SizedBox(height: 5.0),
                        FlatButtonEx.filled(
                            text: 'Copy',
                            onPressed: () {
                              Clipboard.setData(ClipboardData(text: _textController.text));
                              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                                content: Text('Copied'),
                              ));
                            }),
                        const SizedBox(height: 5.0),
                        FlatButtonEx.filled(
                            text: 'Close',
                            onPressed: () {
                              Navigator.of(context).pop();
                            })
                      ]))
                ]);
              });
        }, onError: (error) {
          showError(context, error);
        });
      });
    }

    // many
    void _removeStoreStream(
      StreamsStoreBloc bloc,
      IStream stream,
    ) {
      bloc.removeStream(stream.id!);
    }

    void _playOutputStream(IStream stream) {
      showDialog(
          context: context,
          builder: (context) {
            return OutputPlayerDialog(stream.name, stream.type(), stream.output);
          });
    }

    void _copyStream(StreamsStoreBloc bloc, IStream stream) {
      final copy = stream.copyWith(id: null, name: 'Copy ${stream.name}');
      _editStoreStream(bloc, copy, mode);
    }

    void _moveTo(StreamsStoreBloc bloc, IStream stream, bool isSerial) {
      (stream as VodStream).vodType = isSerial ? VodType.VOD : VodType.SERIAL;
      bloc.editStream(stream);
    }

    List<PopupMenuItem> vodActions(
        StreamsStoreBloc bloc, IStream stream, WsMode? mode, bool canRefresh) {
      return [
        if (canRefresh)
          PopupMenuItem(
              child: Text('Refresh'),
              onTap: () => Future.delayed(
                  const Duration(seconds: 0), () => _startStoreStream(bloc, stream))),
        PopupMenuItem(
          child: Text('Edit'),
          onTap: () => Future.delayed(
              const Duration(seconds: 0), () => _editStoreStream(bloc, stream, mode)),
        ),
        PopupMenuItem(
          child: Text('Copy'),
          onTap: () => Future.delayed(const Duration(seconds: 0), () => _copyStream(bloc, stream)),
        ),
        PopupMenuItem(
          child: Text('Play output'),
          onTap: () => Future.delayed(const Duration(seconds: 0), () => _playOutputStream(stream)),
        ),
        if (mode == WsMode.IPTV)
          PopupMenuItem(
            child: Text(isSerial ? 'Move to vods' : 'Move to series'),
            onTap: () =>
                Future.delayed(const Duration(seconds: 0), () => _moveTo(bloc, stream, isSerial)),
          ),
        PopupMenuItem(
          child: Text('Embed output'),
          onTap: () => Future.delayed(const Duration(seconds: 0), () => _embedOutput(bloc, stream)),
        ),
        PopupMenuItem(
          child: Text('Remove'),
          onTap: () =>
              Future.delayed(const Duration(seconds: 0), () => _removeStoreStream(bloc, stream)),
        ),
      ];
    }

    return PopupMenuButton(
      tooltip: 'Options',
      padding: const EdgeInsets.all(0),
      icon: Icon(Icons.more_horiz),
      itemBuilder: (BuildContext context) {
        return vodActions(bloc, stream, mode, canRefresh);
      },
    );
  }
}
