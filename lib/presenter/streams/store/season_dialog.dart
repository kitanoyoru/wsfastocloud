import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_store_bloc/streams_store_bloc.dart';
import 'package:wsfastocloud/presenter/widgets/add_edit_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/streams/stream_icon.dart';

class SeasonDialog extends StatefulWidget {
  final ServerSeason init;
  final List<IStream> availableEpisodes;

  SeasonDialog.add(String icon, this.availableEpisodes) : init = ServerSeason(icon: icon);

  SeasonDialog.edit(ServerSeason season, this.availableEpisodes) : init = season.copy();

  @override
  _SeasonDialogState createState() {
    return _SeasonDialogState();
  }
}

class _SeasonDialogState extends State<SeasonDialog> {
  late ServerSeason _season;
  final TextEditingController _textEditingController = TextEditingController();
  IStream? _currentEid;

  @override
  void initState() {
    super.initState();
    _season = widget.init;
    List<IStream> episodes = [];
    for (final epi in widget.availableEpisodes) {
      if (!_season.episodes.contains(epi.id)) {
        episodes.add(epi);
      }
    }
  }

  bool isAdd() {
    return _season.id == null;
  }

  @override
  Widget build(BuildContext context) {
    return AddEditDialog.narrow(
        add: isAdd(),
        children: <Widget>[
          _nameField(),
          _iconField(),
          _backgroundUrlField(),
          _groupField(),
          _descriptionField(),
          _seasonField(),
          ..._episodes(),
          _addEpisode()
        ],
        onSave: _save);
  }

  Widget _nameField() {
    return TextFieldEx(
        hintText: 'Name',
        errorText: 'Enter name',
        init: _season.name,
        onFieldChanged: (term) {
          _season.name = term;
        });
  }

  Widget _groupField() {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: ChipListField(
            values: _season.groups,
            hintText: 'Groups',
            onItemAdded: (value) => setState(() => _season.groups.add(value)),
            onItemRemoved: (index) => setState(() => _season.groups.removeAt(index))));
  }

  TextFieldEx _iconField() {
    return TextFieldEx(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        minSymbols: IconUrl.MIN_LENGTH,
        maxSymbols: IconUrl.MAX_LENGTH,
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'Icon',
        errorText: 'Enter icon url',
        init: _season.icon,
        decoration: InputDecoration(icon: PreviewIcon.live(_season.icon, width: 40, height: 40)),
        onFieldChanged: (term) => setState(() => _season.icon = term));
  }

  Widget _backgroundUrlField() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        minSymbols: IconUrl.MIN_LENGTH,
        maxSymbols: IconUrl.MAX_LENGTH,
        hintText: 'Background URL',
        init: _season.background.isEmpty
            ? onBackgroundUrl(context.read<StreamsStoreBloc>().defaults().backgroundUrl)
            : _season.background,
        onFieldChanged: onBackgroundUrl);
  }

  Widget _descriptionField() {
    return TextFieldEx(
        maxSymbols: StreamDescription.MAX_LENGTH,
        hintText: 'Description',
        init: _season.description,
        onFieldChanged: (term) {
          _season.description = term;
        });
  }

  Widget _seasonField() {
    return NumberTextField.integer(
        minInt: 0,
        hintText: 'Season',
        canBeEmpty: false,
        initInt: _season.season,
        onFieldChangedInt: (term) {
          if (term != null) _season.season = term;
        });
  }

  List<Widget> _episodes() {
    final List<Widget> result = [];
    for (final String eid in _season.episodes) {
      for (final IStream avail in widget.availableEpisodes) {
        if (avail.id == eid) {
          result.add(_SeasonEntry(eid, avail.name, () => _deleteEpisode(eid)));
          break;
        }
      }
    }

    return result;
  }

  void _deleteEpisode(String eid) {
    setState(() {
      _season.episodes.remove(eid);
    });
  }

  Widget _addEpisode() {
    final Widget add = IconButton(
        tooltip: 'Add episode',
        onPressed: () {
          if (_currentEid == null) {
            return;
          }
          setState(() {
            _season.episodes.add(_currentEid!.id!);
            _currentEid = null;
          });
        },
        icon: const Icon(Icons.add));
    final List<DropdownMenuItem<IStream>> items = [];
    for (final serial in widget.availableEpisodes) {
      final drop = DropdownMenuItem<IStream>(value: serial, child: Text(serial.name));
      items.add(drop);
    }

    final Widget episodesWidget = DropdownButton2<IStream>(
        hint: Text('Available episodes'),
        value: _currentEid,
        items: items,
        onChanged: (IStream? item) {
          setState(() {
            _currentEid = item;
          });
        },
        searchController: _textEditingController,
        searchInnerWidget: Padding(
            padding: const EdgeInsets.only(top: 8, bottom: 4, right: 8, left: 8),
            child: TextFormField(
                controller: _textEditingController,
                decoration: InputDecoration(
                  isDense: true,
                  contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                  hintText: 'Search for an season...',
                ))),
        searchMatchFn: (item, searchValue) {
          final val = item.value as IStream;
          return val.name.contains(searchValue);
        },
        onMenuStateChange: (isOpen) {
          if (!isOpen) {
            _textEditingController.clear();
          }
        });

    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(children: <Widget>[Expanded(child: episodesWidget), add]));
  }

  String onBackgroundUrl(String term) {
    _season.background = term;
    return _season.background;
  }

  void _save() {
    if (!_season.isValid()) {
      return;
    }

    Navigator.of(context).pop(_season);
  }
}

class _SeasonEntry extends StatelessWidget {
  final String id;
  final String name;
  final void Function() onDelete;

  const _SeasonEntry(this.id, this.name, this.onDelete);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Expanded(child: TextFieldEx(hintText: 'Name', init: name)),
      IconButton(tooltip: 'Remove', icon: const Icon(Icons.close), onPressed: onDelete)
    ]);
  }
}
