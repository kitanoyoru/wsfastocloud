import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';

class SeasonsDataLayout<T> extends StatelessWidget {
  final DataSource<T> dataSource;
  final List<Widget> Function() headerActions;
  final List<Widget> Function(T) singleItemActions;
  final List<Widget> Function(List<T>) multipleItemActions;

  const SeasonsDataLayout(
      {required this.dataSource,
      required this.headerActions,
      required this.singleItemActions,
      required this.multipleItemActions});

  @override
  Widget build(BuildContext context) {
    return DataTableEx.customActions(dataSource, _Header<T>(dataSource, headerActions), () {
      return _actions(context);
    });
  }

  // private:
  Widget _actions(
    BuildContext context,
  ) {
    return _actionsRow();
  }

  Widget _actionsRow() {
    return Row(mainAxisSize: MainAxisSize.min, children: _actionsBuilder());
  }

  List<Widget> _actionsBuilder() {
    if (dataSource.selectedRowCount == 1) {
      return singleItemActions(dataSource.selectedItems().first);
    } else if (dataSource.selectedRowCount > 1) {
      return multipleItemActions(dataSource.selectedItems());
    }
    return [];
  }
}

class _Header<T> extends StatelessWidget {
  final DataSource<T> source;
  final List<Widget> Function() actions;

  const _Header(this.source, this.actions);

  @override
  Widget build(BuildContext context) {
    return DataTableSearchHeader(source: source, actions: actions());
  }
}
