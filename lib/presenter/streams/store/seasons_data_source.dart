import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/presenter/streams/store/stream_data_source.dart';
import 'package:wsfastocloud/presenter/widgets/streams/stream_icon.dart';

class SeasonsSource extends SortableDataSource<ServerSeason> {
  SeasonsSource(List<DataEntry<ServerSeason>> items) : super(items: items);

  @override
  String get itemsName {
    return 'seasons';
  }

  @override
  bool searchCondition(String text, ServerSeason season) {
    return season.name.toLowerCase().contains(text.toLowerCase());
  }

  @override
  Widget get noItems {
    return _NoSeasonsSource();
  }

  @override
  List<Widget> headers() {
    return const [Text('Name'), Text('Season'), Text('Episodes'), Text('Views')];
  }

  @override
  List<Widget> tiles(ServerSeason item) {
    final views = item.viewCount; // #FIXME need to calculate according episodes views
    return [
      NameAndIcon(name: Text(item.name), icon: PreviewIcon.vod(item.icon, width: 40, height: 60)),
      Text('${item.season}'),
      Text('${item.episodes.length}'),
      Text('${views}')
    ];
  }

  @override
  bool equalItemsCondition(ServerSeason item, ServerSeason listItem) {
    return listItem.id == item.id;
  }

  @override
  int compareItems(ServerSeason a, ServerSeason b, int index) {
    switch (index) {
      case 1:
        return a.name.compareTo(b.name);
      case 2:
        return a.season.compareTo(b.season);
      case 3:
        return a.episodes.length.compareTo(b.episodes.length);
      case 4:
        return a.viewCount.compareTo(b.viewCount);
      default:
        throw ArgumentError.value(index, 'index');
    }
  }
}

class _NoSeasonsSource extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Center(
        child: NonAvailableBuffer(icon: Icons.error, message: 'Please create Seasons'));
  }
}
