import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';
import 'package:wsfastocloud/domain/repositories/stream_db_repository.dart';
import 'package:wsfastocloud/presenter/streams/store/seasons_data_source.dart';

part 'seasons_store_event.dart';
part 'seasons_store_state.dart';

class SeasonsBloc extends Bloc<SeasonEvent, SeasonsState> {
  late final StreamSubscription _subscription;
  final WebSocketApiBloc webSocketApiBloc;
  final StreamDBRepository streamRepository;
  final SeasonsSource _seasonsSource = SeasonsSource([]);

  SeasonsBloc(this.webSocketApiBloc, this.streamRepository) : super(SeasonsInitialState()) {
    on<LoadSeasonsEvent>(_loadSeasonsEvent);
    on<UpdateSeasonEvent>(_updateEvent);
    on<ListenSeasonEvent>(_listenEvent);
    on<RemoveSeasonEvent>(_removeEvent);
    on<FailureEvent>(_failure);
    on<InitialEvent>(_init);
    add(ListenSeasonEvent());
  }

  Future<void> addSeason(ServerSeason season) {
    return streamRepository.addSeason(season);
  }

  Future<void> editSeason(ServerSeason season) {
    return streamRepository.editSeason(season);
  }

  Future<void> removeSeason(ServerSeason season) {
    return streamRepository.removeSeason(season);
  }

  void _loadSeasonsEvent(LoadSeasonsEvent event, Emitter<SeasonsState> emit) async {
    try {
      _seasonsSource.clearItems();
      final series = await streamRepository.loadSeasons();
      emit(SeasonsInitialState());
      _seasonsSource.addItems(series);
      emit(SeasonsDataState.create(series, _seasonsSource));
    } catch (e) {
      emit(SeasonsFailureState('Backend error: $e'));
    }
  }

  void _updateEvent(UpdateSeasonEvent event, Emitter<SeasonsState> emit) async {
    if (state is! SeasonsDataState) {
      [event.season].forEach((element) {
        _seasonsSource.addItem(element);
      });
      emit(SeasonsDataState.create([event.season], _seasonsSource));
      return;
    }

    final season = (state as SeasonsDataState).season;

    if (season.containsKey(event.season.id)) {
      _seasonsSource.updateItem(event.season);
      emit(SeasonsDataState.update(season, event.season, _seasonsSource));
    } else {
      _seasonsSource.addItem(event.season);
      emit(SeasonsDataState.add(season, event.season, _seasonsSource));
    }
  }

  void _init(InitialEvent event, Emitter<SeasonsState> emit) {
    _seasonsSource.clearItems();
    emit(SeasonsInitialState());
  }

  void _listenEvent(ListenSeasonEvent event, Emitter<SeasonsState> emit) {
    _subscription = webSocketApiBloc.stream.listen((event) {
      if (event is WebSocketApiMessage) {
        const wsAddedEvent = 'season_added';
        const wsUpdatedEvent = 'season_updated';
        const wsRemovedEvent = 'season_removed';

        if (event.type == wsAddedEvent) {
          final stream = ServerSeason.fromJson(event.data);
          add(UpdateSeasonEvent(stream));
        } else if (event.type == wsUpdatedEvent) {
          final stream = ServerSeason.fromJson(event.data);
          add(UpdateSeasonEvent(stream));
        } else if (event.type == wsRemovedEvent) {
          final stream = ServerSeason.fromJson(event.data);
          add(RemoveSeasonEvent(stream));
        }
      } else if (event is WebSocketFailure) {
        add(FailureEvent(event.description));
      } else if (event is WebSocketConnected) {
        add(LoadSeasonsEvent());
      } else {
        add(InitialEvent());
      }
    });
    if (webSocketApiBloc.isConnected()) {
      add(LoadSeasonsEvent());
    }
  }

  void _failure(FailureEvent event, Emitter<SeasonsState> emit) {
    emit(SeasonsFailureState(event.description));
  }

  void dispose() {
    _subscription.cancel();
  }

  void _removeEvent(RemoveSeasonEvent event, Emitter<SeasonsState> emit) {
    if (state is SeasonsDataState) {
      final seasons = (state as SeasonsDataState).season;
      _seasonsSource.removeItem(event.season);
      emit(SeasonsDataState.remove(seasons, event.season.id, _seasonsSource));
    }
  }
}
