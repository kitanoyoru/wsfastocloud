import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:fastocloud_dart_models/models.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:universal_html/html.dart' as html;
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/base_page.dart';

class SelectM3uDialog extends StatefulWidget {
  final IStreamEditor editor;
  final List<StreamType> types;
  final bool isSeries;
  final String Function(StreamType type) convertTypeToText;

  SelectM3uDialog(
      {required this.editor,
      required this.types,
      required this.isSeries,
      this.convertTypeToText = defaultConverter});

  @override
  _SelectM3uDialogState createState() {
    return _SelectM3uDialogState();
  }

  static String defaultConverter(StreamType type) {
    return type.toHumanReadableWS();
  }
}

class _SelectM3uDialogState extends State<SelectM3uDialog> {
  Map<String, List<int>> _filesData = {};
  final List<String> _tags = [];
  bool _parsingStreams = false;
  double _imageCheckTime = 0.05;
  bool _skipGroups = false;

  late StreamType _type = widget.types[0];

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
        title: const Text('M3U Upload'),
        children: _parsingStreams
            ? [
                const Center(
                    child:
                        Padding(padding: EdgeInsets.all(8.0), child: CircularProgressIndicator()))
              ]
            : _fields());
  }

  List<Widget> _fields() {
    return <Widget>[
      _typeField(),
      _tagsField(),
      _skipFileGroups(),
      Padding(padding: const EdgeInsets.symmetric(horizontal: 8.0), child: _timeImageCheckFiled()),
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: FlatButtonEx.filled(text: 'Select a file', onPressed: _startWebFilePicker)),
      const SizedBox(height: 8),
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: FlatButtonEx.filled(
              text: 'Upload', onPressed: _filesData.isEmpty ? null : _makeRequest))
    ];
  }

  // private:
  void _startWebFilePicker() async {
    _filesData = {};
    if (kIsWeb) {
      final html.FileUploadInputElement uploadInput = html.FileUploadInputElement();
      uploadInput.multiple = true;
      uploadInput.draggable = true;
      uploadInput.click();

      uploadInput.addEventListener('change', (e) {
        for (int i = 0; i < uploadInput.files!.length; ++i) {
          final file = uploadInput.files![i];
          final reader = html.FileReader();
          reader.onLoadEnd.listen((e) {
            final base64 = reader.result.toString().split(',').last;
            final contents = const Base64Decoder().convert(base64);
            _filesData[file.name] = contents;
            if (mounted) {
              setState(() {});
            }
          });
          reader.readAsDataUrl(file);
        }
        if (mounted) {
          setState(() {});
        }
      });
    } else {
      try {
        final FilePickerResult? result = await FilePicker.platform.pickFiles();
        if (result == null) {
          return;
        }

        for (int i = 0; i < result.files.length; ++i) {
          final PlatformFile file = result.files[i];
          final data = File(file.path!).readAsBytesSync();
          _filesData[file.name] = data;
        }
        if (mounted) {
          setState(() {});
        }
      } on PlatformException catch (e) {
        log('Unsupported operation: $e');
      }
    }
  }

  Widget _typeField() {
    return DropdownButtonEx<StreamType>(
        hint: widget.convertTypeToText(_type),
        value: _type,
        values: widget.types,
        onChanged: (c) {
          _type = c;
        },
        itemBuilder: (StreamType value) {
          return DropdownMenuItem(child: Text(widget.convertTypeToText(value)), value: value);
        });
  }

  Widget _timeImageCheckFiled() {
    return NumberTextField.decimal(
        hintText: 'Image check time(sec)',
        initDouble: _imageCheckTime,
        minDouble: 0.01,
        maxDouble: 1,
        onFieldChangedDouble: (term) {
          _imageCheckTime = term!;
        });
  }

  Widget _skipFileGroups() {
    return StateCheckBox(
        title: 'Skip m3u groups',
        init: _skipGroups,
        onChanged: (value) {
          _skipGroups = value;
        });
  }

  Widget _tagsField() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: ChipListField(
        values: _tags,
        hintText: 'Groups',
        onItemAdded: (value) => setState(() => _tags.add(value)),
        onItemRemoved: (index) => setState(() => _tags.removeAt(index)),
      ),
    );
  }

  void _makeRequest() {
    if (mounted) {
      setState(() {
        _parsingStreams = true;
      });
    }

    widget.editor
        .uploadM3uFile(_type, widget.isSeries, _imageCheckTime, _skipGroups, _tags, _filesData)
        .then((bool value) {
      Navigator.of(context).pop();
    }, onError: (error) {
      if (mounted) {
        setState(() {
          _parsingStreams = false;
        });
      }
      showError(context, error);
    });
  }
}
