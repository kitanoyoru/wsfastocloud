import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_store_bloc/streams_store_bloc.dart';
import 'package:wsfastocloud/presenter/widgets/add_edit_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/data_picker.dart';
import 'package:wsfastocloud/presenter/widgets/streams/stream_icon.dart';

class SerialDialog extends StatefulWidget {
  final ServerSerial init;
  final List<ServerSeason> availableEpisodes;

  SerialDialog.add(String icon, this.availableEpisodes) : init = ServerSerial(icon: icon);

  SerialDialog.edit(ServerSerial serial, this.availableEpisodes) : init = serial.copy();

  @override
  _SerialDialogState createState() {
    return _SerialDialogState();
  }
}

class _SerialDialogState extends State<SerialDialog> {
  late ServerSerial _serial;
  final TextEditingController _textEditingController = TextEditingController();
  ServerSeason? _currentEid;

  @override
  void initState() {
    super.initState();
    _serial = widget.init;
    List<ServerSeason> episodes = [];
    for (final epi in widget.availableEpisodes) {
      if (!_serial.seasons.contains(epi.id)) {
        episodes.add(epi);
      }
    }
  }

  bool isAdd() {
    return _serial.id == null;
  }

  @override
  Widget build(BuildContext context) {
    return AddEditDialog.narrow(
        add: isAdd(),
        children: <Widget>[
          _nameField(),
          _iconField(),
          _backgroundUrlField(),
          _groupField(),
          _iarcField(),
          _descriptionField(),
          _primeDateField(),
          _priceField(),
          ..._seasons(),
          _addEpisode(),
          _visibleField()
        ],
        onSave: _save);
  }

  Widget _nameField() {
    return TextFieldEx(
        hintText: 'Name',
        errorText: 'Enter name',
        init: _serial.name,
        onFieldChanged: (term) {
          _serial.name = term;
        });
  }

  Widget _groupField() {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: ChipListField(
            values: _serial.groups,
            hintText: 'Groups',
            onItemAdded: (value) => setState(() => _serial.groups.add(value)),
            onItemRemoved: (index) => setState(() => _serial.groups.removeAt(index))));
  }

  Widget _iarcField() {
    return NumberTextField.integer(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        minInt: IARC.MIN,
        maxInt: IARC.MAX,
        hintText: 'IARC',
        canBeEmpty: false,
        initInt: _serial.iarc,
        onFieldChangedInt: (term) {
          if (term != null) {
            _serial.iarc = term;
          }
        });
  }

  TextFieldEx _iconField() {
    return TextFieldEx(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        minSymbols: IconUrl.MIN_LENGTH,
        maxSymbols: IconUrl.MAX_LENGTH,
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'Icon',
        errorText: 'Enter icon url',
        init: _serial.icon,
        decoration: InputDecoration(icon: PreviewIcon.live(_serial.icon, width: 40, height: 40)),
        onFieldChanged: (term) => setState(() {
              _serial.icon = term;
            }));
  }

  Widget _backgroundUrlField() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        minSymbols: IconUrl.MIN_LENGTH,
        maxSymbols: IconUrl.MAX_LENGTH,
        hintText: 'Background URL',
        init: _serial.background.isEmpty
            ? onBackgroundUrl(context.read<StreamsStoreBloc>().defaults().backgroundUrl)
            : _serial.background,
        onFieldChanged: onBackgroundUrl);
  }

  Widget _descriptionField() {
    return TextFieldEx(
        maxSymbols: StreamDescription.MAX_LENGTH,
        hintText: 'Description',
        init: _serial.description,
        onFieldChanged: (term) {
          _serial.description = term;
        });
  }

  Widget _primeDateField() {
    return DatePicker('Premiere date', _serial.primeDate, (int date) {
      _serial.primeDate = date;
    });
  }

  Widget _priceField() {
    return NumberTextField.decimal(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        minDouble: Price.MIN,
        maxDouble: Price.MAX,
        hintText: 'Price (\$)',
        canBeEmpty: false,
        initDouble: _serial.price,
        onFieldChangedDouble: (term) {
          if (term != null) {
            _serial.price = term;
          }
        });
  }

  Widget _visibleField() {
    return StateCheckBox(
        title: 'Visible for subscribers',
        init: _serial.visible,
        onChanged: (value) {
          _serial.visible = value;
        });
  }

  List<Widget> _seasons() {
    final List<Widget> result = [];
    for (final String eid in _serial.seasons) {
      for (final avail in widget.availableEpisodes) {
        if (avail.id == eid) {
          result.add(_SerialEntry(eid, avail.name, () => _deleteSeason(eid)));
          break;
        }
      }
    }

    return result;
  }

  void _deleteSeason(String eid) {
    setState(() {
      _serial.seasons.remove(eid);
    });
  }

  Widget _addEpisode() {
    final Widget add = IconButton(
        tooltip: 'Add season',
        onPressed: () {
          if (_currentEid == null) {
            return;
          }
          setState(() {
            _serial.seasons.add(_currentEid!.id!);
            _currentEid = null;
          });
        },
        icon: const Icon(Icons.add));

    final List<DropdownMenuItem<ServerSeason>> items = [];
    for (final serial in widget.availableEpisodes) {
      final drop = DropdownMenuItem<ServerSeason>(value: serial, child: Text(serial.name));
      items.add(drop);
    }

    final Widget seasonsWidget = DropdownButton2<ServerSeason>(
        hint: Text('Available seasons'),
        value: _currentEid,
        items: items,
        onChanged: (item) {
          setState(() {
            _currentEid = item;
          });
        },
        searchController: _textEditingController,
        searchInnerWidget: Padding(
            padding: const EdgeInsets.only(top: 8, bottom: 4, right: 8, left: 8),
            child: TextFormField(
                controller: _textEditingController,
                decoration: InputDecoration(
                  isDense: true,
                  contentPadding: const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                  hintText: 'Search for an serial...',
                ))),
        searchMatchFn: (item, searchValue) {
          final val = item.value as ServerSeason;
          return val.name.contains(searchValue);
        },
        onMenuStateChange: (isOpen) {
          if (!isOpen) {
            _textEditingController.clear();
          }
        });

    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(children: <Widget>[Expanded(child: seasonsWidget), add]));
  }

  String onBackgroundUrl(String term) {
    _serial.background = term;
    return _serial.background;
  }

  void _save() {
    if (!_serial.isValid()) {
      return;
    }

    Navigator.of(context).pop(_serial);
  }
}

class _SerialEntry extends StatelessWidget {
  final String id;
  final String name;
  final void Function() onDelete;

  const _SerialEntry(this.id, this.name, this.onDelete);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Expanded(child: TextFieldEx(hintText: 'Name', init: name)),
      IconButton(tooltip: 'Remove', icon: const Icon(Icons.close), onPressed: onDelete)
    ]);
  }
}
