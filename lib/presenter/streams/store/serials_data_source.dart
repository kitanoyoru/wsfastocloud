import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/presenter/streams/store/stream_data_source.dart';
import 'package:wsfastocloud/presenter/widgets/streams/stream_icon.dart';

class SerialsSource extends SortableDataSource<ServerSerial> {
  SerialsSource(List<DataEntry<ServerSerial>> items) : super(items: items);

  @override
  String get itemsName {
    return 'serials';
  }

  @override
  bool searchCondition(String text, ServerSerial season) {
    return season.name.toLowerCase().contains(text.toLowerCase());
  }

  @override
  Widget get noItems {
    return _NoSerialsSource();
  }

  @override
  List<Widget> headers() {
    return const [Text('Name'), Text('Seasons'), Text('Views')];
  }

  @override
  List<Widget> tiles(ServerSerial item) {
    final views = item.viewCount; // #FIXME need to calculate according seasons views
    return [
      NameAndIcon(name: Text(item.name), icon: PreviewIcon.vod(item.icon, width: 40, height: 60)),
      Text('${item.seasons.length}'),
      Text('${views}')
    ];
  }

  @override
  bool equalItemsCondition(ServerSerial item, ServerSerial listItem) {
    return listItem.id == item.id;
  }

  @override
  int compareItems(ServerSerial a, ServerSerial b, int index) {
    switch (index) {
      case 1:
        return a.name.compareTo(b.name);
      case 2:
        return a.seasons.length.compareTo(b.seasons.length);
      case 3:
        return a.viewCount.compareTo(b.viewCount);
      default:
        throw ArgumentError.value(index, 'index');
    }
  }
}

class _NoSerialsSource extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const Center(
        child: NonAvailableBuffer(icon: Icons.error, message: 'Please create Serials'));
  }
}
