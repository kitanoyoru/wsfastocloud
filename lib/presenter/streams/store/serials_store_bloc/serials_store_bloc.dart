import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';
import 'package:wsfastocloud/domain/repositories/stream_db_repository.dart';
import 'package:wsfastocloud/presenter/streams/store/serials_data_source.dart';

part 'serials_store_event.dart';
part 'serials_store_state.dart';

class SerialsBloc extends Bloc<SerialEvent, SerialsState> {
  late final StreamSubscription _subscription;
  final WebSocketApiBloc webSocketApiBloc;
  final StreamDBRepository streamRepository;
  final SerialsSource _serialsSource = SerialsSource([]);

  SerialsBloc(this.webSocketApiBloc, this.streamRepository) : super(SerialsInitialState()) {
    on<LoadSerialsEvent>(_loadSerialsEvent);
    on<UpdateSerialEvent>(_updateEvent);
    on<ListenSerialEvent>(_listenEvent);
    on<RemoveSerialEvent>(_removeEvent);
    on<FailureEvent>(_failure);
    on<InitialEvent>(_init);
    add(ListenSerialEvent());
  }

  Future<void> addSerial(ServerSerial serial) {
    return streamRepository.addSerial(serial);
  }

  Future<void> editSerial(ServerSerial serial) {
    return streamRepository.editSerial(serial);
  }

  Future<void> removeSerial(ServerSerial serial) {
    return streamRepository.removeSerial(serial);
  }

  void _loadSerialsEvent(LoadSerialsEvent event, Emitter<SerialsState> emit) async {
    try {
      _serialsSource.clearItems();
      final serials = await streamRepository.loadSerials();
      emit(SerialsInitialState());
      _serialsSource.addItems(serials);
      emit(SerialsDataState.create(serials, _serialsSource));
    } catch (e) {
      emit(SerialsFailureState('Backend error: $e'));
    }
  }

  void _updateEvent(UpdateSerialEvent event, Emitter<SerialsState> emit) async {
    if (state is! SerialsDataState) {
      [event.serial].forEach((element) {
        _serialsSource.addItem(element);
      });
      emit(SerialsDataState.create([event.serial], _serialsSource));
      return;
    }

    final serial = (state as SerialsDataState).serial;

    if (serial.containsKey(event.serial.id)) {
      _serialsSource.updateItem(event.serial);
      emit(SerialsDataState.update(serial, event.serial, _serialsSource));
    } else {
      _serialsSource.addItem(event.serial);
      emit(SerialsDataState.add(serial, event.serial, _serialsSource));
    }
  }

  void _init(InitialEvent event, Emitter<SerialsState> emit) {
    _serialsSource.clearItems();
    emit(SerialsInitialState());
  }

  void _listenEvent(ListenSerialEvent event, Emitter<SerialsState> emit) {
    _subscription = webSocketApiBloc.stream.listen((event) {
      if (event is WebSocketApiMessage) {
        const wsAddedEvent = 'serial_added';
        const wsUpdatedEvent = 'serial_updated';
        const wsRemovedEvent = 'serial_removed';

        if (event.type == wsAddedEvent) {
          final stream = ServerSerial.fromJson(event.data);
          add(UpdateSerialEvent(stream));
        } else if (event.type == wsUpdatedEvent) {
          final stream = ServerSerial.fromJson(event.data);
          add(UpdateSerialEvent(stream));
        } else if (event.type == wsRemovedEvent) {
          final stream = ServerSerial.fromJson(event.data);
          add(RemoveSerialEvent(stream));
        }
      } else if (event is WebSocketFailure) {
        add(FailureEvent(event.description));
      } else if (event is WebSocketConnected) {
        add(LoadSerialsEvent());
      } else {
        add(InitialEvent());
      }
    });
    if (webSocketApiBloc.isConnected()) {
      add(LoadSerialsEvent());
    }
  }

  void _failure(FailureEvent event, Emitter<SerialsState> emit) {
    emit(SerialsFailureState(event.description));
  }

  void dispose() {
    _subscription.cancel();
  }

  void _removeEvent(RemoveSerialEvent event, Emitter<SerialsState> emit) {
    if (state is SerialsDataState) {
      final serials = (state as SerialsDataState).serial;
      _serialsSource.removeItem(event.serial);
      emit(SerialsDataState.remove(serials, event.serial.id, _serialsSource));
    }
  }
}
