part of 'serials_store_bloc.dart';

abstract class SerialsState extends Equatable {
  const SerialsState();

  @override
  List<Object?> get props => [];
}

class SerialsInitialState extends SerialsState {
  const SerialsInitialState();

  @override
  List<Object?> get props => [];
}

class SerialsFailureState extends SerialsState {
  final String description;

  const SerialsFailureState(this.description);

  @override
  List<Object?> get props => [description];
}

class SerialsDataState extends SerialsState {
  final Map<String, ServerSerial> serial;
  final SerialsSource serialsSource;

  const SerialsDataState(this.serial, this.serialsSource);

  factory SerialsDataState.create(List<ServerSerial> items, SerialsSource storeStreamDataSource) {
    final streamsMap = Map<String, ServerSerial>.fromIterable(items, key: (stream) => stream.id);

    return SerialsDataState(streamsMap, storeStreamDataSource);
  }

  factory SerialsDataState.add(
      Map<String, dynamic> old, ServerSerial item, SerialsSource storeStreamDataSource) {
    final streamsMap = Map<String, ServerSerial>.from(old);

    streamsMap[item.id!] = item;

    return SerialsDataState(streamsMap, storeStreamDataSource);
  }

  factory SerialsDataState.update(
      Map<String, dynamic> old, ServerSerial item, SerialsSource storeStreamDataSource) {
    final streamsMap = Map<String, ServerSerial>.from(old);

    streamsMap[item.id!] = item;

    return SerialsDataState(streamsMap, storeStreamDataSource);
  }

  factory SerialsDataState.remove(
      Map<String, dynamic> old, String? sid, SerialsSource storeStreamDataSource) {
    final streamsMap = Map<String, ServerSerial>.from(old);

    streamsMap.remove(sid);

    return SerialsDataState(streamsMap, storeStreamDataSource);
  }

  @override
  List<Object?> get props => [serial, serialsSource];
}
