import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wsfastocloud/presenter/service_info_bloc/service_info_bloc.dart';
import 'package:wsfastocloud/presenter/streams/store/store_live_widget.dart';
import 'package:wsfastocloud/presenter/streams/store/store_vods_widget.dart';

class ServerStreamsTabs extends StatefulWidget {
  const ServerStreamsTabs(
      {Key? key, required this.connected, required this.mediaServerInfo, required this.mode})
      : super(key: key);
  final bool connected;
  final MediaServerInfo? mediaServerInfo;
  final WsMode? mode;

  @override
  State<ServerStreamsTabs> createState() {
    return _ServerStreamsTabsState();
  }
}

class _ServerStreamsTabsState extends State<ServerStreamsTabs> {
  LiveServer? server;

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
      // ignore: prefer_if_elements_to_conditional_expressions
      _storeContent()
    ]);
  }

  Widget _storeContent() {
    return StoreOwner(server, widget.connected, widget.mediaServerInfo, mode: widget.mode);
  }
}

class StoreOwner extends StatefulWidget {
  final LiveServer? getServer;
  final bool connected;
  final MediaServerInfo? mediaServerInfo;
  final WsMode? mode;

  StoreOwner(this.getServer, this.connected, this.mediaServerInfo, {required this.mode});

  @override
  _StoreOwnerState createState() {
    return _StoreOwnerState();
  }
}

class _StoreOwnerState extends State<StoreOwner> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final tabs = TabBar(
        controller: _tabController,
        tabs: [const Tab(text: 'Live'), Tab(text: 'VODs')],
        labelColor: Colors.black);
    final content = SizedBox(
        height: MediaQuery.of(context).size.height * 0.73,
        child: BlocBuilder<ServiceInfoBloc, ServiceInfoState>(builder: (context, state) {
          if (state is ServiceInfoData) {
            return TabBarView(physics: ScrollPhysics(), controller: _tabController, children: [
              StoreLiveWidget(state.config.copyWith(widget.mediaServerInfo!), mode: widget.mode),
              StoreVodsWidget(state.config.copyWith(widget.mediaServerInfo!), mode: widget.mode)
            ]);
          }
          return TabBarView(physics: ScrollPhysics(), controller: _tabController, children: [
            StoreLiveWidget(widget.getServer, mode: widget.mode),
            StoreVodsWidget(widget.getServer, mode: widget.mode)
          ]);
        }));
    return Column(children: [tabs, content]);
  }
}
