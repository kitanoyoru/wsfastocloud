import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:wsfastocloud/data/repositories/store_editor.dart';
import 'package:wsfastocloud/presenter/streams/store/add_button.dart';
import 'package:wsfastocloud/presenter/streams/store/make_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/options_menu.dart';
import 'package:wsfastocloud/presenter/streams/store/select_m3u_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_store_bloc/streams_store_bloc.dart';
import 'package:wsfastocloud/presenter/streams/store/vod_card.dart';
import 'package:wsfastocloud/presenter/widgets/error_dialog.dart';

class StoreSeriesWidget extends StatefulWidget {
  static const availableTypes = [StreamType.VOD_PROXY, StreamType.VOD_RELAY, StreamType.VOD_ENCODE];
  final LiveServer? getServer;
  final WsMode? mode;
  List<IStream> searchedEpisodes = [];

  StoreSeriesWidget(this.getServer, {required this.mode});

  @override
  _StoreSeriesWidgetState createState() {
    return _StoreSeriesWidgetState();
  }
}

class _StoreSeriesWidgetState extends State<StoreSeriesWidget> {
  final TextEditingController _controller = TextEditingController(text: '');

  bool get _isSearching => _controller.text != '';

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<StreamsStoreBloc>();
    return BlocBuilder<StreamsStoreBloc, StreamsDBState>(builder: (context, state) {
      if (state is StreamsDBData) {
        final data = state.storeSeriesDataSource;
        final actions = [
          _addStreamButton(bloc, widget.mode),
          _uploadM3uFilesButton(bloc),
          _removeAllSeries(bloc, data)
        ];
        final content = Column(children: [
          Padding(
              padding: EdgeInsets.only(top: 12, right: 12, bottom: 24),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [_episodesSearchHeader(data), const Spacer(flex: 2)] + actions)),
          Expanded(
              child: ScreenTypeLayout(
                  desktop:
                      _drawContent(context, false, _isSearching ? widget.searchedEpisodes : data),
                  mobile:
                      _drawContent(context, true, _isSearching ? widget.searchedEpisodes : data)))
        ]);
        return SizedBox(height: 758, child: content);
      } else if (state is StreamsDBFailure) {
        return ErrorDialog(error: state.description);
      }
      final content = Column(children: [
        Padding(
            padding: EdgeInsets.only(top: 12, right: 12, bottom: 24),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [_addStreamButton(bloc, widget.mode), _uploadM3uFilesButton(bloc)])),
        Expanded(child: ScreenTypeLayout(desktop: _drawEmpty(), mobile: _drawEmpty()))
      ]);
      return SizedBox(height: 758, child: content);
    });
  }

  Widget _episodesSearchHeader(List<IStream> data) {
    return Expanded(
      child: ListTile(
        leading: const Icon(Icons.search),
        title: TextField(
          controller: _controller,
          decoration: InputDecoration(hintText: 'Search', border: InputBorder.none),
          onChanged: (text) {
            List<IStream> result = [];
            if (text != '') {
              final title = text.toLowerCase();
              data.forEach((stream) {
                if (stream.name.toLowerCase().contains(title)) {
                  result.add(stream);
                }
              });
            }
            widget.searchedEpisodes = result;
            setState(() {});
          },
        ),
        trailing: _isSearching
            ? IconButton(
                icon: const Icon(Icons.cancel),
                color: Theme.of(context).colorScheme.secondary,
                onPressed: () {
                  setState(() {
                    _controller.text = '';
                  });
                })
            : null,
      ),
    );
  }

  Widget _drawEmpty() {
    return Center(child: NonAvailableBuffer(icon: Icons.error, message: 'Please create Episodes'));
  }

  Widget _drawContent(BuildContext context, bool isMobile, List<IStream> items) {
    return BlocBuilder<StreamsStoreBloc, StreamsDBState>(builder: (context, state) {
      if (items.isEmpty) {
        return _drawEmpty();
      }
      final bloc = context.read<StreamsStoreBloc>();
      return CardGrid<IStream>(items, (int i, IStream item, double maxWidth, double maxHeight) {
        final canRefresh = item.type() != StreamType.VOD_PROXY;
        return Stack(children: <Widget>[
          VodCard(
              iconLink: item.icon!,
              width: maxWidth,
              height: maxHeight,
              onPressed: () => _editStoreStream(bloc, item, widget.mode)),
          VodCardBadge(
              right: 5,
              top: 5,
              bottom: null,
              width: 36.0,
              child: OptionMenu(
                  isSerial: true,
                  mode: widget.mode,
                  getServer: widget.getServer,
                  canRefresh: canRefresh,
                  stream: item,
                  bloc: bloc)),
          VodCardBadge(left: 5, bottom: 5, child: VodCardViews(item.views)),
        ]);
      }, cardsInHorizontal: isMobile ? 4 : 12);
    });
  }

  void _editStoreStream(StreamsStoreBloc bloc, IStream stream, WsMode? mode) {
    final cur = widget.getServer;
    if (cur == null) {
      return;
    }

    showDialog(
        context: context,
        builder: (context) {
          return makeEditStreamDialog(cur, StoreEditor(bloc), stream.type(), stream, mode, true);
        });
  }

  Widget _addStreamButton(StreamsStoreBloc bloc, WsMode? mode) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: AddStreamButton(
            widget.getServer, StoreEditor(bloc), StoreSeriesWidget.availableTypes,
            mode: mode, isSerial: true));
  }

  Widget _removeAllSeries(StreamsStoreBloc bloc, List<IStream> items) {
    return FlatButtonEx.filled(
        text: 'Remove all',
        onPressed: () {
          Widget cancelButton = TextButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              });
          Widget okButton = TextButton(
              child: Text('OK'),
              onPressed: () {
                for (IStream stream in items) {
                  bloc.removeStream(stream.id!);
                }
                Navigator.of(context).pop();
              });

          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                    title: Text('Confirmation'),
                    content: Text('Are you sure to delete all Series?'),
                    actions: [cancelButton, okButton]);
              });
        });
  }

  Widget _uploadM3uFilesButton(StreamsStoreBloc bloc) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(0, 0, 16, 0),
        child: FlatButtonEx.filled(text: 'Upload m3u', onPressed: () => _uploadM3uFiles(bloc)));
  }

  void _uploadM3uFiles(StreamsStoreBloc bloc) {
    showDialog(
        context: context,
        builder: (context) {
          return SelectM3uDialog(
              editor: StoreEditor(bloc),
              types: StoreSeriesWidget.availableTypes,
              isSeries: true,
              convertTypeToText: (StreamType type) {
                if (type == StreamType.VOD_PROXY) {
                  return 'Episode External';
                } else if (type == StreamType.VOD_RELAY) {
                  return 'Episode Restream';
                }
                return 'Episode Encode';
              });
        });
  }
}

class VodCardViews extends StatelessWidget {
  final int count;

  const VodCardViews(this.count);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[Icon(Icons.visibility), Text(' $count')]));
  }
}
