import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/data/repositories/store_editor.dart';
import 'package:wsfastocloud/presenter/streams/store/add_button.dart';
import 'package:wsfastocloud/presenter/streams/store/embed_output_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/make_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/output_player_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/select_m3u_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/stream_data_source.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_store_bloc/streams_store_bloc.dart';
import 'package:wsfastocloud/presenter/widgets/error_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/streams/actions.dart';

class StoreLiveWidget extends StatefulWidget {
  final LiveServer? getServer;
  final WsMode? mode;
  static const availableTypes = [
    StreamType.PROXY,
    StreamType.RELAY,
    StreamType.COD_RELAY,
    StreamType.CHANGER_RELAY,
    StreamType.ENCODE,
    StreamType.COD_ENCODE,
    StreamType.CHANGER_ENCODE
  ];

  StoreLiveWidget(this.getServer, {required this.mode});

  @override
  _StoreLiveWidgetState createState() {
    return _StoreLiveWidgetState();
  }
}

class _StoreLiveWidgetState extends State<StoreLiveWidget> {
  StoreStreamDataSource _storeDataSource = StoreStreamDataSource([]);

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<StreamsStoreBloc>();
    return BlocBuilder<StreamsStoreBloc, StreamsDBState>(builder: (context, state) {
      if (state is StreamsDBData) {
        _storeDataSource = state.storeStreamDataSource;
        final content = DataTableEx(
            _storeDataSource,
            DataTableSearchHeader(
                source: _storeDataSource,
                actions: [_addStreamButton(bloc, widget.mode), _uploadM3uFilesButton(bloc)]),
            _storeActions);
        return Padding(padding: const EdgeInsets.only(right: 10.0), child: content);
      } else if (state is StreamsDBFailure) {
        return ErrorDialog(error: state.description);
      }
      final content = DataTableEx(
          _storeDataSource,
          DataTableSearchHeader(
              source: _storeDataSource,
              actions: [_addStreamButton(bloc, widget.mode), _uploadM3uFilesButton(bloc)]),
          _storeActions);
      return Padding(padding: const EdgeInsets.only(right: 10.0), child: content);
    });
  }

  List<Widget> _storeActions() {
    if (_storeDataSource.selectedRowCount == 0) {
      return [];
    }

    final bloc = context.read<StreamsStoreBloc>();
    final streams = _storeDataSource.selectedItems();
    bool canStart = true;
    for (var stream in streams) {
      if (stream.type() == StreamType.PROXY) {
        canStart = false;
        break;
      }
    }

    bool canEdit = false;
    bool canCopy = false;
    bool canPlay = false;
    bool canEmbed = false;
    if (streams.length == 1) {
      canCopy = true;
      canEdit = true;
      canPlay = true;
      canEmbed = true;
    }
    return [
      Row(children: [
        if (canStart) StreamActionIcon.start(() => _startStoreStream(bloc, streams)),
        if (canEdit) CommonActionIcon.edit(() => _editStoreStream(bloc, streams, widget.mode)),
        if (canPlay) StreamActionIcon.playOutput(() => _playOutputStream(streams)),
        if (canEmbed) StreamActionIcon.embedOutput(() => _embedOutput(bloc, streams)),
        if (canCopy) StreamActionIcon.copyStream(() => _copyStream(bloc, streams)),
        CommonActionIcon.remove(() => _removeStoreStream(bloc, streams)),
      ])
    ];
  }

  void _playOutputStream(List<IStream> streams) {
    for (var stream in streams) {
      showDialog(
          context: context,
          builder: (context) {
            return OutputPlayerDialog(stream.name, stream.type(), stream.output);
          });
    }
  }

  void _embedOutput(StreamsStoreBloc bloc, List<IStream> streams) {
    for (var stream in streams) {
      final result = showDialog(
          context: context,
          builder: (context) {
            return EmbedOutputDialog(stream.output);
          });
      result.then((value) {
        if (value == null) {
          return;
        }

        final resp = bloc.embedOutput(stream.output[value]);
        resp.then((result) {
          TextEditingController _textController = TextEditingController(text: result);
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return SimpleDialog(title: const Text('Get embed output'), children: <Widget>[
                  Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(children: [
                        TextField(
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            controller: _textController),
                        const SizedBox(height: 5.0),
                        FlatButtonEx.filled(
                            text: 'Copy',
                            onPressed: () {
                              Clipboard.setData(ClipboardData(text: _textController.text));
                              ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                                content: Text('Copied'),
                              ));
                            }),
                        const SizedBox(height: 5.0),
                        FlatButtonEx.filled(
                            text: 'Close',
                            onPressed: () {
                              Navigator.of(context).pop();
                            })
                      ]))
                ]);
              });
        }, onError: (error) {
          showError(context, error);
        });
      });
    }
  }

  // many
  void _startStoreStream(StreamsStoreBloc bloc, List<IStream> streams) {
    for (var stream in streams) {
      bloc.startStream(stream.id!);
    }
  }

  void _editStoreStream(StreamsStoreBloc bloc, List<IStream> streams, WsMode? mode) {
    final cur = widget.getServer;

    for (var stream in streams) {
      showDialog(
          context: context,
          builder: (context) {
            return makeEditStreamDialog(
                cur!, StoreEditor(bloc), stream.type(), stream, mode, false);
          });
    }
  }

  // many
  void _removeStoreStream(StreamsStoreBloc bloc, List<IStream> streams) {
    for (var stream in streams) {
      bloc.removeStream(stream.id!);
    }
  }

  Widget _addStreamButton(StreamsStoreBloc bloc, WsMode? mode) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: AddStreamButton(widget.getServer, StoreEditor(bloc), StoreLiveWidget.availableTypes,
            mode: mode, isSerial: false));
  }

  Widget _uploadM3uFilesButton(StreamsStoreBloc bloc) {
    return FlatButtonEx.filled(text: 'Upload m3u', onPressed: () => _uploadM3uFiles(bloc));
  }

  void _uploadM3uFiles(StreamsStoreBloc bloc) {
    showDialog(
        context: context,
        builder: (context) {
          return SelectM3uDialog(
              editor: StoreEditor(bloc), types: StoreLiveWidget.availableTypes, isSeries: false);
        });
  }

  void _copyStream(StreamsStoreBloc bloc, List<IStream> streams) {
    for (var stream in streams) {
      final copy = stream.copyWith(id: null, name: 'Copy ${stream.name}');
      _editStoreStream(bloc, [copy], widget.mode);
    }
  }
}
