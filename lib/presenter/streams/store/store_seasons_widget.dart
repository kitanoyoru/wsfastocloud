import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/presenter/streams/store/season_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/seasons_data_layout.dart';
import 'package:wsfastocloud/presenter/streams/store/seasons_data_source.dart';
import 'package:wsfastocloud/presenter/streams/store/seasons_store_bloc/seasons_store_bloc.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_store_bloc/streams_store_bloc.dart';

class SeasonsListLayout extends StatefulWidget {
  final SeasonsSource dataSource;

  const SeasonsListLayout(this.dataSource);

  @override
  _SeasonsListLayoutState createState() {
    return _SeasonsListLayoutState();
  }
}

class _SeasonsListLayoutState extends State<SeasonsListLayout> {
  @override
  Widget build(BuildContext context) {
    return SeasonsDataLayout<ServerSeason>(
        dataSource: widget.dataSource,
        headerActions: _headerActions,
        singleItemActions: singleStreamActions,
        multipleItemActions: multipleStreamActions);
  }

  // private:
  List<Widget> singleStreamActions(ServerSeason season) {
    return [
      _editSeasonButton(season),
      _copySeasonButton(season),
      _removeButton([season])
    ];
  }

  List<Widget> multipleStreamActions(List<ServerSeason> seasons) {
    return [_removeButton(seasons)];
  }

  List<Widget> _headerActions() {
    return <Widget>[_addSeasonButton()];
  }

  Widget _addSeasonButton() {
    return FlatButtonEx.filled(text: 'Add', onPressed: () => _addSeason());
  }

  String defaultSeasonLogoIcon() {
    final fetcher = context.read<StreamsStoreBloc>().streamRepository;
    final defaults = fetcher.defaults();
    return defaults.streamLogoIcon;
  }

  void _addSeason() {
    final result = showDialog<ServerSeason>(
        context: context,
        builder: (context) {
          final fetcher = context.read<StreamsStoreBloc>().streamRepository;
          return FutureBuilder<List<IStream>>(
              builder: (context, snap) {
                if (snap.hasData) {
                  return SeasonDialog.add(defaultSeasonLogoIcon(), snap.data!);
                }
                return const Center(child: CircularProgressIndicator());
              },
              future: fetcher.loadEpisodes());
        });

    result.then((ServerSeason? season) {
      if (season == null) {
        return;
      }
      context.read<SeasonsBloc>().addSeason(season);
    });
  }

  Widget _editSeasonButton(ServerSeason season) {
    return IconButton(
        icon: const Icon(Icons.edit), tooltip: 'Edit', onPressed: () => _editSeason(season));
  }

  Widget _copySeasonButton(ServerSeason season) {
    return IconButton(
        icon: const Icon(Icons.copy), tooltip: 'Copy', onPressed: () => _copySeason(season));
  }

  void _editSeason(ServerSeason season) {
    final result = showDialog<ServerSeason>(
        context: context,
        builder: (context) {
          final fetcher = context.read<StreamsStoreBloc>().streamRepository;
          return FutureBuilder<List<IStream>>(
              builder: (context, snap) {
                if (snap.hasData) {
                  return SeasonDialog.edit(season, snap.data!);
                }
                return const Center(child: CircularProgressIndicator());
              },
              future: fetcher.loadEpisodes());
        });
    result.then((ServerSeason? season) {
      if (season == null) {
        return;
      }
      context.read<SeasonsBloc>().editSeason(season);
    });
  }

  Widget _removeButton(List<ServerSeason> seasons) {
    return IconButton(
        icon: const Icon(Icons.delete), tooltip: 'Remove', onPressed: () => _removeSeason(seasons));
  }

  void _removeSeason(List<ServerSeason> seasons) {
    for (final season in seasons) {
      context.read<SeasonsBloc>().removeSeason(season);
    }
  }

  void _copySeason(ServerSeason season) {
    final copy = season.copyWith(id: null, name: 'Copy ${season.name}');
    _editSeason(copy);
  }
}
