import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/presenter/streams/store/serial_dialog.dart';
import 'package:wsfastocloud/presenter/streams/store/serials_data_layout.dart';
import 'package:wsfastocloud/presenter/streams/store/serials_data_source.dart';
import 'package:wsfastocloud/presenter/streams/store/serials_store_bloc/serials_store_bloc.dart';
import 'package:wsfastocloud/presenter/streams/store/streams_store_bloc/streams_store_bloc.dart';

class SerialsListLayout extends StatefulWidget {
  final SerialsSource dataSource;

  const SerialsListLayout(this.dataSource);

  @override
  _SerialsListLayoutState createState() {
    return _SerialsListLayoutState();
  }
}

class _SerialsListLayoutState extends State<SerialsListLayout> {
  @override
  Widget build(BuildContext context) {
    return SerialsDataLayout<ServerSerial>(
        dataSource: widget.dataSource,
        headerActions: _headerActions,
        singleItemActions: singleStreamActions,
        multipleItemActions: multipleStreamActions);
  }

  // private:
  List<Widget> singleStreamActions(ServerSerial serial) {
    return [
      _editSerialButton(serial),
      _copySerialButton(serial),
      _removeButton([serial])
    ];
  }

  List<Widget> multipleStreamActions(List<ServerSerial> serials) {
    return [_removeButton(serials)];
  }

  List<Widget> _headerActions() {
    return <Widget>[_addSerialButton()];
  }

  Widget _addSerialButton() {
    return FlatButtonEx.filled(text: 'Add', onPressed: () => _addSerial());
  }

  String defaultSerialLogoIcon() {
    final fetcher = context.read<StreamsStoreBloc>().streamRepository;
    final defaults = fetcher.defaults();
    return defaults.streamLogoIcon;
  }

  void _addSerial() {
    final result = showDialog<ServerSerial>(
        context: context,
        builder: (context) {
          final fetcher = context.read<StreamsStoreBloc>().streamRepository;
          return FutureBuilder<List<ServerSeason>>(
              builder: (context, snap) {
                if (snap.hasData) {
                  return SerialDialog.add(defaultSerialLogoIcon(), snap.data!);
                }
                return const Center(child: CircularProgressIndicator());
              },
              future: fetcher.loadSeasons());
        });

    result.then((ServerSerial? serial) {
      if (serial == null) {
        return;
      }
      context.read<SerialsBloc>().addSerial(serial);
    });
  }

  Widget _editSerialButton(ServerSerial serial) {
    return IconButton(
        icon: const Icon(Icons.edit), tooltip: 'Edit', onPressed: () => _editSerial(serial));
  }

  Widget _copySerialButton(ServerSerial serial) {
    return IconButton(
        icon: const Icon(Icons.copy), tooltip: 'Copy', onPressed: () => _copySerial(serial));
  }

  void _editSerial(ServerSerial serial) {
    final result = showDialog<ServerSerial>(
        context: context,
        builder: (context) {
          final fetcher = context.read<StreamsStoreBloc>().streamRepository;
          return FutureBuilder<List<ServerSeason>>(
              builder: (context, snap) {
                if (snap.hasData) {
                  return SerialDialog.edit(serial, snap.data!);
                }
                return const Center(child: CircularProgressIndicator());
              },
              future: fetcher.loadSeasons());
        });
    result.then((ServerSerial? serial) {
      if (serial == null) {
        return;
      }
      context.read<SerialsBloc>().editSerial(serial);
    });
  }

  Widget _removeButton(List<ServerSerial> serials) {
    return IconButton(
        icon: const Icon(Icons.delete), tooltip: 'Remove', onPressed: () => _removeSerial(serials));
  }

  void _removeSerial(List<ServerSerial> serials) {
    for (final serial in serials) {
      context.read<SerialsBloc>().removeSerial(serial);
    }
  }

  void _copySerial(ServerSerial serial) {
    final copy = serial.copyWith(id: null, name: 'Copy ${serial.name}');
    _editSerial(copy);
  }
}
