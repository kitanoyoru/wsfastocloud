import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/presenter/widgets/streams/stream_icon.dart';

String _date(int milliseconds) => DateTime.fromMillisecondsSinceEpoch(milliseconds).toString();

class NameAndIcon extends StatelessWidget {
  final Widget icon;
  final Widget name;

  const NameAndIcon({required this.name, required this.icon});

  @override
  Widget build(BuildContext context) {
    return Row(children: [icon, const SizedBox(width: 16), Expanded(child: name)]);
  }
}

Widget streamTilesNameAndIcon(IStream init) {
  return NameAndIcon(name: Text(init.name), icon: StreamIcon(init.icon!, init.type()));
}

class StoreStreamDataSource extends SortableDataSource<IStream> {
  StoreStreamDataSource(List<DataEntry<IStream>> items) : super(items: items);

  @override
  bool equalItemsCondition(item, listItem) => item.id == listItem.id;

  @override
  List<Widget> headers() {
    final headers = ['Id', 'Name', 'Views', 'Created date', 'Type'];

    return List.generate(headers.length, (index) => Text(headers[index]));
  }

  @override
  String get itemsName => 'streams';

  @override
  Widget get noItems => const _NoStreamsAvailable();

  @override
  bool searchCondition(String text, IStream stream) {
    return stream.name.toLowerCase().contains(text.toLowerCase());
  }

  @override
  List<Widget> tiles(IStream s) {
    return [
      Text(s.id.toString()),
      streamTilesNameAndIcon(s),
      Text('${s.views}'),
      Text(_date(s.createdDate!)),
      Text(s.type().toHumanReadableWS())
    ];
  }

  @override
  int compareItems(IStream a, IStream b, int index) {
    switch (index) {
      case 1:
        return a.id!.compareTo(b.id!);
      case 2:
        return a.name.compareTo(b.name);
      case 3:
        return a.views.compareTo(b.views);
      case 4:
        return a.createdDate!.compareTo(b.createdDate!);
      case 5:
        return a.type.toString().compareTo(b.type.toString());

      default:
        throw ArgumentError.value(index, 'index');
    }
  }
}

class _NoStreamsAvailable extends StatelessWidget {
  const _NoStreamsAvailable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
        child: NonAvailableBuffer(icon: Icons.error, message: 'Please create Streams'));
  }
}
