import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/data/services/websocket_api_bloc/websocket_api_bloc.dart';
import 'package:wsfastocloud/domain/repositories/stream_db_repository.dart';
import 'package:wsfastocloud/presenter/streams/store/stream_data_source.dart';

part 'streams_store_event.dart';
part 'streams_store_state.dart';

class StreamsStoreBloc extends Bloc<StreamsStoreEvent, StreamsDBState> {
  late final StreamSubscription _subscription;
  final WebSocketApiBloc webSocketApiBloc;
  final StreamDBRepository streamRepository;
  final StoreStreamDataSource _storeStreamDataSource = StoreStreamDataSource([]);
  final List<IStream> _storeVodsDataSource = [];
  final List<IStream> _storeEpisodesDataSource = [];

  StreamsStoreBloc(this.webSocketApiBloc, this.streamRepository) : super(StreamsDBInitial()) {
    on<ListenStoreEvent>(_listen);
    on<UpdateStreamEvent>(_updateStream);
    on<RemoveStreamEvent>(_removeStream);
    on<FetchStreamEvent>(_fetch);
    on<InitialEvent>(_init);
    on<FailureEvent>(_failure);
    add(ListenStoreEvent());
  }

  Defaults defaults() {
    return streamRepository.defaults();
  }

  Future<void> startStream(String id) {
    return streamRepository.startStream(id);
  }

  Future<bool> addStream(IStream stream) {
    return streamRepository.addStream(stream);
  }

  Future<bool> editStream(IStream stream) {
    return streamRepository.editStream(stream);
  }

  Future<String> probeOutUrl(OutputUrl url) {
    return streamRepository.probeOutUrl(url);
  }

  Future<String> getTokenUrl(String ip) {
    return streamRepository.getTokenUrl(ip);
  }

  Future<String> probeInUrl(InputUrl url) {
    return streamRepository.probeInUrl(url);
  }

  Future<bool> removeStream(String id) {
    return streamRepository.removeStream(id);
  }

  Future<String> embedOutput(OutputUrl url) {
    return streamRepository.embedOutput(url);
  }

  Future<bool> uploadM3uFile(StreamType type, bool isSeries, double imageCheckTime,
      final bool skipGroups, final List<String> groups, Map<String, List<int>> data) {
    return streamRepository.uploadM3uFile(type, isSeries, imageCheckTime, skipGroups, groups, data);
  }

  void dispose() {
    _subscription.cancel();
  }

  void _init(InitialEvent event, Emitter<StreamsDBState> emit) {
    _storeVodsDataSource.clear();
    _storeStreamDataSource.clearItems();
    emit(StreamsDBInitial());
  }

  void _listen(ListenStoreEvent event, Emitter<StreamsDBState> emit) async {
    _subscription = webSocketApiBloc.stream.listen((event) {
      if (event is WebSocketApiMessage) {
        const wsAddedEvent = 'stream_added';
        const wsUpdatedEvent = 'stream_updated';
        const wsRemovedEvent = 'stream_removed';

        if (event.type == wsAddedEvent) {
          final stream = makeStream(event.data);
          if (stream != null) {
            add(UpdateStreamEvent(stream));
          }
        } else if (event.type == wsUpdatedEvent) {
          final stream = makeStream(event.data);
          if (stream != null) {
            add(UpdateStreamEvent(stream));
          }
        } else if (event.type == wsRemovedEvent) {
          final stream = makeStream(event.data);
          if (stream != null) {
            add(RemoveStreamEvent(stream.id));
          }
        }
      } else if (event is WebSocketFailure) {
        add(FailureEvent(event.description));
      } else if (event is WebSocketConnected) {
        add(FetchStreamEvent());
      } else {
        add(InitialEvent());
      }
    });
    if (webSocketApiBloc.isConnected()) {
      add(FetchStreamEvent());
    }
  }

  void _updateStream(UpdateStreamEvent event, Emitter<StreamsDBState> emit) {
    if (state is! StreamsDBData) {
      [event.stream].forEach((element) {
        if (element.isVod()) {
          if ((element as VodStream).vodType != VodType.VOD) {
            _storeEpisodesDataSource.add(element);
          } else {
            _storeVodsDataSource.add(element);
          }
        } else {
          _storeStreamDataSource.addItem(element);
        }
      });
      emit(StreamsDBData.create(
          [event.stream], _storeStreamDataSource, _storeVodsDataSource, _storeEpisodesDataSource));
      return;
    }

    final streams = (state as StreamsDBData).streams;

    if (streams.containsKey(event.stream.id)) {
      if (event.stream.isVod()) {
        if ((event.stream as VodStream).vodType != VodType.VOD) {
          final index =
              _storeEpisodesDataSource.indexWhere((element) => element.id == event.stream.id);
          if (index >= 0) {
            _storeEpisodesDataSource.removeAt(index);
            _storeEpisodesDataSource.insert(index, event.stream);
          } else {
            final index =
                _storeVodsDataSource.indexWhere((element) => element.id == event.stream.id);
            _storeVodsDataSource.removeAt(index);
            _storeEpisodesDataSource.add(event.stream);
          }
        } else {
          final index = _storeVodsDataSource.indexWhere((element) => element.id == event.stream.id);
          if (index >= 0) {
            _storeVodsDataSource.removeAt(index);
            _storeVodsDataSource.insert(index, event.stream);
          } else {
            final index =
                _storeEpisodesDataSource.indexWhere((element) => element.id == event.stream.id);
            _storeEpisodesDataSource.removeAt(index);
            _storeVodsDataSource.add(event.stream);
          }
        }
      } else {
        _storeStreamDataSource.updateItem(event.stream);
      }
      emit(StreamsDBData.update(streams, event.stream, _storeStreamDataSource, _storeVodsDataSource,
          _storeEpisodesDataSource));
    } else {
      if (event.stream.isVod()) {
        if ((event.stream as VodStream).vodType != VodType.VOD) {
          _storeEpisodesDataSource.add(event.stream);
        } else {
          _storeVodsDataSource.add(event.stream);
        }
      } else {
        _storeStreamDataSource.addItem(event.stream);
      }
      emit(StreamsDBData.add(streams, event.stream, _storeStreamDataSource, _storeVodsDataSource,
          _storeEpisodesDataSource));
    }
  }

  void _removeStream(RemoveStreamEvent event, Emitter<StreamsDBState> emit) {
    if (state is StreamsDBData) {
      final streams = (state as StreamsDBData).streams;
      final removed = streams[event.id]!;
      if (removed.isVod()) {
        if ((removed as VodStream).vodType != VodType.VOD) {
          final index = _storeEpisodesDataSource.indexWhere((element) => element.id == event.id);
          if (index >= 0) {
            _storeEpisodesDataSource.removeAt(index);
          }
        } else {
          final index = _storeVodsDataSource.indexWhere((element) => element.id == event.id);
          _storeVodsDataSource.removeAt(index);
        }
      } else {
        _storeStreamDataSource.removeItem(removed);
      }
      emit(StreamsDBData.remove(streams, removed, _storeStreamDataSource, _storeVodsDataSource,
          _storeEpisodesDataSource));
    }
  }

  void _fetch(FetchStreamEvent event, Emitter<StreamsDBState> emit) async {
    try {
      _storeStreamDataSource.clearItems();
      _storeVodsDataSource.clear();
      _storeEpisodesDataSource.clear();
      final streams = await streamRepository.getDBStreams();
      emit(StreamsDBInitial());
      streams.forEach((element) {
        if (element.isVod()) {
          if ((element as VodStream).vodType != VodType.VOD) {
            _storeEpisodesDataSource.add(element);
          } else {
            _storeVodsDataSource.add(element);
          }
        } else {
          _storeStreamDataSource.addItem(element);
        }
      });
      emit(StreamsDBData.create(
          streams, _storeStreamDataSource, _storeVodsDataSource, _storeEpisodesDataSource));
    } catch (e) {
      emit(StreamsDBFailure('Backend error: $e'));
    }
  }

  void _failure(FailureEvent event, Emitter<StreamsDBState> emit) {
    emit(StreamsDBFailure(event.description));
  }
}
