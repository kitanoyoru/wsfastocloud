part of 'streams_store_bloc.dart';

abstract class StreamsStoreEvent extends Equatable {
  const StreamsStoreEvent();

  @override
  List<Object?> get props => [];
}

class ListenStoreEvent extends StreamsStoreEvent {
  const ListenStoreEvent();

  @override
  List<Object?> get props => [];
}

class UpdateStreamEvent extends StreamsStoreEvent {
  final IStream stream;

  const UpdateStreamEvent(this.stream);

  @override
  List<Object?> get props => [stream];
}

class RemoveStreamEvent extends StreamsStoreEvent {
  final String? id;

  const RemoveStreamEvent(this.id);

  @override
  List<Object?> get props => [id];
}

class FetchStreamEvent extends StreamsStoreEvent {
  const FetchStreamEvent();

  @override
  List<Object?> get props => [];
}

class InitialEvent extends StreamsStoreEvent {
  const InitialEvent();

  @override
  List<Object?> get props => [];
}

class FailureEvent extends StreamsStoreEvent {
  final String description;

  const FailureEvent(this.description);

  @override
  List<Object?> get props => [description];
}
