part of 'streams_store_bloc.dart';

abstract class StreamsDBState extends Equatable {
  const StreamsDBState();

  @override
  List<Object?> get props => [];
}

class StreamsDBInitial extends StreamsDBState {
  const StreamsDBInitial();

  @override
  List<Object?> get props => [];
}

class StreamsDBData extends StreamsDBState {
  final Map<String, IStream> streams;
  final StoreStreamDataSource storeStreamDataSource;
  final List<IStream> storeVodsDataSource;
  final List<IStream> storeSeriesDataSource;

  const StreamsDBData(this.streams, this.storeStreamDataSource, this.storeVodsDataSource,
      this.storeSeriesDataSource);

  factory StreamsDBData.create(List<IStream> items, StoreStreamDataSource storeStreamDataSource,
      List<IStream> storeVodsDataSource, List<IStream> storeSeriesDataSource) {
    final streamsMap = Map<String, IStream>.fromIterable(items, key: (stream) => stream.id);

    return StreamsDBData(
        streamsMap, storeStreamDataSource, storeVodsDataSource, storeSeriesDataSource);
  }

  factory StreamsDBData.add(
      Map<String, dynamic> old,
      IStream item,
      StoreStreamDataSource storeStreamDataSource,
      List<IStream> storeVodsDataSource,
      List<IStream> storeSeriesDataSource) {
    final streamsMap = Map<String, IStream>.from(old);

    streamsMap[item.id!] = item;

    return StreamsDBData(
        streamsMap, storeStreamDataSource, storeVodsDataSource, storeSeriesDataSource);
  }

  factory StreamsDBData.update(
      Map<String, dynamic> old,
      IStream item,
      StoreStreamDataSource storeStreamDataSource,
      List<IStream> storeVodsDataSource,
      List<IStream> storeSeriesDataSource) {
    final streamsMap = Map<String, IStream>.from(old);

    streamsMap[item.id!] = item;

    return StreamsDBData(
        streamsMap, storeStreamDataSource, storeVodsDataSource, storeSeriesDataSource);
  }

  factory StreamsDBData.remove(
      Map<String, dynamic> old,
      IStream item,
      StoreStreamDataSource storeStreamDataSource,
      List<IStream> storeVodsDataSource,
      List<IStream> storeSeriesDataSource) {
    final streamsMap = Map<String, IStream>.from(old);

    streamsMap.remove(item.id);

    return StreamsDBData(
        streamsMap, storeStreamDataSource, storeVodsDataSource, storeSeriesDataSource);
  }

  @override
  List<Object?> get props => [
        streams,
        storeStreamDataSource,
        storeVodsDataSource,
        storeSeriesDataSource,
      ];
}

class StreamsDBFailure extends StreamsDBState {
  final String description;

  const StreamsDBFailure(this.description);

  @override
  List<Object?> get props => [description];
}
