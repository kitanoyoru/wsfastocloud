import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/presenter/widgets/preview_icon.dart';

class VodCard extends StatelessWidget {
  final String iconLink;
  final double? width;
  final double? height;
  final double borderRadius;
  final Function? onPressed;
  final bool uploadVideo;

  const VodCard(
      {required this.iconLink, this.height, this.width, this.borderRadius = 2.0, this.onPressed})
      : uploadVideo = false;

  const VodCard.uploadVideo(
      {required this.iconLink, this.height, this.width, this.borderRadius = 2.0, this.onPressed})
      : uploadVideo = true;

  static const CARD_WIDTH = 376.0;
  static const ASPECT_RATIO = 16 / 9;

  @override
  Widget build(BuildContext context) {
    Size getSize() {
      if (height != null && width == null) {
        return Size(height! / ASPECT_RATIO, height!);
      } else if (height == null && width != null) {
        return Size(width!, width! * ASPECT_RATIO);
      } else if (height != null && width != null) {
        return Size(width!, height!);
      }
      return const Size(CARD_WIDTH, CARD_WIDTH * ASPECT_RATIO);
    }

    final size = getSize();
    final border = BorderRadius.circular(borderRadius);
    return SizedBox(
        width: size.width,
        height: size.height,
        child: Card(
            margin: const EdgeInsets.all(0),
            elevation: 2,
            shape: RoundedRectangleBorder(borderRadius: border),
            child: Stack(children: <Widget>[
              ClipRRect(
                  borderRadius: border,
                  child: uploadVideo
                      ? PreviewIcon(iconLink,
                          assetLink: 'install/assets/upload_video.png',
                          width: width,
                          height: height)
                      : PreviewIcon(iconLink, width: width, height: height)),
              InkWell(onTap: () {
                onPressed?.call();
              })
            ])));
  }
}

class VodCardBadge extends StatelessWidget {
  static const HEIGHT = 36.0;

  final Widget child;
  final double? top;
  final double? bottom;
  final double? left;
  final double? right;
  final double? width;

  const VodCardBadge(
      {required this.child, this.top, this.bottom, this.left, this.right, this.width});

  @override
  Widget build(BuildContext context) {
    final color = Theme.of(context).colorScheme.secondary;
    final widget = Container(
        decoration: BoxDecoration(
            color: color, borderRadius: const BorderRadius.all(Radius.circular(HEIGHT / 2))),
        height: HEIGHT,
        width: width,
        child: child);
    return Positioned(left: left, top: top, right: right, bottom: bottom, child: widget);
  }
}

class CardGrid<T> extends StatefulWidget {
  static const double EDGE_INSETS = 4.0;

  final List<T> streams;
  final Widget Function(int, T, double, double) tile;
  final int cardsInHorizontal;
  static const int defaultVodsPerPage = 10;
  final int vodsPerPage;
  final List<int> availableVodsPerPage;

  const CardGrid(
    this.streams,
    this.tile, {
    this.cardsInHorizontal = 3,
    this.vodsPerPage = defaultVodsPerPage,
    this.availableVodsPerPage = const <int>[
      defaultVodsPerPage,
      defaultVodsPerPage * 2,
      defaultVodsPerPage * 5,
      defaultVodsPerPage * 10,
      defaultVodsPerPage * 20,
      defaultVodsPerPage * 50,
      defaultVodsPerPage * 100
    ],
  });

  @override
  State<CardGrid<T>> createState() => _CardGridState<T>();
}

class _CardGridState<T> extends State<CardGrid<T>> {
  late int _vodsCount;
  int _vodsPerPage = 10;
  int currentPage = 0;

  num pageCount() {
    return _vodsCount / _vodsPerPage;
  }

  @override
  void didUpdateWidget(CardGrid<T> oldWidget) {
    super.didUpdateWidget(oldWidget);
    _vodsCount = widget.streams.length;
  }

  void _vodsPerPageChanged(int rows) {
    setState(() {
      currentPage = 0;
      _vodsPerPage = rows;
    });
    const UpdateScrollBar().dispatch(context);
  }

  _nextPage() {
    if ((currentPage + 1) < pageCount()) {
      setState(() {
        currentPage += 1;
      });
    }
  }

  _previousPage() {
    if (currentPage != 0) {
      setState(() {
        currentPage -= 1;
      });
    }
  }

  String calcPages() {
    if (pageCount().round() < pageCount()) {
      return (pageCount().round() + 1).toString();
    } else {
      return (pageCount().round()).toString();
    }
  }

  int calcItemCount() {
    if (_vodsCount > _vodsPerPage) {
      if (currentPage == pageCount() ~/ 1) {
        return _vodsCount - currentPage * _vodsPerPage;
      } else {
        return _vodsPerPage;
      }
    } else {
      return _vodsCount;
    }
  }

  @override
  void initState() {
    _vodsCount = widget.streams.length;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final maxWidth = size.width - 2 * CardGrid.EDGE_INSETS * widget.cardsInHorizontal;
    final cardWidth = maxWidth / widget.cardsInHorizontal;
    const double aspect = 9 / 16;
    final maxHeight = maxWidth / aspect;
    return LayoutBuilder(builder: (BuildContext context, BoxConstraints constraints) {
      return Card(
        semanticContainer: false,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            _header(),
            Divider(),
            Expanded(
              child: ConstrainedBox(
                constraints: BoxConstraints(minWidth: constraints.maxWidth),
                child: Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: GridView.builder(
                      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                          maxCrossAxisExtent: cardWidth + 2 * CardGrid.EDGE_INSETS,
                          crossAxisSpacing: CardGrid.EDGE_INSETS,
                          mainAxisSpacing: CardGrid.EDGE_INSETS,
                          childAspectRatio: aspect),
                      itemCount: calcItemCount(),
                      itemBuilder: (BuildContext context, int index) {
                        return widget.tile(
                            (index + (currentPage * _vodsPerPage)),
                            widget.streams[index + (currentPage * _vodsPerPage)],
                            maxWidth,
                            maxHeight);
                      }),
                ),
              ),
            ),
          ],
        ),
      );
    });
  }

  Widget _header() {
    return DefaultTextStyle(
        style: Theme.of(context).textTheme.caption!,
        child: IconTheme.merge(
            data: const IconThemeData(opacity: 0.54),
            child: SizedBox(
                height: 56.0,
                child: SingleChildScrollView(
                    dragStartBehavior: DragStartBehavior.start,
                    scrollDirection: Axis.horizontal,
                    reverse: true,
                    child: Row(children: _headerWidgets())))));
  }

  List<Widget> _headerWidgets() {
    final List<Widget> footerWidgets = <Widget>[];
    final List<Widget> availableVodsPerPage = widget.availableVodsPerPage
        .where((int value) => value <= _vodsCount || value == widget.vodsPerPage)
        .map<DropdownMenuItem<int>>((int value) {
      return DropdownMenuItem<int>(value: value, child: Text('$value'));
    }).toList();
    footerWidgets.addAll(<Widget>[
      Container(width: 14.0),
      Text('Vods per page'),
      ConstrainedBox(
          constraints: const BoxConstraints(minWidth: 64.0),
          child: Align(
              alignment: AlignmentDirectional.centerEnd,
              child: DropdownButtonHideUnderline(
                  child: DropdownButton<int>(
                      items: availableVodsPerPage.cast<DropdownMenuItem<int>>(),
                      value: _vodsPerPage,
                      onChanged: (count) => _vodsPerPageChanged(count!),
                      style: Theme.of(context).textTheme.caption))))
    ]);
    footerWidgets.addAll(<Widget>[
      Container(width: 32.0),
      Text('Page ${currentPage + 1} of ${calcPages()}'),
      Container(width: 32.0),
      IconButton(
          icon: const Icon(Icons.chevron_left),
          padding: EdgeInsets.zero,
          tooltip: 'Previous page',
          onPressed: _previousPage),
      Container(width: 24.0),
      IconButton(
          icon: const Icon(Icons.chevron_right),
          padding: EdgeInsets.zero,
          tooltip: 'Next page',
          onPressed: _nextPage),
      Container(width: 14.0)
    ]);
    return footerWidgets;
  }
}
