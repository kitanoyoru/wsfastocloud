import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:wsfastocloud/intl/l10n/l10n.dart';
import 'package:wsfastocloud/presenter/widgets/scrollbar.dart';

class AddEditDialog extends StatelessWidget {
  static const double NARROW_WIDTH = 640;
  static const double WIDE_WIDTH = 800;
  static const double NARROW_HEIGHT = 480;
  static const double WIDE_HEIGHT = 600;
  final bool add;
  final void Function() onSave;
  final List<Widget> children;
  final double maxWidth;
  final double maxHeight;

  AddEditDialog(
      {this.add = false,
      required this.children,
      required this.onSave,
      required this.maxWidth,
      required this.maxHeight});

  AddEditDialog.narrow({this.add = false, required this.children, required this.onSave})
      : maxWidth = NARROW_WIDTH,
        maxHeight = NARROW_HEIGHT;

  AddEditDialog.wide({this.add = false, required this.children, required this.onSave})
      : maxWidth = WIDE_WIDTH,
        maxHeight = WIDE_HEIGHT;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: BaseAddEditDialog(
          isAdd: add,
          width: maxWidth,
          height: maxHeight,
          onSave: () {
            _formKey.currentState!.validate();
            onSave();
          },
          content: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ScrollbarEx(builder: (controller) {
                return SingleChildScrollView(
                    controller: controller,
                    child: Form(
                        key: _formKey,
                        child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: children)));
              }))),
    );
  }

  static double getNarrow(bool isMobile) {
    return isMobile ? double.maxFinite : NARROW_WIDTH;
  }

  static double getWide(bool isMobile) {
    return isMobile ? double.maxFinite : WIDE_HEIGHT;
  }
}

class BaseAddEditDialog extends StatelessWidget {
  const BaseAddEditDialog(
      {required this.width,
      required this.height,
      required this.isAdd,
      required this.onSave,
      required this.content,
      Key? key})
      : super(key: key);

  final double width;
  final double height;
  final bool isAdd;
  final VoidCallback onSave;
  final Widget content;

  @override
  Widget build(BuildContext context) {
    Widget nameField(bool isAdd) {
      final text = isAdd ? context.l10n.add : context.l10n.edit;
      return Padding(
          padding: const EdgeInsets.all(12.0), child: Text(text, style: TextStyle(fontSize: 24)));
    }

    return ResponsiveBuilder(builder: (context, sizingInformation) {
      return AlertDialog(
          insetPadding: sizingInformation.isMobile
              ? const EdgeInsets.all(0)
              : const EdgeInsets.symmetric(horizontal: 40.0, vertical: 24.0),
          contentPadding: const EdgeInsets.all(0),
          shape: RoundedRectangleBorder(
              borderRadius:
                  BorderRadius.all(Radius.circular(sizingInformation.isMobile ? 0 : 4.0))),
          content: SingleChildScrollView(
              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            nameField(isAdd),
            SizedBox(
                height: sizingInformation.isMobile ? double.maxFinite : height,
                width: sizingInformation.isMobile ? double.maxFinite : width,
                child: content)
          ])),
          actions: <Widget>[
            FlatButtonEx.notFilled(text: context.l10n.cancel, onPressed: Navigator.of(context).pop),
            FlatButtonEx.filled(
                text: isAdd ? context.l10n.add : context.l10n.save, onPressed: onSave)
          ]);
    });
  }
}
