import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/presenter/widgets/color_picker.dart';
import 'package:wsfastocloud/presenter/widgets/optional.dart';
import 'package:wsfastocloud/presenter/widgets/save_image.dart';

class HostAndPortTextField extends StatelessWidget {
  final HostAndPort value;

  final String hostHint;
  final String hostError;
  final String portHint;
  final String portError;
  final int ratio;

  const HostAndPortTextField(
      {required this.value,
      this.hostHint = 'Host',
      this.hostError = 'Input host',
      this.portHint = 'Port',
      this.portError = 'Input port',
      this.ratio = 2});

  @override
  Widget build(BuildContext context) {
    return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[Expanded(flex: ratio, child: _host()), Expanded(child: _port())]);
  }

  Widget _host() {
    return TextFieldEx(
        validator: (String text) {
          return HostAndPort.isValidHost(text) ? null : hostError;
        },
        hintText: hostHint,
        errorText: hostError,
        init: value.host,
        onFieldChanged: (val) {
          value.host = val;
        });
  }

  Widget _port() {
    return NumberTextField.integer(
        canBeEmpty: false,
        hintText: 'Port',
        initInt: value.port,
        onFieldChangedInt: (val) {
          if (val != null) {
            value.port = val;
          }
        });
  }
}

class SizeField extends StatelessWidget {
  final Size value;

  const SizeField(this.value);

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_widthField(), const Text('x'), _heightField()]);
  }

  Widget _widthField() {
    return Expanded(
        child: NumberTextField.integer(
            canBeEmpty: false,
            hintText: 'Width',
            initInt: value.width,
            minInt: Size.MIN_WIDTH,
            onFieldChangedInt: (term) {
              if (term != null) value.width = term;
            }));
  }

  Widget _heightField() {
    return Expanded(
        child: NumberTextField.integer(
            canBeEmpty: false,
            hintText: 'Height',
            initInt: value.height,
            minInt: Size.MIN_HEIGHT,
            onFieldChangedInt: (term) {
              if (term != null) {
                value.height = term;
              }
            }));
  }
}

class PointField extends StatelessWidget {
  final Point value;

  const PointField(this.value);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[_widthField(), _heightField()]);
  }

  Widget _widthField() {
    return Expanded(
        child: NumberTextField.integer(
            canBeEmpty: false,
            hintText: 'X',
            initInt: value.x,
            onFieldChangedInt: (term) {
              if (term != null) value.x = term;
            }));
  }

  Widget _heightField() {
    return Expanded(
        child: NumberTextField.integer(
            canBeEmpty: false,
            hintText: 'Y',
            initInt: value.y,
            onFieldChangedInt: (term) {
              if (term != null) value.y = term;
            }));
  }
}

class AspectRatioField extends StatelessWidget {
  final Rational value;

  const AspectRatioField(this.value);

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: <Widget>[_numField(), _denField()]);
  }

  Widget _numField() {
    return Expanded(
        child: NumberTextField.integer(
            hintText: 'Num',
            canBeEmpty: false,
            initInt: value.numerator,
            onFieldChangedInt: (term) {
              if (term != null) value.numerator = term;
            }));
  }

  Widget _denField() {
    return Expanded(
        child: NumberTextField.integer(
            hintText: 'Den',
            canBeEmpty: false,
            initInt: value.denominator,
            onFieldChangedInt: (term) {
              if (term != null) value.denominator = term;
            }));
  }
}

class LogoField extends StatelessWidget {
  final Logo value;

  const LogoField(this.value);

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      _pathField(),
      SizeField(value.size),
      PointField(value.position),
      _alphaField()
    ]);
  }

  Widget _pathField() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        minSymbols: FilePath.MIN_PATH_LENGTH,
        maxSymbols: FilePath.MAX_PATH_LENGTH,
        hintText: 'Path',
        errorText: 'Enter path',
        init: value.path,
        onFieldChanged: (term) {
          value.path = term;
        });
  }

  Widget _alphaField() {
    return NumberTextField.decimal(
        hintText: 'Alpha',
        initDouble: value.alpha,
        minDouble: Alpha.MIN,
        maxDouble: Alpha.MAX,
        canBeEmpty: false,
        onFieldChangedDouble: (term) {
          if (term != null) value.alpha = term;
        });
  }
}

class RsvgLogoField extends StatelessWidget {
  final RsvgLogo value;

  const RsvgLogoField(this.value);

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_pathField(), SizeField(value.size), PointField(value.position)]);
  }

  Widget _pathField() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        minSymbols: FilePath.MIN_PATH_LENGTH,
        maxSymbols: FilePath.MAX_PATH_LENGTH,
        hintText: 'Path',
        errorText: 'Enter path',
        init: value.path,
        onFieldChanged: (term) {
          value.path = term;
        });
  }
}

class TextOverlayField extends StatelessWidget {
  final TextOverlay value;

  const TextOverlayField(this.value);

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_textField(), _xAbsoluteField(), _yAbsoluteField(), _fontField()]);
  }

  Widget _textField() {
    return TextFieldEx(
        minSymbols: 1,
        maxSymbols: 512,
        hintText: 'Text',
        errorText: 'Enter text',
        init: value.text,
        onFieldChanged: (term) {
          value.text = term;
        });
  }

  Widget _xAbsoluteField() {
    return NumberTextField.decimal(
        hintText: 'X-Absolute',
        initDouble: value.xAbsolute,
        minDouble: 0.0,
        maxDouble: 1.0,
        canBeEmpty: false,
        onFieldChangedDouble: (term) {
          if (term != null) {
            value.xAbsolute = term;
          }
        });
  }

  Widget _yAbsoluteField() {
    return NumberTextField.decimal(
        hintText: 'Y-Absolute',
        initDouble: value.yAbsolute,
        minDouble: 0.0,
        maxDouble: 1.0,
        canBeEmpty: false,
        onFieldChangedDouble: (term) {
          if (term != null) {
            value.yAbsolute = term;
          }
        });
  }

  Widget _fontField() {
    return OptionalFieldTile(
        title: 'Font',
        init: value.font != null,
        onChanged: (bool chan) {
          value.font = chan ? Font(family: 'Sans', size: 18) : null;
        },
        builder: () {
          return Row(mainAxisSize: MainAxisSize.min, children: [
            Expanded(
                child: TextFieldEx(
                    minSymbols: 1,
                    maxSymbols: 512,
                    hintText: 'Font family',
                    errorText: 'Enter font family',
                    init: value.font!.family,
                    onFieldChanged: (String term) {
                      value.font!.family = term;
                    })),
            Expanded(
                child: NumberTextField.integer(
                    initInt: value.font!.size,
                    minInt: 1,
                    maxInt: 1000,
                    onFieldChangedInt: (int? size) {
                      if (size != null) {
                        value.font!.size = size;
                      }
                    }))
          ]);
        });
  }
}

class MachineLearningField extends StatelessWidget {
  final MachineLearning init;

  const MachineLearningField(this.init);

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      _backendField(),
      _modelPathField(),
      _trackingField(),
      SaveImagesTile(init),
      _overlayField()
    ]);
  }

  Widget _backendField() {
    return DropdownButtonEx<MlBackend>(
        hint: init.backend.toHumanReadable(),
        value: init.backend,
        values: MlBackend.values,
        onChanged: (c) {
          init.backend = c;
        },
        itemBuilder: (MlBackend value) {
          return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
        });
  }

  Widget _modelPathField() {
    return TextFieldEx(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'Model URL',
        errorText: 'Enter model url',
        init: init.modelUrl,
        onFieldChanged: (term) {
          init.modelUrl = term;
        });
  }

  Widget _trackingField() {
    return StateCheckBox(
        title: 'Tracking',
        init: init.tracking,
        onChanged: (tracking) {
          init.tracking = tracking;
        });
  }

  Widget _overlayField() {
    return StateCheckBox(
        title: 'Overlay',
        init: init.overlay,
        onChanged: (overlay) {
          init.overlay = overlay;
        });
  }
}

class BackgroundEffectField<S extends EncodeStream> extends StatefulWidget {
  final S stream;

  const BackgroundEffectField(this.stream);

  @override
  _BackgroundEffectFieldState<S> createState() {
    return _BackgroundEffectFieldState<S>();
  }
}

class _BackgroundEffectFieldState<S extends EncodeStream> extends State<BackgroundEffectField<S>> {
  BackgroundEffect get value => widget.stream.backgroundEffect!;

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      DropdownButtonEx<BackgroundEffectType>(
          hint: value.type.toHumanReadable(),
          value: value.type,
          values: BackgroundEffectType.values,
          onChanged: (c) {
            setState(() {
              switch (c) {
                case BackgroundEffectType.BLUR:
                  widget.stream.backgroundEffect = BlurBackgroundEffect();
                  break;
                case BackgroundEffectType.IMAGE:
                  widget.stream.backgroundEffect = ImageBackgroundEffect.createDefault();
                  break;
                case BackgroundEffectType.COLOR:
                  widget.stream.backgroundEffect = ColorBackgroundEffect();
                  break;
              }
            });
          },
          itemBuilder: (BackgroundEffectType value) {
            return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
          }),
      if (value.type == BackgroundEffectType.BLUR) _blur(),
      if (value.type == BackgroundEffectType.IMAGE) _image(),
      if (value.type == BackgroundEffectType.COLOR) _color()
    ]);
  }

  Widget _blur() {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Row(children: [
          const Text('0.0'),
          Expanded(
              child: Slider(
                  value: (value as BlurBackgroundEffect).strength,
                  onChanged: (newValue) {
                    setState(() {
                      (value as BlurBackgroundEffect).strength = newValue;
                    });
                  })),
          const Text('1.0')
        ]));
  }

  Widget _image() {
    final effect = value as ImageBackgroundEffect;
    return TextFieldEx(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'URL',
        errorText: 'Enter URL',
        init: effect.path,
        onFieldChanged: (term) {
          effect.path = term;
        });
  }

  Widget _color() {
    return Padding(
        padding: const EdgeInsets.all(16.0),
        child: ColorPicker(
            initialColor: Color((value as ColorBackgroundEffect).hex),
            onChanged: (Color color) {
              (value as ColorBackgroundEffect).hex = color.value;
            }));
  }
}

class StreamTTLField extends StatelessWidget {
  final StreamTTL init;

  const StreamTTLField(this.init);

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_autoExitField(), _phoenixField()]);
  }

  Widget _autoExitField() {
    return NumberTextField.integer(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        minInt: AutoExitTime.MIN,
        maxInt: AutoExitTime.MAX,
        hintText: 'Stream Time Work',
        canBeEmpty: false,
        initInt: init.ttl,
        onFieldChangedInt: (term) {
          if (term != null) {
            init.ttl = term;
          }
        });
  }

  Widget _phoenixField() {
    return StateCheckBox(
        title: 'Phoenix',
        init: init.phoenix,
        onChanged: (value) {
          init.phoenix = value;
        });
  }
}

class StreamNotificationField extends StatefulWidget {
  final List<NotificationStreamContact> notificationContacts;

  const StreamNotificationField(this.notificationContacts);

  @override
  State<StreamNotificationField> createState() => _StreamNotificationFieldState();
}

class _StreamNotificationFieldState extends State<StreamNotificationField> {
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Row(children: [
        Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return _contactFields(widget.notificationContacts[index]);
                },
                itemCount: widget.notificationContacts.length))
      ]),
      _addNotificationContact()
    ]);
  }

  Widget _contactFields(NotificationStreamContact contact) {
    return Row(
      children: <Widget>[
        Expanded(
          flex: 3,
          child: TextFieldEx(
            hintText: 'Contact',
            onFieldChanged: (value) {
              contact.email = value;
            },
            init: contact.email,
          ),
        ),
        Expanded(
          flex: 3,
          child: DropdownButtonEx<NotificationStreamType>(
            hint: 'Notification type',
            value: contact.type,
            values: NotificationStreamType.values,
            onChanged: (value) {
              contact.type = value;
            },
            itemBuilder: (value) {
              return DropdownMenuItem<NotificationStreamType>(
                  child: Text(value.toHumanReadable()), value: value);
            },
          ),
        ),
        Expanded(
          flex: 1,
          child: IconButton(
              onPressed: () {
                setState(() {
                  widget.notificationContacts.remove(contact);
                });
              },
              icon: Icon(Icons.delete)),
        ),
      ],
    );
  }

  Widget _addNotificationContact() {
    return ElevatedButton(
        onPressed: () {
          setState(() {
            widget.notificationContacts.add(
                NotificationStreamContact(email: '', type: NotificationStreamType.STREAM_FINISHED));
          });
        },
        child: Text('Add contact'));
  }
}
