import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/widgets.dart';

typedef ColorChanged = void Function(Color color);

class ColorBloc {
  Color _color;
  final StreamController<Color> _updates = StreamController<Color>.broadcast();

  ColorBloc(Color color) : _color = color;

  Color get color => _color;

  Stream<Color> get updates => _updates.stream;

  String get hex => _color.value.toRadixString(16).substring(2);

  set hex(String value) {
    _updateColor(Color(int.parse(value.isEmpty ? 'FF000000' : value, radix: 16) + 0xFF000000));
  }

  int get red => _color.red;

  set red(int value) {
    _updateColor(_color.withRed(value));
  }

  int get green => _color.green;

  set green(int value) {
    _updateColor(_color.withGreen(value));
  }

  int get blue => _color.blue;

  set blue(int value) {
    _updateColor(_color.withBlue(value));
  }

  void dispose() {
    _updates.close();
  }

  void _updateColor(Color color) {
    _color = color;
    _updates.add(color);
  }
}

class ColorPicker extends StatefulWidget {
  final Color initialColor;
  final ColorChanged onChanged;

  const ColorPicker({required this.initialColor, required this.onChanged, Key? key})
      : super(key: key);

  @override
  _ColorPickerState createState() => _ColorPickerState();
}

class _ColorPickerState extends State<ColorPicker> {
  late ColorBloc _bloc;
  late TextEditingController _controller;
  late StreamSubscription<Color> _colorUpdates;

  @override
  void initState() {
    super.initState();
    _bloc = ColorBloc(widget.initialColor);
    _controller = TextEditingController(text: _bloc.hex);
    _colorUpdates = _bloc.updates.listen(widget.onChanged);
  }

  @override
  void dispose() {
    super.dispose();
    _colorUpdates.cancel();
    _controller.dispose();
    _bloc.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      SizedBox(
          height: 48 * 3,
          width: 80,
          child: Column(children: [
            Expanded(
                child: StreamBuilder<Color>(
                    initialData: _bloc.color,
                    stream: _bloc.updates,
                    builder: (context, snapshot) {
                      return Container(color: snapshot.data);
                    })),
            const SizedBox(height: 16),
            TextFieldEx(
                hintText: 'HEX',
                controller: _controller,
                padding: const EdgeInsets.all(0),
                formatters: [TextFieldFilter.license, LengthLimitingTextInputFormatter(6)],
                init: _bloc.hex,
                onFieldChanged: (term) {
                  _bloc.hex = term;
                })
          ])),
      const SizedBox(width: 16),
      Expanded(
          child: Column(children: [
        RGBSliderRed(_bloc, (_) => _controller.text = _bloc.hex),
        RGBSliderGreen(_bloc, (_) => _controller.text = _bloc.hex),
        RGBSliderBlue(_bloc, (_) => _controller.text = _bloc.hex)
      ]))
    ]);
  }
}

class RGBSliderRed extends RGBSlider {
  const RGBSliderRed(ColorBloc handler, ColorChanged onChanged, {Key? key})
      : super(handler, 'R', Colors.red, onChanged, key: key);

  @override
  int getValue(Color color) => color.red;

  @override
  void setValue(int value) {
    handler.red = value;
  }
}

class RGBSliderGreen extends RGBSlider {
  const RGBSliderGreen(ColorBloc handler, ColorChanged onChanged, {Key? key})
      : super(handler, 'G', Colors.green, onChanged, key: key);

  @override
  int getValue(Color color) => color.green;

  @override
  void setValue(int value) {
    handler.green = value;
  }
}

class RGBSliderBlue extends RGBSlider {
  const RGBSliderBlue(ColorBloc handler, ColorChanged onChanged, {Key? key})
      : super(handler, 'B', Colors.blue, onChanged, key: key);

  @override
  int getValue(Color color) => color.blue;

  @override
  void setValue(int value) {
    handler.blue = value;
  }
}

abstract class RGBSlider extends StatelessWidget {
  const RGBSlider(this.handler, this.name, this.color, this.onChanged, {Key? key})
      : super(key: key);

  final ColorBloc handler;
  final String name;
  final Color color;
  final ColorChanged onChanged;

  @override
  Widget build(BuildContext context) {
    return DefaultTextStyle(
        style: Theme.of(context).textTheme.headline6!.copyWith(fontSize: 16),
        child: Row(children: [
          Text(name),
          Expanded(
              child: StreamBuilder<Color>(
                  initialData: handler.color,
                  stream: handler.updates,
                  builder: (context, snapshot) {
                    final int value = getValue(snapshot.data!);
                    return Row(children: [
                      Expanded(
                          child: SliderTheme(
                              data: SliderThemeData(
                                  activeTrackColor: color,
                                  inactiveTrackColor: Colors.black12,
                                  overlayColor: color.withOpacity(0.12),
                                  thumbColor: color),
                              child: Slider(
                                  value: value.toDouble(),
                                  max: 255,
                                  divisions: 255,
                                  onChanged: (value) {
                                    setValue(value.toInt());
                                    onChanged(handler.color);
                                  }))),
                      Text('$value')
                    ]);
                  }))
        ]));
  }

  int getValue(Color color);

  void setValue(int value);
}
