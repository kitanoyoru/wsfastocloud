import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/data/services/api/models/connection_info.dart';
import 'package:wsfastocloud/presenter/widgets/optional.dart';

class ConnectDialog extends StatelessWidget {
  final WSServer value;

  final String urlHint;
  final String urlError;

  const ConnectDialog({required this.value, this.urlHint = 'URL', this.urlError = 'Input URL'});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: const Text('Connect'),
        content: SingleChildScrollView(
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Row(children: [Expanded(flex: 2, child: _url())]),
          Row(children: [Expanded(flex: 2, child: _useLoginAndPasswordField())])
        ])),
        actions: [FlatButtonEx.filled(text: 'Connect', onPressed: () => _save(context))]);
  }

  Widget _url() {
    return TextFieldEx(
        validator: (String text) {
          return WSServer(url: value.url).isValid() ? null : urlError;
        },
        hintText: urlHint,
        errorText: urlError,
        init: value.url,
        onFieldChanged: (val) {
          value.url = val;
        });
  }

  Widget _useLoginAndPasswordField() {
    return OptionalFieldTile(
        title: 'Use login and password',
        init: value.needAuth(),
        onChanged: (state) {
          if (state) {
            value.password = WSServer.DEFAULT_PASSWORD;
            value.login = WSServer.DEFAULT_LOGIN;
          } else {
            value.login = null;
            value.password = null;
          }
        },
        builder: () {
          return Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [_loginField(), _passwordField()]);
        });
  }

  Widget _loginField() {
    return TextFieldEx(
      hintText: 'Login',
      errorText: 'Enter login',
      init: value.login,
      keyboardType: TextInputType.text,
      onFieldChanged: (val) => value.login = val,
    );
  }

  Widget _passwordField() {
    return PassWordTextField(
      hintText: 'Password',
      errorText: 'Enter password',
      init: value.password,
      onFieldChanged: (val) => value.password = val,
    );
  }

  void _save(BuildContext context) {
    final info = ConnectionInfo.fromUserInput(value);
    Navigator.pop(context, info);
  }
}
