import 'package:flutter/material.dart';

class ErrorDialog extends StatelessWidget {
  final String error;

  const ErrorDialog({Key? key, required this.error}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Error'),
      contentPadding: const EdgeInsets.all(8),
      content: Container(
        child: Text(error),
      ),
    );
  }
}
