import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';

class GetHardwareHashDialog extends StatelessWidget {
  GetHardwareHashDialog(this.host) : _algo = NodeAlgoInfo(host, AlgoType.HDD);

  final HostAndPort host;

  final NodeAlgoInfo _algo;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: const Text('Get hardware hash'),
        contentPadding: const EdgeInsets.all(8),
        content: SingleChildScrollView(
            child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[_TypePicker(_algo)])),
        actions: <Widget>[
          FlatButtonEx.notFilled(text: 'Cancel', onPressed: Navigator.of(context).pop),
          FlatButtonEx.filled(
              text: 'Get',
              onPressed: () {
                final copy = _algo.copy();
                Navigator.of(context).pop(copy);
              })
        ]);
  }
}

class _TypePicker extends DropdownButtonEx<AlgoType> {
  _TypePicker(NodeAlgoInfo message)
      : super(
            hint: message.algo.toHumanReadable(),
            value: message.algo,
            values: AlgoType.values,
            onChanged: (m) {
              message.algo = m;
            },
            itemBuilder: (AlgoType type) {
              return DropdownMenuItem(child: Text(type.toHumanReadable()), value: type);
            });
}
