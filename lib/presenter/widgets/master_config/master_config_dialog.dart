import 'package:fastocloud_dart_models/fastocloud_dart_models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/data/services/api/models/master_config_info.dart';
import 'package:wsfastocloud/presenter/widgets/add_edit_dialog.dart';
import 'package:wsfastocloud/presenter/widgets/master_config/master_config_widgets.dart';
import 'package:wsfastocloud/presenter/widgets/optional.dart';

class MasterConfigDialog extends StatefulWidget {
  final MasterConfigInfo init;
  final void Function(NodeAlgoInfo algo) onNodeHashRequested;

  MasterConfigDialog.edit(MasterConfigInfo value, this.onNodeHashRequested) : init = value.copy();

  @override
  State<MasterConfigDialog> createState() {
    return _MasterConfigDialogState();
  }
}

class _MasterConfigDialogState extends State<MasterConfigDialog> {
  MasterConfigInfo get config => widget.init;
  TextEditingController _textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AddEditDialog.wide(
        children: <Widget>[
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                _header('HLS host'),
                _hlsHostAndAliasField(),
                _header('CODs host'),
                _codsHostAndAliasField(),
                _header('VODs host'),
                _vodsHostAndAliasField(),
                Row(children: [Expanded(child: _nodeField())]),
                Row(children: [Expanded(child: _webRTCField())]),
                _corsField(),
                _authField(),
                _mailSettingsField(),
                _header('Auth content'),
                _authContent(),
                _vodInfoSettingsField(),
                _blackListField()
              ])
        ],
        onSave: () {
          Navigator.pop(context, config);
        });
  }

  Widget _header(String text) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Text(text, style: const TextStyle(fontWeight: FontWeight.bold)));
  }

  //fields

  List<Widget> _blackList() {
    List<Widget> blackList = [];
    config.blacklist.forEach((ipAddress) {
      blackList.add(Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        IpAddressField(value: ipAddress),
        IconButton(
            onPressed: () {
              setState(() {
                config.blacklist.remove(ipAddress);
              });
            },
            icon: Icon(Icons.delete))
      ]));
    });
    return blackList;
  }

  Widget _blackListField() {
    return Column(children: [
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        _header('Black list'),
        IconButton(
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (context) {
                    return _blackListDialog();
                  });
            },
            icon: Icon(Icons.add))
      ]),
      Column(children: _blackList())
    ]);
  }

  Widget _blackListDialog() {
    return SimpleDialog(title: Text('Add to black list'), children: [
      Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(children: [
            TextFieldEx(
                validator: (String text) {
                  return HostAndPort.isValidHost(text) ? null : 'Invalid IP address';
                },
                decoration:
                    InputDecoration(border: OutlineInputBorder(), labelText: 'Input IP Address'),
                controller: _textController),
            SizedBox(height: 5.0),
            Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
              FlatButtonEx.filled(
                  text: 'Close',
                  onPressed: () {
                    Navigator.of(context).pop();
                  }),
              FlatButtonEx.filled(
                  text: 'Add',
                  onPressed: () {
                    if (HostAndPort.isValidHost(_textController.text)) {
                      setState(() {
                        config.blacklist.add(IpAddress(ip: _textController.text));
                        Navigator.pop(context, config);
                      });
                    }
                  })
            ])
          ]))
    ]);
  }

  Widget _hlsHostAndAliasField() {
    return Row(children: [Expanded(child: HostAndAliasField(value: config.hlsHost))]);
  }

  Widget _codsHostAndAliasField() {
    return Row(children: [Expanded(child: HostAndAliasField(value: config.codsHost))]);
  }

  Widget _vodsHostAndAliasField() {
    return Row(children: [Expanded(child: HostAndAliasField(value: config.vodsHost))]);
  }

  Widget _nodeField() {
    return OptionalFieldTile(
        title: 'Node',
        init: config.node != null,
        onChanged: (value) {
          config.node = value ? Node.createDefault() : null;
        },
        builder: () {
          return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: NodeField(
                  value: config.node!, onNodeHashRequested: this.widget.onNodeHashRequested));
        });
  }

  Widget _webRTCField() {
    return OptionalFieldTile(
        title: 'WebRTC',
        init: config.webrtc != null,
        onChanged: (value) {
          config.webrtc = value ? Webrtc.createDefault() : null;
        },
        builder: () {
          return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: WebRTCField(value: config.webrtc!));
        });
  }

  Widget _authField() {
    return OptionalFieldTile(
        title: 'Auth',
        init: config.auth != null,
        onChanged: (value) {
          config.auth = value ? Auth.createDefault() : null;
        },
        builder: () {
          return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: AuthField(value: config.auth!));
        });
  }

  Widget _mailSettingsField() {
    return OptionalFieldTile(
        title: 'Alarm settings',
        init: config.streamNotification != null,
        onChanged: (value) {
          if (value) {
            config.streamNotification = MailConfig.createDefault();
          } else {
            config.streamNotification = null;
          }
        },
        builder: () {
          final currentNotification = config.streamNotification!;
          return Column(
            children: <Widget>[
              DropdownButtonEx<NotificationType>(
                  onChanged: (value) {
                    if (value == NotificationType.MAIL) {
                      setState(() {
                        config.streamNotification = MailConfig.createDefault();
                      });
                    }
                  },
                  value: currentNotification.type,
                  values: [NotificationType.MAIL],
                  itemBuilder: (value) {
                    return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
                  }),
              currentNotification is MailConfig ? MailConfigField(currentNotification) : Container()
            ],
          );
        });
  }

  Widget _authContent() {
    return DropdownButtonEx<AuthContentType>(
        value: config.authContent,
        values: AuthContentType.values,
        onChanged: (c) {
          setState(() {
            config.authContent = c;
          });
        },
        itemBuilder: (AuthContentType data) {
          return DropdownMenuItem(child: Text(data.toHumanReadable()), value: data);
        });
  }

  Widget _vodInfoSettingsField() {
    return OptionalFieldTile(
        title: 'Vod Info Settings',
        init: config.vodInfo != null,
        onChanged: (value) {
          config.vodInfo = value ? VodInfoSettings.createDefault() : null;
        },
        builder: () {
          return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: VodInfoSettingsField(vodInfo: config.vodInfo!));
        });
  }

  Widget _corsField() {
    return Row(children: [
      Expanded(
          child: CheckboxListTile(
              title: Text('Cors'),
              value: config.cors,
              onChanged: (val) {
                setState(() {
                  config.cors = val!;
                });
              }))
    ]);
  }
}
