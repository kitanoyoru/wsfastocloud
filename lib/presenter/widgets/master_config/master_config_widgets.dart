import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/data/services/api/models/master_config_info.dart';
import 'package:wsfastocloud/presenter/stats_header.dart';
import 'package:wsfastocloud/presenter/widgets/base.dart';
import 'package:wsfastocloud/presenter/widgets/master_config/get_hardware_hash_dialog.dart';

class WebRTCField extends StatelessWidget {
  final Webrtc value;

  const WebRTCField({Key? key, required this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(children: <Widget>[Expanded(child: _stun()), Expanded(child: _turn())]);
  }

  Widget _stun() {
    return TextFieldEx(
        hintText: 'Stun',
        errorText: 'Input stun',
        init: value.stun,
        onFieldChanged: (val) {
          value.stun = val;
        });
  }

  Widget _turn() {
    return TextFieldEx(
        hintText: 'Turn',
        errorText: 'Input turn',
        init: value.turn,
        onFieldChanged: (val) {
          value.turn = val;
        });
  }
}

class HostAndAliasField extends StatelessWidget {
  final HostAndAlias value;

  const HostAndAliasField({Key? key, required this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(children: <Widget>[
      Expanded(child: _alias()),
      Expanded(child: HostAndPortTextField(value: value.host))
    ]);
  }

  Widget _alias() {
    return TextFieldEx(
        hintText: 'Alias',
        errorText: 'Input alias',
        init: value.alias,
        onFieldChanged: (val) {
          value.alias = val;
        });
  }
}

class IpAddressField extends StatelessWidget {
  final IpAddress value;

  const IpAddressField({Key? key, required this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: TextFieldEx(
            hintText: 'IP',
            errorText: 'Input IP',
            init: value.ip,
            onFieldChanged: (val) {
              value.ip = val;
            }));
  }
}

class AuthField extends StatelessWidget {
  final Auth value;

  const AuthField({super.key, required this.value});

  @override
  Widget build(BuildContext context) {
    return Row(children: [Expanded(child: _login()), Expanded(child: _password())]);
  }

  Widget _login() {
    return TextFieldEx(
        hintText: 'Login',
        errorText: 'Input Login',
        init: value.login,
        onFieldChanged: (val) {
          value.login = val;
        });
  }

  Widget _password() {
    return PassWordTextField(
        hintText: 'Password',
        errorText: 'Input Password',
        init: value.password,
        onFieldChanged: (val) {
          value.password = val;
        });
  }
}

class MailConfigField extends StatelessWidget {
  final MailConfig mailConfig;

  MailConfigField(this.mailConfig);

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Row(children: <Widget>[_mailServerField(), _mailPortField()]),
      Row(children: <Widget>[_userField(), _userPasswordField()])
    ]);
  }

  Widget _mailServerField() {
    return Expanded(
        child: TextFieldEx(
            hintText: 'Mail server',
            errorText: 'Input mail server',
            init: mailConfig.mailServer,
            onFieldChanged: (val) {
              mailConfig.mailServer = val;
            }));
  }

  Widget _mailPortField() {
    return Expanded(
        child: NumberTextField.integer(
            hintText: 'Mail port',
            initInt: mailConfig.mailPort,
            onFieldChangedInt: (val) {
              if (val != null) {
                mailConfig.mailPort = val;
              }
            }));
  }

  Widget _userField() {
    return Expanded(
        child: TextFieldEx(
            hintText: 'User',
            errorText: 'Input user',
            init: mailConfig.user,
            onFieldChanged: (val) {
              mailConfig.user = val;
            }));
  }

  Widget _userPasswordField() {
    return Expanded(
        child: PassWordTextField(
            hintText: 'User password',
            errorText: 'Input user password',
            init: mailConfig.mailPassword,
            onFieldChanged: (val) {
              mailConfig.mailPassword = val;
            }));
  }
}

class VodInfoSettingsField extends StatelessWidget {
  final VodInfoSettings vodInfo;

  const VodInfoSettingsField({required this.vodInfo, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [Expanded(child: _type()), Expanded(child: _key())],
    );
  }

  Widget _type() {
    return DropdownButtonEx<TypeVodService>(
        hint: 'Vod Settings Type',
        value: vodInfo.service,
        values: TypeVodService.values,
        onChanged: (c) {
          vodInfo.service = c;
        },
        itemBuilder: (TypeVodService value) {
          return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
        });
  }

  Widget _key() {
    return TextFieldEx(
        hintText: 'Key',
        errorText: 'Input Key',
        init: vodInfo.tmdb.key,
        onFieldChanged: (val) {
          vodInfo.tmdb.key = val;
        });
  }
}

class NodeField extends StatelessWidget {
  final Node value;
  final void Function(NodeAlgoInfo algo) onNodeHashRequested;

  const NodeField({Key? key, required this.value, required this.onNodeHashRequested})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Row(children: [
        Expanded(child: HostAndPortTextField(value: value.host)),
        StatsButton(
            title: 'Get hash',
            onPressed: () async {
              final hashFor = await showDialog(
                  context: context,
                  builder: (context) {
                    return GetHardwareHashDialog(value.host);
                  });
              if (hashFor == null) {
                return;
              }

              onNodeHashRequested.call(hashFor);
            })
      ]),
      _key()
    ]);
  }

  Widget _key() {
    return TextFieldEx(
        hintText: 'Key',
        errorText: 'Input key',
        init: value.key,
        onFieldChanged: (val) {
          value.key = val;
        });
  }
}
