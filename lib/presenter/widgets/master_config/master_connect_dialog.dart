import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';

class Credentials {
  String login;
  String password;

  Credentials(this.login, this.password);
}

class MasterConnectDialog extends StatefulWidget {
  MasterConnectDialog({Key? key}) : super(key: key);

  @override
  State<MasterConnectDialog> createState() => _ConnectDialogState();
}

class _ConnectDialogState extends State<MasterConnectDialog> {
  final _loginController = TextEditingController(text: 'master');
  final _passwordController = TextEditingController(text: 'master');

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Connect'),
      content: SingleChildScrollView(
        child: Column(mainAxisSize: MainAxisSize.min, children: [_loginAndPassword()]),
      ),
      actions: [FlatButtonEx.filled(text: 'Connect', onPressed: () => _save(context))],
    );
  }

  Widget _loginAndPassword() {
    return Column(children: [
      TextFieldEx(hintText: 'Login', controller: _loginController),
      PassWordTextField(hintText: 'Password', controller: _passwordController)
    ]);
  }

  void _save(BuildContext context) {
    Navigator.pop(context, Credentials(_loginController.text, _passwordController.text));
  }
}
