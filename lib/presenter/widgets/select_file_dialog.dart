import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:provider/provider.dart';
import 'package:universal_html/html.dart' as html;
import 'package:wsfastocloud/data/services/api/fetcher.dart';

enum UploadDialogs { dialog_true, dialog_false, dialog_showData, dialog_error }

class MediaInfo {
  static const _ROTATION_FIELD = 'rotation';
  static const _WIDTH_FIELD = 'width';
  static const _HEIGHT_FIELD = 'height';
  static const _DURATION_FIELD = 'duration';
  static const _URL_FIELD = 'url';

  int rotation;
  int width;
  int height;
  int duration;
  String url;

  MediaInfo(
      {required this.rotation,
      required this.width,
      required this.height,
      required this.duration,
      required this.url});

  factory MediaInfo.fromJson(Map<String, dynamic> json) {
    return MediaInfo(
        rotation: json[_ROTATION_FIELD],
        width: json[_WIDTH_FIELD],
        height: json[_HEIGHT_FIELD],
        duration: json[_DURATION_FIELD],
        url: json[_URL_FIELD]);
  }

  Duration getDuration() {
    return Duration(seconds: duration ~/ 1000);
  }
}

class SelectFileDialog extends StatefulWidget {
  @override
  _SelectFileDialogState createState() {
    return _SelectFileDialogState();
  }
}

class _SelectFileDialogState extends State<SelectFileDialog> {
  Map<String, List<int>> _filesData = {};
  UploadDialogs _parsingStreams = UploadDialogs.dialog_false;
  MediaInfo? mData;
  String? path;

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
        title: const Text('File Upload'),
        children:
            _parsingStreams == UploadDialogs.dialog_false ? _fields() : uploadOut(_parsingStreams));
  }

  List<Widget> _fields() {
    return <Widget>[
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: FlatButtonEx.filled(text: 'Select a file', onPressed: _startWebFilePicker)),
      const SizedBox(height: 8),
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: FlatButtonEx.filled(
              text: 'Upload', onPressed: _filesData.isEmpty ? null : _makeRequest))
    ];
  }

  List<Widget> uploadOut(UploadDialogs uploadDialogs) {
    if (uploadDialogs == UploadDialogs.dialog_true) {
      return <Widget>[
        const Center(
            child: Padding(padding: EdgeInsets.all(8.0), child: CircularProgressIndicator()))
      ];
    } else if (uploadDialogs == UploadDialogs.dialog_showData) {
      final generateHttpUrl = context.read<Fetcher>().getBackendEndpoint(path!);
      return <Widget>[
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Text(
                'Rotation: ${mData!.rotation.toString()}°\nSize: ${mData!.width} x ${mData!.height}\nDuration: ${_printDuration(mData!.getDuration())}')),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: TextFieldEx.readOnly(hint: 'File URL', init: mData!.url.toString())),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: TextFieldEx.readOnly(hint: 'Http URL', init: generateHttpUrl.toString())),
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: FlatButtonEx.filled(
                text: 'Close',
                onPressed: () {
                  Navigator.of(context).pop();
                }))
      ];
    } else {
      return <Widget>[
        Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0), child: Text('Error in upload')),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: FlatButtonEx.filled(
              text: 'Close',
              onPressed: () {
                Navigator.of(context).pop();
              }),
        )
      ];
    }
  }

  // private:
  void _startWebFilePicker() async {
    _filesData = {};
    if (kIsWeb) {
      final html.FileUploadInputElement uploadInput = html.FileUploadInputElement();
      uploadInput.multiple = false;
      uploadInput.draggable = true;
      uploadInput.click();

      uploadInput.addEventListener('change', (e) {
        for (int i = 0; i < uploadInput.files!.length; ++i) {
          final file = uploadInput.files![i];
          final reader = html.FileReader();
          reader.onLoadEnd.listen((e) {
            final base64 = reader.result.toString().split(',').last;
            final contents = const Base64Decoder().convert(base64);
            _filesData[file.name] = contents;
            if (mounted) {
              setState(() {});
            }
          });
          reader.readAsDataUrl(file);
        }
        if (mounted) {
          setState(() {});
        }
      });
    } else {
      try {
        final FilePickerResult? result = await FilePicker.platform.pickFiles();
        if (result == null) {
          return;
        }

        for (int i = 0; i < result.files.length; ++i) {
          final PlatformFile file = result.files[i];
          final data = File(file.path!).readAsBytesSync();
          _filesData[file.name] = data;
        }
        if (mounted) {
          setState(() {});
        }
      } on PlatformException catch (e) {
        log('Unsupported operation: $e');
      }
    }
  }

  void _makeRequest() {
    if (mounted) {
      setState(() {
        _parsingStreams = UploadDialogs.dialog_true;
      });
    }
    final Map<String, dynamic> field = {};

    context.read<Fetcher>().sendFiles('/server/video/upload', _filesData, field).then((resp) {
      if (resp.statusCode == 200) {
        final respData = httpDataResponseFromString(resp.body);
        if (respData != null) {
          final err = respData.error();
          if (err != null) {
            throw ErrorHttp(401, 'backend response', err);
          } else {
            final content = respData.contentMap();
            setState(() {
              _parsingStreams = UploadDialogs.dialog_showData;
              mData = MediaInfo.fromJson(content!['info']);
              path = content['path'];
            });
          }
        }
      } else {
        setState(() {
          _parsingStreams = UploadDialogs.dialog_error;
        });
      }
    }, onError: (error) {
      if (mounted) {
        setState(() {
          _parsingStreams = UploadDialogs.dialog_false;
        });
      }
      showError(context, error);
    });
  }

  String _printDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, '0');
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return '${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds';
  }
}
