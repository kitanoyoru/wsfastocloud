import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class ServerDetailsBuilder extends StatelessWidget {
  final Widget stats;
  final Widget table;
  final Widget desc;

  const ServerDetailsBuilder({required this.stats, required this.table, required this.desc});

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(desktop: _DesktopLayout(desc), mobile: _MobileLayout(stats, table));
  }
}

// layouts
class _MobileLayout extends StatefulWidget {
  final Widget stats;
  final Widget table;

  const _MobileLayout(this.stats, this.table);

  @override
  _MobileLayoutState createState() {
    return _MobileLayoutState();
  }
}

class _MobileLayoutState extends State<_MobileLayout> with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this, initialIndex: 1);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: kIsWeb
            ? MediaQuery.of(context).size.height * 0.86
            : MediaQuery.of(context).size.height * 0.849,
        child: Column(children: [
          TabBar(
              controller: _tabController,
              tabs: [const Tab(text: 'Statistics'), Tab(text: 'Content')],
              labelColor: Colors.black),
          SizedBox(
              height: MediaQuery.of(context).size.height * 0.78,
              child: TabBarView(controller: _tabController, children: [
                SingleChildScrollView(
                    child: Padding(padding: const EdgeInsets.only(top: 4), child: widget.stats)),
                SingleChildScrollView(
                    child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 8.0), child: widget.table))
              ]))
        ]));
  }
}

class _DesktopLayout extends StatelessWidget {
  final Widget desc;

  const _DesktopLayout(this.desc, [Key? key]) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return desc;
  }
}
