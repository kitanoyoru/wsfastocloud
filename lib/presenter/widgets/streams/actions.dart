import 'package:flutter/material.dart';

class StreamActionIcon extends IconButton {
  final void Function() onTap;

  const StreamActionIcon.start(this.onTap)
      : super(icon: const Icon(Icons.play_arrow), tooltip: 'Start', onPressed: onTap);

  const StreamActionIcon.embedOutput(this.onTap)
      : super(icon: const Icon(Icons.my_library_add), tooltip: 'Embed output', onPressed: onTap);

  const StreamActionIcon.copyStream(this.onTap)
      : super(icon: const Icon(Icons.copy), tooltip: 'Copy', onPressed: onTap);

  const StreamActionIcon.stop(this.onTap)
      : super(icon: const Icon(Icons.stop), tooltip: 'Stop', onPressed: onTap);

  const StreamActionIcon.kill(this.onTap)
      : super(icon: const Icon(Icons.not_interested), tooltip: 'Kill', onPressed: onTap);

  const StreamActionIcon.restart(this.onTap)
      : super(icon: const Icon(Icons.loop), tooltip: 'Restart', onPressed: onTap);

  const StreamActionIcon.changeStream(this.onTap)
      : super(
            icon: const Icon(Icons.switch_left_sharp), tooltip: 'Change stream', onPressed: onTap);

  const StreamActionIcon.play(this.onTap)
      : super(icon: const Icon(Icons.playlist_play), tooltip: 'Play', onPressed: onTap);

  const StreamActionIcon.playOutput(this.onTap)
      : super(icon: const Icon(Icons.tv), tooltip: 'Play output', onPressed: onTap);

  const StreamActionIcon.getLog(this.onTap)
      : super(icon: const Icon(Icons.file_download), tooltip: 'Get log', onPressed: onTap);

  const StreamActionIcon.viewLog(this.onTap)
      : super(icon: const Icon(Icons.info), tooltip: 'View log', onPressed: onTap);

  const StreamActionIcon.getPipeline(this.onTap)
      : super(icon: const Icon(Icons.trending_flat), tooltip: 'Get pipeline', onPressed: onTap);

  const StreamActionIcon.viewPipeline(this.onTap)
      : super(icon: const Icon(Icons.trending_flat), tooltip: 'View pipeline', onPressed: onTap);

  const StreamActionIcon.refresh(this.onTap)
      : super(icon: const Icon(Icons.refresh), tooltip: 'Refresh', onPressed: onTap);

  const StreamActionIcon.getConfig(this.onTap)
      : super(icon: const Icon(Icons.settings), tooltip: 'Get config', onPressed: onTap);
}
