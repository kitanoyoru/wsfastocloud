import 'dart:async';

import 'package:fastocloud_dart_models/models.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:wsfastocloud/data/services/api/fetcher.dart';
import 'package:wsfastocloud/presenter/widgets/add_edit_dialog.dart';

class TabWidget {
  final String name;
  final Widget widget;

  TabWidget(this.name, this.widget);
}

abstract class IStreamEditor {
  Defaults defaults();

  Future<bool> add(IStream stream);

  Future<bool> edit(IStream stream);

  Future<String> probeOutUrl(OutputUrl url);

  Future<String> probeInUrl(InputUrl url);

  Future<bool> uploadM3uFile(StreamType type, bool isSeries, double imageCheckTime,
      final bool skipGroups, final List<String> groups, Map<String, List<int>> data);
}

abstract class BaseStreamPage<S extends IStream> extends StatefulWidget {
  final S init;
  final LiveServer server;
  final WsMode? mode;
  final IStreamEditor editor;
  final bool isEpisode;

  static IStream _createDefault(
      LiveServer server, IStreamEditor editor, StreamType type, WsMode? mode, bool isSerial) {
    String defaultStreamLogoIcon() {
      final defaults = editor.defaults();
      return defaults.streamLogoIcon;
    }

    String defaultBackgroundUrl() {
      final defaults = editor.defaults();
      return defaults.backgroundUrl;
    }

    String defaultVodTrailerUrl() {
      final defaults = editor.defaults();
      return defaults.vodTrailerUrl;
    }

    final stream = server.createDefault(
        type, defaultStreamLogoIcon(), defaultBackgroundUrl(), defaultVodTrailerUrl(), isSerial);
    // need to patch hls, http output
    return stream;
  }

  BaseStreamPage.add(this.server, this.editor, StreamType type, this.mode, {this.isEpisode = false})
      : init = _createDefault(server, editor, type, mode, isEpisode) as S;

  BaseStreamPage.edit(this.server, this.editor, S stream, this.mode, {this.isEpisode = false})
      : init = stream.copy() as S;
}

abstract class BaseStreamPageState<T extends BaseStreamPage, S extends IStream> extends State<T>
    with SingleTickerProviderStateMixin {
  late S stream;

  S get origStream => widget.init as S;

  bool get isAdd => stream.id == null;

  @override
  void initState() {
    super.initState();
    stream = widget.init as S;
  }

  @override
  Widget build(BuildContext context) {
    return _Dialog(isAdd: isAdd, tabs: tabs(), onSave: save);
  }

  List<TabWidget> tabs();

  bool validate();

  void save() {
    final _validator = validate();

    if (_validator) {
      isAdd ? _addStream() : _editStream();
    }
  }

  void _addStream() {
    final resp = widget.editor.add(stream);
    resp.then((value) {
      exit();
    }, onError: (error) {
      showError(context, error);
    });
  }

  void _editStream() {
    final resp = widget.editor.edit(stream);
    resp.then((value) {
      exit();
    }, onError: (error) {
      showError(context, error);
    });
  }

  void exit() {
    Navigator.of(context).pop();
  }
}

class _Dialog extends StatefulWidget {
  const _Dialog({required this.tabs, required this.isAdd, required this.onSave, Key? key})
      : super(key: key);

  final List<TabWidget> tabs;
  final bool isAdd;
  final VoidCallback onSave;

  @override
  _DialogState createState() => _DialogState();
}

class _DialogState extends State<_Dialog> {
  final List<GlobalKey<FormState>> _formKeys = [];
  final StreamController<int> _currentTab = StreamController<int>.broadcast();
  final StreamController<List<int>> _errors = StreamController<List<int>>.broadcast();

  @override
  void initState() {
    super.initState();
    for (int i = 0; i < widget.tabs.length; i++) {
      _formKeys.add(GlobalKey<FormState>(debugLabel: '$i'));
    }
  }

  @override
  void dispose() {
    _currentTab.close();
    _errors.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BaseAddEditDialog(
        isAdd: widget.isAdd,
        width: AddEditDialog.WIDE_WIDTH,
        height: AddEditDialog.WIDE_HEIGHT,
        onSave: _onSave,
        content: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 8.0),
              child: _TabBar(widget.tabs, _currentTab.add, _errors.stream)),
          Expanded(
              child: StreamBuilder<int>(
                  initialData: 0,
                  stream: _currentTab.stream,
                  builder: (context, snapshot) {
                    return Stack(
                      children: List<Widget>.generate(widget.tabs.length, (index) {
                        return Offstage(
                            offstage: snapshot.data != index,
                            child: Form(key: _formKeys[index], child: widget.tabs[index].widget));
                      }),
                    );
                  }))
        ]));
  }

  void _onSave() {
    final List<int> _errorsIndexes = [];
    for (int i = 0; i < widget.tabs.length; i++) {
      if (!_formKeys[i].currentState!.validate()) {
        _errorsIndexes.add(i);
      }
    }
    _errors.add(_errorsIndexes);
    widget.onSave();
  }
}

class _TabBar extends StatelessWidget {
  const _TabBar(this.tabs, this.onTab, this.errors, {Key? key}) : super(key: key);

  final List<TabWidget> tabs;
  final void Function(int index) onTab;
  final Stream<List<int>> errors;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: tabs.length,
        child: TabBar(
            labelPadding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
            onTap: onTab,
            labelColor: Colors.black,
            isScrollable: true,
            tabs: List<Widget>.generate(tabs.length, (index) {
              return StreamBuilder<List<int>>(
                  initialData: const [],
                  stream: errors,
                  builder: (_, snapshot) => _Tab(tabs[index].name, snapshot.data!.contains(index)));
            })));
  }
}

class _Tab extends StatelessWidget {
  const _Tab(this.name, this.hasError, {Key? key}) : super(key: key);

  final String name;
  final bool hasError;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 48,
        child: Center(
            child: Container(
                decoration: hasError
                    ? BoxDecoration(
                        borderRadius: const BorderRadius.all(Radius.circular(4)),
                        color: Colors.redAccent.withOpacity(0.2))
                    : null,
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Tab(text: name),
                ))));
  }
}
