import 'package:fastocloud_dart_models/models.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:flutter/material.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/base_page.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/istream_page.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/audio.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/input.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/other.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/video.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/vod.dart';

abstract class HardwareStreamPage<S extends HardwareStream> extends IStreamPage<S> {
  HardwareStreamPage.add(LiveServer server, IStreamEditor editor, StreamType type, WsMode? mode,
      {bool isSerial = false})
      : super.add(server, editor, type, mode, isSerial: isSerial);

  HardwareStreamPage.edit(
      LiveServer server, IStreamEditor editor, HardwareStream stream, WsMode? mode,
      {bool isSerial = false})
      : super.edit(server, editor, stream as S, mode, isSerial: isSerial);
}

abstract class _HardwareStreamPageState<T extends HardwareStreamPage, S extends HardwareStream>
    extends IStreamPageState<T, S> {
  String? scanFolderOid;

  @override
  List<TabWidget> tabs() {
    return [
      TabWidget('Base', base()),
      //  TabWidget('Meta urls', metaUrls()),
      TabWidget('Input urls', inputUrls()),
      TabWidget('Output urls', outputUrls()),
      TabWidget('Audio', audio()),
      TabWidget('Video', video()),
      TabWidget('Other', other())
    ];
  }

  Widget inputUrls() => InputUrlSection<S>(stream, widget.server, probeUrl);

  Widget audio() => AudioSection<S>(stream);

  Widget video() => VideoSection<S>(stream);

  Widget other() => OtherSection<S>(stream);

  @override
  bool validate() {
    return stream.isValid();
  }
}

// relay
abstract class RelayStreamPageBase<S extends RelayStream> extends HardwareStreamPage<S> {
  RelayStreamPageBase.add(LiveServer server, IStreamEditor editor, StreamType type, WsMode? mode,
      {bool isSerial = false})
      : super.add(server, editor, type, mode, isSerial: isSerial);

  RelayStreamPageBase.edit(LiveServer server, IStreamEditor editor, S stream, WsMode? mode,
      {bool isSerial = false})
      : super.edit(server, editor, stream, mode, isSerial: isSerial);
}

abstract class _RelayStreamPageBaseState<T extends RelayStreamPageBase, S extends RelayStream>
    extends _HardwareStreamPageState<T, S> {
  @override
  Widget audio() => AudioSectionRelay<S>(stream);

  @override
  Widget video() => VideoSectionRelay<S>(stream);

  @override
  Widget other() => OtherSectionRelay<S>(stream);
}

class RelayStreamPage extends RelayStreamPageBase<RelayStream> {
  RelayStreamPage.add(LiveServer server, IStreamEditor editor, WsMode? mode)
      : super.add(server, editor, StreamType.RELAY, mode);

  RelayStreamPage.edit(LiveServer server, IStreamEditor editor, RelayStream stream, WsMode? mode)
      : super.edit(server, editor, stream, mode);

  @override
  _RelayStreamPageState createState() => _RelayStreamPageState();
}

class _RelayStreamPageState extends _RelayStreamPageBaseState<RelayStreamPage, RelayStream> {}

abstract class EncodeStreamPageBase<S extends EncodeStream> extends HardwareStreamPage<S> {
  EncodeStreamPageBase.add(LiveServer server, IStreamEditor editor, StreamType type, WsMode? mode,
      {bool isSerial = false})
      : super.add(server, editor, type, mode, isSerial: isSerial);

  EncodeStreamPageBase.edit(
      LiveServer server, IStreamEditor editor, EncodeStream stream, WsMode? mode,
      {bool isSerial = false})
      : super.edit(server, editor, stream, mode, isSerial: isSerial);
}

abstract class _EncodeStreamPageBaseState<T extends EncodeStreamPageBase, S extends EncodeStream>
    extends _HardwareStreamPageState<T, S> {
  @override
  Widget audio() => AudioSectionEncode<S>(stream);

  @override
  Widget video() => VideoSectionEncode<S>(stream);

  @override
  Widget other() => OtherSectionEncode<S>(stream);
}

// encode
class EncodeStreamPage extends EncodeStreamPageBase<EncodeStream> {
  EncodeStreamPage.add(LiveServer server, IStreamEditor editor, WsMode? mode)
      : super.add(server, editor, StreamType.ENCODE, mode);

  EncodeStreamPage.edit(LiveServer server, IStreamEditor editor, EncodeStream stream, WsMode? mode)
      : super.edit(server, editor, stream, mode);

  @override
  _EncodeStreamPageState createState() {
    return _EncodeStreamPageState();
  }
}

class _EncodeStreamPageState extends _EncodeStreamPageBaseState<EncodeStreamPage, EncodeStream> {}

// timeshift recorder
abstract class TimeshiftRecorderStreamPageBase<S extends TimeshiftRecorderStream>
    extends RelayStreamPageBase<S> {
  TimeshiftRecorderStreamPageBase.add(
      LiveServer server, IStreamEditor editor, StreamType type, WsMode? mode)
      : super.add(server, editor, type, mode);

  TimeshiftRecorderStreamPageBase.edit(
      LiveServer server, IStreamEditor editor, S stream, WsMode? mode)
      : super.edit(server, editor, stream, mode);
}

abstract class _TimeshiftRecorderStreamPageBaseState<T extends TimeshiftRecorderStreamPageBase,
    S extends TimeshiftRecorderStream> extends _RelayStreamPageBaseState<T, S> {}

class TimeshiftRecorderStreamPage extends TimeshiftRecorderStreamPageBase<TimeshiftRecorderStream> {
  TimeshiftRecorderStreamPage.add(LiveServer server, IStreamEditor editor, WsMode? mode)
      : super.add(server, editor, StreamType.TIMESHIFT_RECORDER, mode);

  TimeshiftRecorderStreamPage.edit(
      LiveServer server, IStreamEditor editor, TimeshiftRecorderStream stream, WsMode? mode)
      : super.edit(server, editor, stream, mode);

  @override
  _TimeshiftRecorderStreamPageState createState() {
    return _TimeshiftRecorderStreamPageState();
  }
}

class _TimeshiftRecorderStreamPageState extends _TimeshiftRecorderStreamPageBaseState<
    TimeshiftRecorderStreamPage, TimeshiftRecorderStream> {
  @override
  List<TabWidget> tabs() {
    return [
      TabWidget('Base', base()),
      //TabWidget('Meta urls', metaUrls()),
      TabWidget('Input urls', inputUrls()),
      TabWidget('Audio', audio()),
      TabWidget('Video', video()),
      TabWidget('Other', other())
    ];
  }
}

// catchup
class CatchupStreamPage extends TimeshiftRecorderStreamPageBase<CatchupStream> {
  CatchupStreamPage.add(LiveServer server, IStreamEditor editor, WsMode? mode)
      : super.add(server, editor, StreamType.CATCHUP, mode);

  CatchupStreamPage.edit(
      LiveServer server, IStreamEditor editor, CatchupStream stream, WsMode? mode)
      : super.edit(server, editor, stream, mode);

  @override
  _CatchupStreamPageState createState() {
    return _CatchupStreamPageState();
  }
}

class _CatchupStreamPageState
    extends _TimeshiftRecorderStreamPageBaseState<CatchupStreamPage, CatchupStream> {}

// timeshift player
class TimeshiftPlayerStreamPage extends RelayStreamPageBase<TimeshiftPlayerStream> {
  TimeshiftPlayerStreamPage.add(LiveServer server, IStreamEditor editor, WsMode? mode)
      : super.add(server, editor, StreamType.TIMESHIFT_PLAYER, mode);

  TimeshiftPlayerStreamPage.edit(
      LiveServer server, IStreamEditor editor, TimeshiftPlayerStream stream, WsMode? mode)
      : super.edit(server, editor, stream, mode);

  @override
  _TimeshiftPlayerStreamPageState createState() {
    return _TimeshiftPlayerStreamPageState();
  }
}

class _TimeshiftPlayerStreamPageState
    extends _RelayStreamPageBaseState<TimeshiftPlayerStreamPage, TimeshiftPlayerStream> {}

// test
class TestLifeStreamPage extends RelayStreamPageBase<TestLifeStream> {
  TestLifeStreamPage.add(LiveServer server, IStreamEditor editor, WsMode? mode)
      : super.add(server, editor, StreamType.TEST_LIFE, mode);

  TestLifeStreamPage.edit(
      LiveServer server, IStreamEditor editor, TestLifeStream stream, WsMode? mode)
      : super.edit(server, editor, stream, mode);

  @override
  _TestLifeStreamPageState createState() {
    return _TestLifeStreamPageState();
  }
}

class _TestLifeStreamPageState
    extends _RelayStreamPageBaseState<TestLifeStreamPage, TestLifeStream> {
  @override
  List<TabWidget> tabs() {
    return [
      TabWidget('Base', base()),
      //  TabWidget('Meta urls', metaUrls()),
      TabWidget('Input urls', inputUrls()),
      TabWidget('Audio', audio()),
      TabWidget('Video', video()),
      TabWidget('Other', other())
    ];
  }
}

// cvdata
class CvDataStreamPage extends EncodeStreamPageBase<CvDataStream> {
  CvDataStreamPage.add(LiveServer server, IStreamEditor editor, WsMode? mode)
      : super.add(server, editor, StreamType.CV_DATA, mode);

  CvDataStreamPage.edit(LiveServer server, IStreamEditor editor, CvDataStream stream, WsMode? mode)
      : super.edit(server, editor, stream, mode);

  @override
  _CvDataStreamPageState createState() {
    return _CvDataStreamPageState();
  }
}

class _CvDataStreamPageState extends _EncodeStreamPageBaseState<CvDataStreamPage, CvDataStream> {
  @override
  List<TabWidget> tabs() {
    return [
      TabWidget('Base', base()),
      // TabWidget('Meta urls', metaUrls()),
      TabWidget('Input urls', inputUrls()),
      TabWidget('Audio', audio()),
      TabWidget('Video', video()),
      TabWidget('Other', other())
    ];
  }
}

// changer
class ChangerRelayStreamPage extends RelayStreamPageBase<ChangerRelayStream> {
  ChangerRelayStreamPage.add(LiveServer server, IStreamEditor editor, WsMode? mode)
      : super.add(server, editor, StreamType.CHANGER_RELAY, mode);

  ChangerRelayStreamPage.edit(
      LiveServer server, IStreamEditor editor, ChangerRelayStream stream, WsMode? mode)
      : super.edit(server, editor, stream, mode);

  @override
  _ChangerRelayStreamPageState createState() {
    return _ChangerRelayStreamPageState();
  }
}

class _ChangerRelayStreamPageState
    extends _RelayStreamPageBaseState<ChangerRelayStreamPage, ChangerRelayStream> {}

class ChangerEncoderStreamPage extends EncodeStreamPageBase<ChangerEncodeStream> {
  ChangerEncoderStreamPage.add(LiveServer server, IStreamEditor editor, WsMode? mode)
      : super.add(server, editor, StreamType.CHANGER_ENCODE, mode);

  ChangerEncoderStreamPage.edit(
      LiveServer server, IStreamEditor editor, ChangerEncodeStream stream, WsMode? mode)
      : super.edit(server, editor, stream, mode);

  @override
  _ChangerEncoderStreamPageState createState() {
    return _ChangerEncoderStreamPageState();
  }
}

class _ChangerEncoderStreamPageState
    extends _EncodeStreamPageBaseState<ChangerEncoderStreamPage, ChangerEncodeStream> {}

// vod relay
//? only adds vods fields
class VodRelayStreamPage extends RelayStreamPageBase<VodRelayStream> {
  VodRelayStreamPage.add(LiveServer server, IStreamEditor editor, WsMode? mode, bool isSerial)
      : super.add(server, editor, StreamType.VOD_RELAY, mode, isSerial: isSerial);

  VodRelayStreamPage.edit(
      LiveServer server, IStreamEditor editor, VodRelayStream stream, WsMode? mode, bool isSerial)
      : super.edit(server, editor, stream, mode, isSerial: isSerial);

  @override
  _VodRelayStreamPage createState() {
    return _VodRelayStreamPage();
  }
}

class _VodRelayStreamPage extends _RelayStreamPageBaseState<VodRelayStreamPage, VodRelayStream> {
  @override
  List<TabWidget> tabs() {
    final def = widget.editor.defaults();
    return [
      TabWidget('Base', base()),
      //  TabWidget('Meta urls', metaUrls()),
      TabWidget('Input urls', inputUrls()),
      TabWidget('Output urls', outputUrls()),
      TabWidget(
          widget.isEpisode ? 'Episode' : 'VOD',
          VodSection<VodRelayStream>(stream, setBaseFields, def.backgroundUrl, def.streamLogoIcon,
              isSerial: widget.isEpisode)),
      TabWidget('Audio', audio()),
      TabWidget('Video', video()),
      TabWidget('Other', other())
    ];
  }
}

// vod encode
//? only adds vods fields
class VodEncodeStreamPage extends EncodeStreamPageBase<VodEncodeStream> {
  VodEncodeStreamPage.add(LiveServer server, IStreamEditor editor, WsMode? mode, bool isSerial)
      : super.add(server, editor, StreamType.VOD_ENCODE, mode, isSerial: isSerial);

  VodEncodeStreamPage.edit(
      LiveServer server, IStreamEditor editor, VodEncodeStream stream, WsMode? mode, bool isSerial)
      : super.edit(server, editor, stream, mode, isSerial: isSerial);

  @override
  _VodEncodeStreamPageState createState() {
    return _VodEncodeStreamPageState();
  }
}

class _VodEncodeStreamPageState
    extends _EncodeStreamPageBaseState<VodEncodeStreamPage, VodEncodeStream> {
  @override
  List<TabWidget> tabs() {
    final def = widget.editor.defaults();
    return [
      TabWidget('Base', base()),
      // TabWidget('Meta urls', metaUrls()),
      TabWidget('Input urls', inputUrls()),
      TabWidget('Output urls', outputUrls()),
      TabWidget(
          widget.isEpisode ? 'Episode' : 'VOD',
          VodSection<VodEncodeStream>(
            stream,
            setBaseFields,
            def.backgroundUrl,
            def.streamLogoIcon,
            isSerial: widget.isEpisode,
          )),
      TabWidget('Audio', audio()),
      TabWidget('Video', video()),
      TabWidget('Other', other())
    ];
  }
}

// cod relay
class CodRelayStreamPage extends RelayStreamPageBase<CodRelayStream> {
  CodRelayStreamPage.add(LiveServer server, IStreamEditor editor, WsMode? mode)
      : super.add(server, editor, StreamType.COD_RELAY, mode);

  CodRelayStreamPage.edit(
      LiveServer server, IStreamEditor editor, CodRelayStream stream, WsMode? mode)
      : super.edit(server, editor, stream, mode);

  @override
  _CodRelayStreamPageState createState() {
    return _CodRelayStreamPageState();
  }
}

class _CodRelayStreamPageState
    extends _RelayStreamPageBaseState<CodRelayStreamPage, CodRelayStream> {}

// cod encode
class CodEncodeStreamPage extends EncodeStreamPageBase<CodEncodeStream> {
  CodEncodeStreamPage.add(LiveServer server, IStreamEditor editor, WsMode? mode)
      : super.add(server, editor, StreamType.COD_ENCODE, mode);

  CodEncodeStreamPage.edit(
      LiveServer server, IStreamEditor editor, CodEncodeStream stream, WsMode? mode)
      : super.edit(server, editor, stream, mode);

  @override
  _CodEncodeStreamPageState createState() {
    return _CodEncodeStreamPageState();
  }
}

class _CodEncodeStreamPageState
    extends _EncodeStreamPageBaseState<CodEncodeStreamPage, CodEncodeStream> {}

// event stream
class EventStreamPage extends EncodeStreamPageBase<EventStream> {
  EventStreamPage.add(LiveServer server, IStreamEditor editor, WsMode? mode)
      : super.add(server, editor, StreamType.EVENT, mode);

  EventStreamPage.edit(LiveServer server, IStreamEditor editor, EventStream stream, WsMode? mode)
      : super.edit(server, editor, stream, mode);

  @override
  _EventStreamPageState createState() {
    return _EventStreamPageState();
  }
}

class _EventStreamPageState extends _EncodeStreamPageBaseState<EventStreamPage, EventStream> {}
