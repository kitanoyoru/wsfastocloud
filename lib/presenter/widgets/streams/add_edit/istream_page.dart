import 'package:fastocloud_dart_models/models.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:flutter/material.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/base_page.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/base.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/meta.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/output.dart';

abstract class IStreamPage<S extends IStream> extends BaseStreamPage<S> {
  IStreamPage.add(LiveServer server, IStreamEditor editor, StreamType type, WsMode? mode,
      {bool isSerial = false})
      : super.add(server, editor, type, mode, isEpisode: isSerial);

  IStreamPage.edit(LiveServer server, IStreamEditor editor, S stream, WsMode? mode,
      {bool isSerial = false})
      : super.edit(server, editor, stream, mode, isEpisode: isSerial);
}

abstract class IStreamPageState<T extends IStreamPage, S extends IStream>
    extends BaseStreamPageState<T, S> {
  final GlobalKey<BaseSectionState> _baseKey = GlobalKey<BaseSectionState>();

  @override
  List<TabWidget> tabs() {
    return [
      TabWidget('Base', base()),
      //TabWidget('Meta urls', metaUrls()),
      TabWidget('Output urls', outputUrls())
    ];
  }

  Widget base() => BaseSection<S>(stream, isAdd, key: _baseKey, mode: widget.mode);

  Widget metaUrls() => MetaUrlSection<S>(stream);

  Widget outputUrls() => OutputUrlSection<S>(stream, widget.server, probeRawUrl);

  void probeRawUrl(OutputUrl url) {
    final resp = widget.editor.probeOutUrl(url);
    resp.then((value) {
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
                title: const Text('Probe Stream'), content: Text(value, softWrap: true));
          });
    }, onError: (error) {
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(title: const Text('Error'), content: Text(error, softWrap: true));
          });
    });
  }

  void probeUrl(InputUrl url) {
    final resp = widget.editor.probeInUrl(url);
    resp.then((value) {
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
                title: const Text('Probe Stream'), content: Text(value, softWrap: true));
          });
    }, onError: (error) {
      showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(title: const Text('Error'), content: Text(error, softWrap: true));
          });
    });
  }

  void setBaseFields(String name, String icon, String description) {
    _baseKey.currentState!.setFields(name: name, icon: icon, description: description);
  }
}
