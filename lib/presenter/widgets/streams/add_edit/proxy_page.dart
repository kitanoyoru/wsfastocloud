import 'package:fastocloud_dart_models/models.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/base_page.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/istream_page.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/vod.dart';

class ProxyStreamPage extends IStreamPage<ProxyStream> {
  ProxyStreamPage.add(LiveServer server, IStreamEditor editor, WsMode? mode)
      : super.add(server, editor, StreamType.PROXY, mode);

  ProxyStreamPage.edit(LiveServer server, IStreamEditor editor, ProxyStream stream, WsMode? mode)
      : super.edit(server, editor, stream, mode);

  @override
  _ProxyStreamPageState createState() {
    return _ProxyStreamPageState();
  }
}

class _ProxyStreamPageState extends IStreamPageState<ProxyStreamPage, ProxyStream> {
  @override
  bool validate() {
    return stream.isValid();
  }
}

class VodProxyStreamPage extends IStreamPage<VodProxyStream> {
  VodProxyStreamPage.add(LiveServer server, IStreamEditor editor, WsMode? mode, bool isSerial)
      : super.add(server, editor, StreamType.VOD_PROXY, mode, isSerial: isSerial);

  VodProxyStreamPage.edit(
      LiveServer server, IStreamEditor editor, VodProxyStream stream, WsMode? mode, bool isSerial)
      : super.edit(server, editor, stream, mode, isSerial: isSerial);

  @override
  _VodProxyStreamPageState createState() {
    return _VodProxyStreamPageState();
  }
}

class _VodProxyStreamPageState extends IStreamPageState<VodProxyStreamPage, VodProxyStream> {
  @override
  List<TabWidget> tabs() {
    final def = widget.editor.defaults();
    return [
      ...super.tabs(),
      TabWidget(
          widget.isEpisode ? 'Episode' : 'VOD',
          VodSection<VodProxyStream>(
            stream,
            setBaseFields,
            def.backgroundUrl,
            def.streamLogoIcon,
            isSerial: widget.isEpisode,
          ))
    ];
  }

  @override
  bool validate() {
    return stream.isValid();
  }
}
