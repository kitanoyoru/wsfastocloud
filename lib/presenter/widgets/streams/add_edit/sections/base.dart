import 'dart:async';

import 'package:fastocloud_dart_models/models.dart';
import 'package:fastotv_dart/commands_info/types.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/widgets.dart';
import 'package:rxdart/subjects.dart';
import 'package:wsfastocloud/presenter/widgets/scrollbar.dart';
import 'package:wsfastocloud/presenter/widgets/streams/stream_icon.dart';

class BaseSection<S extends IStream> extends StatefulWidget {
  const BaseSection(this.stream, this.isAdd, {Key? key, required this.mode}) : super(key: key);

  final S stream;
  final WsMode? mode;
  final bool isAdd;

  @override
  State<BaseSection<S>> createState() => BaseSectionState<S>();
}

class BaseSectionState<S extends IStream> extends State<BaseSection<S>> {
  bool get hasEpg => (widget.stream.epgId ?? '').isNotEmpty;
  final BehaviorSubject<bool> _hasEpg = BehaviorSubject<bool>();

  final StreamController<String> _iconsUpdates = StreamController<String>();

  @override
  void dispose() {
    _iconsUpdates.close();
    _hasEpg.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ScrollbarEx(builder: (controller) {
      return ListView(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          controller: controller,
          children: [
            if (!widget.isAdd) TextFieldEx.readOnly(hint: 'ID', init: widget.stream.id!),
            nameField(),
            iconField(),
            descriptionField(),
            if (!widget.stream.isVod() && widget.mode == WsMode.IPTV) epgField(),
            _Groups<S>(widget.stream),
            priceField(),
            visibleField(),
            iarcField()
          ]);
    });
  }

  Widget epgField() {
    return Row(
      children: [
        Expanded(
          child: TextFieldEx(
              key: const ValueKey<String>('Epg field'),
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              maxSymbols: EpgName.MAX_LENGTH,
              hintText: 'Epg ID',
              init: widget.stream.epgId,
              onFieldChanged: (term) {
                if (hasEpg != term.isNotEmpty) {
                  _hasEpg.add(term.isNotEmpty);
                }

                widget.stream.epgId = term;
              }),
        ),
        SizedBox(width: 144, child: archive())
      ],
    );
  }

  Widget priceField() {
    return NumberTextField.decimal(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        minDouble: Price.MIN,
        maxDouble: Price.MAX,
        hintText: 'Price (\$)',
        canBeEmpty: false,
        initDouble: widget.stream.price,
        onFieldChangedDouble: (term) {
          if (term != null) widget.stream.price = term;
        });
  }

  Widget archive() {
    return StreamBuilder(
        initialData: hasEpg,
        stream: _hasEpg.stream,
        builder: (context, snapshot) {
          if (hasEpg) {
            return StateCheckBox(
                title: 'Archive',
                init: widget.stream.archive,
                onChanged: (value) {
                  widget.stream.archive = value;
                });
          }
          return IgnorePointer(
              ignoring: !hasEpg,
              child:
                  Opacity(opacity: hasEpg ? 1 : 0.5, child: const StateCheckBox(title: 'Archive')));
        });
  }

  TextFieldEx nameField() {
    return TextFieldEx(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        maxSymbols: StreamName.MAX_LENGTH,
        hintText: 'Name',
        errorText: 'Enter name',
        init: widget.stream.name,
        onFieldChanged: (term) {
          widget.stream.name = term;
        });
  }

  TextFieldEx descriptionField() {
    return TextFieldEx(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        maxSymbols: StreamDescription.MAX_LENGTH,
        hintText: 'Description',
        init: widget.stream.description,
        onFieldChanged: (term) {
          widget.stream.description = term;
        });
  }

  TextFieldEx iconField() {
    return TextFieldEx(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        minSymbols: IconUrl.MIN_LENGTH,
        maxSymbols: IconUrl.MAX_LENGTH,
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'Icon',
        errorText: 'Enter icon url',
        init: widget.stream.icon,
        decoration: InputDecoration(
            icon: StreamBuilder<String>(
                initialData: widget.stream.icon,
                stream: _iconsUpdates.stream,
                builder: (_, snapshot) {
                  return PreviewIcon.live(snapshot.data!, width: 40, height: 40);
                })),
        onFieldChanged: (term) {
          widget.stream.icon = term;
          _iconsUpdates.add(term);
        });
  }

  Widget iarcField() {
    return NumberTextField.integer(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        minInt: IARC.MIN,
        maxInt: IARC.MAX,
        hintText: 'IARC',
        canBeEmpty: false,
        initInt: widget.stream.iarc,
        onFieldChangedInt: (term) {
          if (term != null) widget.stream.iarc = term;
        });
  }

  Widget visibleField() {
    return StateCheckBox(
        title: 'Visible for subscribers',
        init: widget.stream.visible,
        onChanged: (value) {
          widget.stream.visible = value;
        });
  }

  void setFields({String? name, String? icon, String? description}) {
    if (name != null) {
      widget.stream.name = name;
    }
    if (icon != null) {
      widget.stream.icon = icon;
      _iconsUpdates.add(icon);
    }
    if (description != null) {
      widget.stream.description = description;
    }
    setState(() {});
  }
}

class _Groups<S extends IStream> extends StatefulWidget {
  const _Groups(this.stream, {Key? key}) : super(key: key);

  final S stream;

  @override
  _GroupsState createState() => _GroupsState();
}

class _GroupsState extends State<_Groups> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: ChipListField(
        hintText: 'Groups',
        values: widget.stream.groups,
        onItemAdded: (item) => setState(() => widget.stream.groups.add(item)),
        onItemRemoved: (index) => setState(() => widget.stream.groups.removeAt(index)),
      ),
    );
  }
}
