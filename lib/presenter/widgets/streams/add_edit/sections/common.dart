import 'package:flutter/material.dart';

class IOFieldBorder extends StatelessWidget {
  final Widget child;
  final VoidCallback? onDelete;

  const IOFieldBorder({required this.child, this.onDelete});

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Expanded(
          child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8.0),
              child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Theme.of(context).brightness == Brightness.light
                              ? Colors.black38
                              : Colors.white38),
                      borderRadius: const BorderRadius.all(Radius.circular(8.0))),
                  child: child))),
      if (onDelete != null)
        IconButton(tooltip: 'Remove', icon: const Icon(Icons.close), onPressed: onDelete)
    ]);
  }
}

class IOTypeRadioTile extends StatelessWidget {
  final String title;
  final bool selected;
  final void Function() onChanged;

  const IOTypeRadioTile({required this.title, required this.selected, required this.onChanged});

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          onChanged();
        },
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(children: [
              Icon(selected ? Icons.radio_button_on : Icons.radio_button_off,
                  color: selected ? Theme.of(context).colorScheme.secondary : null),
              Text(title)
            ])));
  }
}
