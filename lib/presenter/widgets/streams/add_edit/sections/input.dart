import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:wsfastocloud/presenter/widgets/scrollbar.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/common.dart';
import 'package:wsfastocloud/presenter/widgets/urls/input.dart';

class InputUrlSection<S extends HardwareStream> extends StatefulWidget {
  final S stream;
  final LiveServer server;
  final void Function(InputUrl) probe;

  const InputUrlSection(this.stream, this.server, this.probe);

  @override
  _InputUrlSectionState<S> createState() {
    return _InputUrlSectionState<S>();
  }
}

class _InputUrlSectionState<S extends HardwareStream> extends State<InputUrlSection<S>> {
  final GlobalKey<ScrollbarExState> _scrollKey = GlobalKey<ScrollbarExState>();

  S get stream => widget.stream;

  @override
  Widget build(BuildContext context) {
    switch (stream.type()) {
      case StreamType.RELAY:
      case StreamType.ENCODE:
      case StreamType.COD_RELAY:
      case StreamType.COD_ENCODE:
      case StreamType.CHANGER_RELAY:
      case StreamType.CHANGER_ENCODE:
      case StreamType.VOD_RELAY:
      case StreamType.VOD_ENCODE:
        return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: TextButton.icon(
                  onPressed: _addInput,
                  icon: const Icon(Icons.add),
                  label: const Text('Add input URL'))),
          Expanded(
              child: ScrollbarEx(
                  key: _scrollKey,
                  builder: (controller) {
                    return ListView.builder(
                        padding: const EdgeInsets.symmetric(horizontal: 24),
                        controller: controller,
                        itemCount: stream.input.length,
                        itemBuilder: (context, index) {
                          return IOFieldBorder(
                              onDelete: stream.input.length == 1
                                  ? null
                                  : () {
                                      _deleteInputUrl(index);
                                    },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: _createTile(index),
                              ));
                        });
                  }))
        ]);
      default:
        return ScrollbarEx(
            key: _scrollKey,
            builder: (controller) {
              return ListView(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  controller: controller,
                  children: [_createTile(0)]);
            });
    }
  }

  Widget _createTile(int index) {
    final InputUrl url = stream.input[index];
    final bool isHttp = url is HttpInputUrl;
    final bool isUdp = url is UdpInputUrl;
    final bool isFile = url is FileInputUrl;
    final bool isSrt = url is SrtInputUrl;
    final bool isRtmp = url is RtmpInputUrl;
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(children: [
            IOTypeRadioTile(
                title: 'HTTP/HTTPS/HLS',
                selected: isHttp,
                onChanged: () {
                  _setUrlType(index, HttpInputUrl(id: index, uri: ''));
                }),
            IOTypeRadioTile(
                title: 'RTMP',
                selected: isRtmp,
                onChanged: () {
                  _setUrlType(
                      index, RtmpInputUrl(id: index, uri: '', rtmpSrcType: RtmpSrcType.RTMPSRC));
                }),
            IOTypeRadioTile(
                title: 'UDP',
                selected: isUdp,
                onChanged: () {
                  _setUrlType(index, UdpInputUrl(id: index, uri: ''));
                }),
            IOTypeRadioTile(
                title: 'File',
                selected: isFile,
                onChanged: () {
                  _setUrlType(index, FileInputUrl(id: index, uri: ''));
                }),
            IOTypeRadioTile(
                title: 'SRT',
                selected: isSrt,
                onChanged: () {
                  _setUrlType(index, SrtInputUrl(id: index, uri: '', mode: SrtMode.CALLER));
                }),
            IOTypeRadioTile(
                title: 'Other',
                selected: !(isHttp || isUdp || isFile || isSrt),
                onChanged: () {
                  _setUrlType(index, InputUrl.createInvalid(id: index));
                })
          ])),
      _createInputField(index)
    ]);
  }

  Widget _createInputField(int index) {
    final InputUrl url = stream.input[index];
    final probeCB = widget.server.isPro() ? widget.probe : null;
    final isEncode = stream is EncodeStream; // TODO(?) Use stream.type()

    if (url is HttpInputUrl) {
      return HttpInputUrlField(url, probeCB, isEncode);
    } else if (url is RtmpInputUrl) {
      return RtmpInputUrlField(url, probeCB);
    } else if (url is UdpInputUrl) {
      return UdpInputUrlField(url, probeCB);
    } else if (url is FileInputUrl) {
      return FileInputUrlField(url, probeCB, widget.server.isPro());
    } else if (url is SrtInputUrl) {
      return SrtInputUrlField(url, probeCB);
    }
    return InputUrlField(url, probeCB);
  }

  void _addInput() {
    setState(() {
      final int index = stream.input.length;
      stream.input.add(InputUrl.createInvalid(id: index));
    });
    _scrollKey.currentState!.refresh();
  }

  void _deleteInputUrl(int index) {
    setState(() {
      stream.input.removeAt(index);
    });
    _scrollKey.currentState!.refresh();
  }

  void _setUrlType(int index, InputUrl url) {
    setState(() {
      stream.input[index] = url;
    });
  }
}
