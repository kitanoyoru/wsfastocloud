import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/presenter/widgets/base.dart';
import 'package:wsfastocloud/presenter/widgets/optional.dart';
import 'package:wsfastocloud/presenter/widgets/scrollbar.dart';

class OtherSection<S extends HardwareStream> extends StatelessWidget {
  final S stream;

  const OtherSection(this.stream);

  @override
  Widget build(BuildContext context) {
    return ScrollbarEx(builder: (controller) {
      return ListView(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          controller: controller,
          children: fields());
    });
  }

  List<Widget> fields() {
    return [
      _loopField(),
      _selectedInputField(),
      _autoExitField(),
      _logLevelField(),
      TextFieldEx.readOnly(hint: 'Feedback directory', init: stream.feedbackDirectory ?? '~'),
      _restartAttemptsField(),
      _notification()
    ];
  }

  Widget _loopField() {
    return StateCheckBox(
        title: 'Loop',
        init: stream.loop,
        onChanged: (value) {
          stream.loop = value;
        });
  }

  Widget _selectedInputField() {
    return NumberTextField.integer(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        minInt: 0,
        hintText: 'Selected input',
        canBeEmpty: false,
        initInt: stream.selectedInput,
        onFieldChangedInt: (term) {
          if (term != null) stream.selectedInput = term;
        });
  }

  Widget _autoExitField() {
    return OptionalFieldTile(
        title: 'Change Stream Time Work',
        init: stream.autoExit != null,
        onChanged: (value) {
          stream.autoExit = value ? StreamTTL.createDefault(3600) : null;
        },
        builder: () {
          return StreamTTLField(stream.autoExit!);
        });
  }

  Widget _logLevelField() {
    return DropdownButtonEx<StreamLogLevel>(
        hint: 'Audio codes',
        value: stream.logLevel,
        values: const <StreamLogLevel>[
          StreamLogLevel.EMERG,
          StreamLogLevel.ALERT,
          StreamLogLevel.CRIT,
          StreamLogLevel.ERR,
          StreamLogLevel.WARNING,
          StreamLogLevel.NOTICE,
          StreamLogLevel.INFO,
          StreamLogLevel.DEBUG
        ],
        onChanged: (t) {
          stream.logLevel = t;
        },
        itemBuilder: (StreamLogLevel logLevel) {
          return DropdownMenuItem(child: Text(logLevel.toHumanReadable()), value: logLevel);
        });
  }

  Widget _restartAttemptsField() {
    return NumberTextField.integer(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        minInt: RestartAttempts.MIN,
        maxInt: RestartAttempts.MAX,
        hintText: 'Max restart attempts',
        canBeEmpty: false,
        initInt: stream.restartAttempts,
        onFieldChangedInt: (term) {
          if (term != null) stream.restartAttempts = term;
        });
  }

  Widget _notification() {
    final enabled = stream.notificationContacts != null;
    return OptionalFieldTile(
        title: 'Notification',
        init: enabled,
        onChanged: (value) {
          if (value) {
            stream.notificationContacts = [
              NotificationStreamContact(
                  email: 'test@fastocloud.com', type: NotificationStreamType.STREAM_FINISHED)
            ];
          } else {
            stream.notificationContacts = null;
          }
        },
        builder: () {
          return StreamNotificationField(stream.notificationContacts ?? []);
        });
  }
}

class OtherSectionRelay<S extends RelayStream> extends OtherSection<S> {
  const OtherSectionRelay(S stream) : super(stream);

  @override
  List<Widget> fields() {
    return super.fields() + [_AutoStart<S>(stream)];
  }
}

class OtherSectionEncode<S extends EncodeStream> extends OtherSection<S> {
  const OtherSectionEncode(S stream) : super(stream);

  @override
  List<Widget> fields() {
    return super.fields() + [_machineLearningField(), _AutoStart<S>(stream)];
  }

  Widget _machineLearningField() {
    return OptionalFieldTile(
        title: 'Machine Learning',
        init: stream.machineLearning != null,
        onChanged: (value) {
          stream.machineLearning = value ? MachineLearning.createYoloV3TinyExample() : null;
        },
        builder: () => MachineLearningField(stream.machineLearning!));
  }
}

class _AutoStart<S extends HardwareStream> extends StateCheckBox {
  _AutoStart(S stream)
      : super(
            title: 'Auto start',
            init: stream.autoStart,
            onChanged: (value) {
              stream.autoStart = value;
            });
}
