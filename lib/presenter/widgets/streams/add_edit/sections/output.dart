import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wsfastocloud/presenter/widgets/scrollbar.dart';
import 'package:wsfastocloud/presenter/widgets/streams/add_edit/sections/common.dart';
import 'package:wsfastocloud/presenter/widgets/urls/output.dart';

import '../../../../../domain/repositories/stream_db_repository.dart';

class OutputUrlSection<S extends IStream> extends StatefulWidget {
  final S stream;
  final LiveServer server;
  final void Function(OutputUrl) probe;

  const OutputUrlSection(
    this.stream,
    this.server,
    this.probe,
  );

  @override
  _OutputUrlSectionState<S> createState() {
    return _OutputUrlSectionState<S>();
  }
}

class _OutputUrlSectionState<S extends IStream> extends State<OutputUrlSection<S>> {
  final GlobalKey<ScrollbarExState> _scrollKey = GlobalKey<ScrollbarExState>();

  S get stream => widget.stream;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: TextButton.icon(
              icon: const Icon(Icons.add),
              label: const Text('Add output URL'),
              onPressed: _addOutput)),
      Expanded(
          child: ScrollbarEx(
              key: _scrollKey,
              builder: (controller) {
                return ListView.builder(
                    padding: const EdgeInsets.symmetric(horizontal: 24),
                    controller: controller,
                    itemCount: stream.output.length,
                    itemBuilder: (context, index) {
                      return IOFieldBorder(
                          onDelete: stream.output.length == 1 ? null : () => _deleteOutput(index),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: _createTile(index),
                          ));
                    });
              }))
    ]);
  }

  Widget _createTile(int index) {
    final OutputUrl url = stream.output[index];
    final bool isHttp = url is HttpOutputUrl;
    final bool isHLS = url is HttpOutputUrl && url.isHls();
    final bool isSrt = url is SrtOutputUrl;
    final bool isGs = url is GoogleOutputUrl;
    final bool isRtmp = url is RtmpOutputUrl;
    final bool isUDP = url is UdpOutputUrl;
    final bool isUnknown = url is UnknownOutputUrl;
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(children: [
            IOTypeRadioTile(
                title: 'HLS',
                selected: isHLS,
                onChanged: () {
                  HttpOutputUrl uri;
                  if (widget.stream.type() == StreamType.COD_ENCODE ||
                      widget.stream.type() == StreamType.COD_RELAY) {
                    uri = widget.server.genTemplateCodsHttpOutputUrl(index);
                  } else if (widget.stream.type() == StreamType.VOD_PROXY ||
                      widget.stream.type() == StreamType.VOD_RELAY ||
                      widget.stream.type() == StreamType.VOD_ENCODE) {
                    uri = widget.server.genTemplateVodsHttpOutputUrl(index);
                  } else {
                    uri = widget.server.genTemplateHLSHttpOutputUrl(index);
                  }
                  _setUrlType(index, uri);
                }),
            IOTypeRadioTile(
                title: 'HTTP',
                selected: isHttp && !isHLS,
                onChanged: () {
                  _setUrlType(
                      index,
                      HttpOutputUrl.createDefaultHTTP(
                          id: index, uri: 'http://0.0.0.0:8444/master'));
                }),
            IOTypeRadioTile(
                title: 'SRT',
                selected: isSrt,
                onChanged: () {
                  _setUrlType(index, SrtOutputUrl.createDefault(id: index));
                }),
            IOTypeRadioTile(
                title: 'UDP',
                selected: isUDP,
                onChanged: () {
                  _setUrlType(index, UdpOutputUrl.createDefault(id: index));
                }),
            IOTypeRadioTile(
                title: 'RTMP',
                selected: isRtmp,
                onChanged: () {
                  _setUrlType(index, RtmpOutputUrl.createDefault(id: index));
                }),
            IOTypeRadioTile(
                title: 'Google Storage',
                selected: isGs,
                onChanged: () {
                  _setUrlType(index, GoogleOutputUrl.createDefault(id: index));
                }),
            IOTypeRadioTile(
                title: 'Specific',
                selected: isUnknown,
                onChanged: () {
                  _setUrlType(index, UnknownOutputUrl.createWebRTC(id: index));
                }),
            IOTypeRadioTile(
                title: 'Other',
                selected: !(isHttp || isSrt || isGs || isUDP || isRtmp || isHLS || isUnknown),
                onChanged: () {
                  _setUrlType(index, OutputUrl.createInvalid(id: index));
                })
          ])),
      _createOutputField(index)
    ]);
  }

  Widget _createOutputField(int index) {
    final OutputUrl url = stream.output[index];
    final probeCB = widget.server.isPro() ? widget.probe : null;
    if (url is HttpOutputUrl) {
      final bool isProxy =
          stream.type() == StreamType.PROXY || stream.type() == StreamType.VOD_PROXY;
      if (isProxy) {
        if (!url.isHls()) {
          return HttpOutputUrlField(url, probeCB);
        }

        return HLSOutputUrlField(url, probeCB, null, proxyDirectory: widget.server.proxyDirectory,
            onPrepareOutputTemplate: (url) {
          return _convertProxyFileToHttp(url)!;
        }, isExternal: true);
      }

      if (!url.isHls()) {
        return HttpOutputUrlField(url, probeCB);
      }

      final getToken = (String host) {
        return context.read<StreamDBRepository>().getTokenUrl(host);
      };
      return GoHttpOutputUrlField(url, probeCB, getToken);
    } else if (url is SrtOutputUrl) {
      return SrtOutputUrlField(url, probeCB);
    } else if (url is GoogleOutputUrl) {
      return GoogleOutputUrlField(url, probeCB);
    } else if (url is UdpOutputUrl) {
      return UdpOutputUrlField(url, probeCB);
    } else if (url is RtmpOutputUrl) {
      return RtmpOutputUrlField(url, probeCB);
    } else if (url is UnknownOutputUrl) {
      return UnknownOutputUrlField(url, probeCB);
    }
    return OutputUrlField(url, probeCB);
  }

  String? _convertProxyFileToHttp(String path) {
    final url = widget.server.generateHttpProxyUrl(path);
    if (url == null) {
      return null;
    }
    return url.toString();
  }

  void _addOutput() {
    setState(() {
      final int index = stream.output.length;
      stream.output.add(OutputUrl.createInvalid(id: index));
    });
    _scrollKey.currentState!.refresh();
  }

  void _deleteOutput(int index) {
    setState(() {
      stream.output.removeAt(index);
    });
    _scrollKey.currentState!.refresh();
  }

  void _setUrlType(int index, OutputUrl url) {
    setState(() {
      stream.output[index] = url;
    });
  }
}
