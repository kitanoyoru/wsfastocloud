import 'package:fastocloud_dart_models/models.dart';
import 'package:fastocloud_dart_models/src/models/alpha_method.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/presenter/widgets/base.dart';
import 'package:wsfastocloud/presenter/widgets/color_picker.dart';
import 'package:wsfastocloud/presenter/widgets/optional.dart';
import 'package:wsfastocloud/presenter/widgets/scrollbar.dart';

class VideoSection<S extends HardwareStream> extends StatefulWidget {
  final S stream;

  const VideoSection(this.stream);

  @override
  _VideoSectionState<S> createState() => _VideoSectionState<S>();
}

class _VideoSectionState<S extends HardwareStream> extends State<VideoSection<S>> {
  S get stream => widget.stream;

  @override
  Widget build(BuildContext context) {
    return ScrollbarEx(builder: (controller) {
      return ListView(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          controller: controller,
          children: [
            StateCheckBox(
                title: 'Has video',
                init: stream.haveVideo,
                onChanged: (value) {
                  setState(() {
                    stream.haveVideo = value;
                  });
                }),
            if (stream is EncodeStream)
              StateCheckBox(
                  title: 'Relay video',
                  init: (stream as EncodeStream).relayVideo,
                  onChanged: (value) {
                    setState(() {
                      (stream as EncodeStream).relayVideo = value;
                    });
                  }),
            if (stream.haveVideo) ...fields()
          ]);
    });
  }

  List<Widget> fields() => [];
}

class VideoSectionRelay<S extends RelayStream> extends VideoSection<S> {
  const VideoSectionRelay(S stream) : super(stream);

  @override
  _VideoSectionRelayState<S> createState() => _VideoSectionRelayState<S>();
}

class _VideoSectionRelayState<S extends RelayStream> extends _VideoSectionState<S> {
  @override
  List<Widget> fields() {
    return super.fields() + [_videoParser()];
  }

  Widget _videoParser() {
    return OptionalFieldTile(
        title: 'Video parser',
        init: stream.videoParser != null,
        onChanged: (value) {
          stream.videoParser = value ? VideoParser.H264 : null;
        },
        builder: () {
          return DropdownButtonEx<VideoParser>(
              hint: 'Video parser',
              value: stream.videoParser!,
              values: VideoParser.values,
              onChanged: (t) {
                stream.videoParser = t;
              },
              itemBuilder: (VideoParser parser) {
                return DropdownMenuItem(child: Text(parser.toHumanReadable()), value: parser);
              });
        });
  }
}

class _StreamOverlayWidget extends StatefulWidget {
  final StreamOverlay overlay;

  const _StreamOverlayWidget(this.overlay);

  @override
  _StreamOverlayWidgetState createState() {
    return _StreamOverlayWidgetState();
  }
}

class _StreamOverlayWidgetState extends State<_StreamOverlayWidget> {
  StreamOverlay get value => widget.overlay;

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      TextFieldEx(
          minSymbols: IconUrl.MIN_LENGTH,
          maxSymbols: IconUrl.MAX_LENGTH,
          formatters: <TextInputFormatter>[TextFieldFilter.url],
          hintText: 'Overlay',
          errorText: 'Enter overlay url',
          init: value.url.url,
          onFieldChanged: (term) {
            value.url.url = term;
          }),
      _streamOverlayType(),
      _streamOverlaySize(),
      _streamOverlayBackground(),
      _streamOverlayMethod()
    ]);
  }

  Widget _streamOverlayType() {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      DropdownButtonEx<OverlayUrlType>(
          hint: value.url.type.toHumanReadable(),
          value: value.url.type,
          values: OverlayUrlType.values,
          onChanged: (c) {
            setState(() {
              switch (c) {
                case OverlayUrlType.URL:
                  widget.overlay.url = OverlayUrlUrl.createDefault();
                  break;
                case OverlayUrlType.WPE:
                  widget.overlay.url = OverlayUrlWpe.createDefault();
                  break;
                case OverlayUrlType.CEF:
                  widget.overlay.url = OverlayUrlCef.createDefault();
                  break;
              }
            });
          },
          itemBuilder: (OverlayUrlType value) {
            return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value);
          }),
      if (value.url.type == OverlayUrlType.URL) _url(),
      if (value.url.type == OverlayUrlType.WPE) _wpe(),
      if (value.url.type == OverlayUrlType.CEF) _cef()
    ]);
  }

  Widget _url() {
    return const SizedBox();
  }

  Widget _wpe() {
    final wpe = value.url as OverlayUrlWpe;
    return StateCheckBox(
        title: 'GL',
        init: wpe.wpe.gl,
        onChanged: (gl) {
          wpe.wpe.gl = gl;
        });
  }

  Widget _cef() {
    final cef = value.url as OverlayUrlCef;
    return StateCheckBox(
        title: 'GPU',
        init: cef.cef.gpu,
        onChanged: (gpu) {
          cef.cef.gpu = gpu;
        });
  }

  Widget _streamOverlaySize() {
    return OptionalFieldTile(
        title: 'Size',
        init: value.size != null,
        onChanged: (val) {
          value.size = val ? Size.create640x480() : null;
        },
        builder: () {
          return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8), child: SizeField(value.size!));
        });
  }

  Widget _streamOverlayBackground() {
    final values = ['Checker Pattern', 'Black', 'White', 'Transparent'];

    return OptionalFieldTile(
      title: 'Background',
      init: value.background != null,
      onChanged: (val) {
        value.background = val ? 0 : null;
      },
      builder: () {
        final selectedBackground = value.background;

        return Row(mainAxisSize: MainAxisSize.min, children: [
          for (var i = 0; i < values.length; i++) ...[
            const SizedBox(width: 8),
            _backgroudRadio(i, selectedBackground),
            Text(values[i]),
          ]
        ]);
      },
    );
  }

  Widget _backgroudRadio(int vl, int? groupValue) {
    return Radio<int>(
      value: vl,
      groupValue: groupValue,
      onChanged: (val) => setState(() => value.background = val),
    );
  }

  Widget _streamOverlayMethod() {
    return OptionalFieldTile(
      title: 'Method',
      init: value.method != null,
      onChanged: (val) => value.method = val ? StableAlphaMethod.createDefault() : null,
      builder: () => _AlphaMethod(
        method: value.method!,
        onChanged: (val) => value.method = val,
      ),
    );
  }
}

class VideoSectionEncode<S extends EncodeStream> extends VideoSection<S> {
  const VideoSectionEncode(S stream) : super(stream);

  @override
  _VideoSectionEncodeState<S> createState() => _VideoSectionEncodeState<S>();
}

class _VideoSectionEncodeState<S extends EncodeStream> extends _VideoSectionState<S> {
  @override
  List<Widget> fields() {
    final List<Widget> noRelayVideo = [
      ...super.fields(),
      _frameRateField(),
      _VideoBitrateWithConverter(stream: stream),
      _aspectRatioField(),
      _deinterlaceField(),
      _FlipVideo<S>(stream),
      _sizeField(),
      _logoField(),
      _rsvgLogoField(),
      _textOverlay(),
      _streamOverlay(),
      _backgroundEffects()
    ];

    return [
      _videoCodecField(),
      if (!stream.relayVideo) ...noRelayVideo,
    ];
  }

  Widget _deinterlaceField() {
    return StateCheckBox(
        title: 'Deinterlace',
        init: stream.deinterlace ?? false,
        onChanged: (value) {
          stream.deinterlace = value ? value : null;
        });
  }

  Widget _videoCodecField() {
    return DropdownButtonEx<VideoCodec>(
        hint: 'Video codec',
        value: stream.videoCodec,
        values: VideoCodec.values,
        onChanged: (t) {
          stream.videoCodec = t;
        },
        itemBuilder: (VideoCodec codec) {
          return DropdownMenuItem(child: Text(codec.toHumanReadable()), value: codec);
        });
  }

  Widget _frameRateField() {
    return OptionalFieldTile(
        title: 'Change Frame Rate',
        init: stream.frameRate != null,
        onChanged: (value) {
          stream.frameRate = value ? Rational.create25x1() : null;
        },
        builder: () {
          return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: AspectRatioField(stream.frameRate!));
        });
  }

  Widget _sizeField() {
    return OptionalFieldTile(
        title: 'Change resolution',
        init: stream.size != null,
        onChanged: (value) {
          stream.size = value ? Size.create640x480() : null;
        },
        builder: () {
          return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8), child: SizeField(stream.size!));
        });
  }

  Widget _logoField() {
    return OptionalFieldTile(
        title: 'Insert logo',
        init: stream.logo != null,
        onChanged: (value) {
          stream.logo = value ? Logo.createDefault() : null;
        },
        builder: () {
          return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8), child: LogoField(stream.logo!));
        });
  }

  Widget _rsvgLogoField() {
    return OptionalFieldTile(
        title: 'Insert RSVG Logo',
        init: stream.rsvgLogo != null,
        onChanged: (value) {
          stream.rsvgLogo = value ? RsvgLogo.createDefault() : null;
        },
        builder: () {
          return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: RsvgLogoField(stream.rsvgLogo!));
        });
  }

  Widget _aspectRatioField() {
    return OptionalFieldTile(
        title: 'Change Aspect Ratio',
        init: stream.aspectRatio != null,
        onChanged: (value) {
          stream.aspectRatio = value ? Rational.create4x3() : null;
        },
        builder: () {
          return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: AspectRatioField(stream.aspectRatio!));
        });
  }

  Widget _textOverlay() {
    return OptionalFieldTile(
        title: 'Text overlay',
        init: stream.textOverlay != null,
        onChanged: (value) {
          stream.textOverlay = value ? TextOverlay.createDefault() : null;
        },
        builder: () => TextOverlayField(stream.textOverlay!));
  }

  Widget _streamOverlay() {
    return OptionalFieldTile(
      title: 'Stream overlay',
      init: stream.streamOverlay != null,
      onChanged: (value) {
        stream.streamOverlay = value ? StreamOverlay.createDefault() : null;
      },
      builder: () {
        return _StreamOverlayWidget(stream.streamOverlay!);
      },
    );
  }

  Widget _backgroundEffects() {
    return OptionalFieldTile(
        title: 'Background effect',
        init: stream.backgroundEffect != null,
        onChanged: (value) {
          stream.backgroundEffect = value ? BlurBackgroundEffect() : null;
        },
        builder: () => BackgroundEffectField<S>(stream));
  }
}

class _FlipVideo<S extends EncodeStream> extends OptionalFieldTile {
  _FlipVideo(S stream)
      : super(
            title: 'Flip video',
            init: stream.videoFlip != null,
            onChanged: (value) {
              stream.videoFlip = value ? VideoFlip.DEFAULT : null;
            },
            builder: () {
              return NumberTextField.integer(
                  padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  canBeEmpty: false,
                  minInt: VideoFlip.MIN,
                  maxInt: VideoFlip.MAX,
                  hintText: 'Mode',
                  initInt: stream.videoFlip,
                  onFieldChangedInt: (term) {
                    stream.videoFlip = term;
                  });
            });
}

class _VideoBitrateWithConverter extends StatefulWidget {
  final EncodeStream stream;

  const _VideoBitrateWithConverter({required this.stream, Key? key}) : super(key: key);

  @override
  _VideoBitrateWithConverterState createState() => _VideoBitrateWithConverterState();
}

class _VideoBitrateWithConverterState extends State<_VideoBitrateWithConverter> {
  static const DEFAULT_BITRATE = 2048 * 1024;
  final _bitController = TextEditingController();
  final _kBitController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _bitController.addListener(onBitrateChange);
  }

  void onBitrateChange() {
    final _bitrate = double.tryParse(_bitController.text);

    if (_bitrate != null) {
      _kBitController.value =
          TextEditingValue(text: (widget.stream.videoBitRate! / 1024).toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return OptionalFieldTile(
        title: 'Change bitrate',
        init: widget.stream.videoBitRate != null,
        onChanged: (value) {
          widget.stream.videoBitRate = value ? DEFAULT_BITRATE : null;
        },
        builder: () {
          return Row(
            children: [
              Expanded(
                child: NumberTextField.integer(
                    padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                    hintText: 'Bitrate',
                    canBeEmpty: false,
                    textEditingController: _bitController,
                    minInt: 1,
                    initInt: widget.stream.videoBitRate,
                    onFieldChangedInt: (term) {
                      widget.stream.videoBitRate = term;
                      _bitController.value =
                          TextEditingValue(text: (widget.stream.videoBitRate! / 1024).toString());
                    }),
                flex: 4,
              ),
              Expanded(
                child: TextFieldEx(
                    readOnly: true,
                    controller: _kBitController
                      ..text = (widget.stream.videoBitRate! / 1024).toString(),
                    keyboardType: TextInputType.number,
                    decoration:
                        const InputDecoration(labelText: 'Kb/s', border: OutlineInputBorder())),
              ),
            ],
          );
        });
  }
}

class _AlphaMethod extends StatefulWidget {
  final AlphaMethod method;
  final ValueChanged<AlphaMethod> onChanged;

  const _AlphaMethod({Key? key, required this.method, required this.onChanged}) : super(key: key);

  @override
  _AlphaMethodState createState() => _AlphaMethodState();
}

class _AlphaMethodState extends State<_AlphaMethod> {
  late AlphaMethod _method;

  @override
  void initState() {
    _method = widget.method;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final types = [
      AlphaMethodType.SET,
      AlphaMethodType.GREEN,
      AlphaMethodType.BLUE,
      AlphaMethodType.CUSTOM
    ];

    return Column(children: [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: DropdownButtonFormField<AlphaMethodType>(
          value: _method.method,
          items: [
            for (var type in types)
              DropdownMenuItem(value: type, child: Text(type.toHumanReadable()))
          ],
          onChanged: (value) {
            if (value == AlphaMethodType.CUSTOM) {
              _update(() => _method = CustomAlphaMethod(), true);
            } else {
              _update(() => _method = CustomAlphaMethod(), true);
            }
          },
        ),
      ),
      NumberTextField.decimal(
        formatters: <TextInputFormatter>[TextFieldFilter.digits],
        hintText: 'Alpha',
        initDouble: _method.alpha,
        onFieldChangedDouble: (value) => _update(() => _method.alpha = value),
      ),
      if (_method is CustomAlphaMethod)
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: ColorPicker(
            initialColor: Color((_method as CustomAlphaMethod).hex),
            onChanged: (color) => _update(() => (_method as CustomAlphaMethod).hex = color.value),
          ),
        ),
    ]);
  }

  void _update(VoidCallback update, [bool rebuild = false]) {
    if (rebuild) {
      setState(update);
    } else {
      update();
    }

    widget.onChanged(_method);
  }
}
