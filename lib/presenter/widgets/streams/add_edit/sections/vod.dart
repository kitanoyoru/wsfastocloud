import 'dart:async';

import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_common/flutter_common.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/presenter/service_statistics/store/server_statistics_bloc/server_statistics_bloc.dart';
import 'package:wsfastocloud/presenter/widgets/data_picker.dart';
import 'package:wsfastocloud/presenter/widgets/scrollbar.dart';
import 'package:wsfastocloud/presenter/widgets/streams/stream_icon.dart';

class VodSection<S extends VodStream> extends StatefulWidget {
  const VodSection(
      this.stream, this.onVodDetailsChanged, this.defaultBackgroundUrl, this.defaultBannerUrl,
      {Key? key, required this.isSerial})
      : super(key: key);

  final S stream;
  final void Function(String name, String icon, String description) onVodDetailsChanged;
  final String defaultBackgroundUrl;
  final String defaultBannerUrl;
  final bool isSerial;

  @override
  _VodSectionState createState() => _VodSectionState();
}

class _VodSectionState extends State<VodSection> {
  final StreamController<String> _backgroundUpdates = StreamController<String>();

  @override
  void dispose() {
    _backgroundUpdates.close();
    super.dispose();
  }

  VodDetails? tmdb;
  List<VodDetails> tmdbResults = [];

  @override
  void initState() {
    super.initState();
    widget.stream.vodType = widget.isSerial ? VodType.SERIAL : VodType.VOD;
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      context.read<ServiceStatisticsBloc>().tmdbRequest(widget.stream.name).then((result) {
        setState(() {
          tmdbResults = result;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return ScrollbarEx(builder: (controller) {
      return ListView(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          controller: controller,
          children: [
            //   typeField(),
            _tmdbField(),
            _backgroundUrlField(),
            _trailerUrlField(),
            _userScoreField(),
            _primeDateField(),
            _countryField(),
            _durationField()
          ]);
    });
  }

  // Widget typeField() {
  //   return DropdownButtonEx<VodType>(
  //       hint: 'Movie type',
  //       value: widget.stream.vodType,
  //       values: VodType.values,
  //       onChanged: (type) {
  //         widget.stream.vodType = type;
  //       },
  //       itemBuilder: (VodType type) {
  //         return DropdownMenuItem(child: Text(type.toHumanReadable()), value: type);
  //       });
  // }

  Widget _backgroundUrlField() {
    return TextFieldEx(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        minSymbols: IconUrl.MIN_LENGTH,
        maxSymbols: IconUrl.MAX_LENGTH,
        hintText: 'Background URL',
        errorText: 'Enter background URL',
        init: widget.stream.backgroundUrl,
        decoration: InputDecoration(
            icon: StreamBuilder<String>(
                initialData: widget.stream.backgroundUrl,
                stream: _backgroundUpdates.stream,
                builder: (_, snapshot) {
                  return PreviewIcon.live(snapshot.data!, width: 60, height: 40);
                })),
        onFieldChanged: (term) {
          widget.stream.backgroundUrl = term;
          _backgroundUpdates.add(term);
        });
  }

  Widget _trailerUrlField() {
    return TextFieldEx(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        minSymbols: TrailerUrl.MIN_LENGTH,
        maxSymbols: TrailerUrl.MAX_LENGTH,
        hintText: 'Trailer URL',
        init: widget.stream.trailerUrl,
        onFieldChanged: (term) {
          widget.stream.trailerUrl = term;
        });
  }

  Widget _userScoreField() {
    return NumberTextField.decimal(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        minDouble: UserScore.MIN,
        maxDouble: UserScore.MAX,
        hintText: 'User score',
        canBeEmpty: false,
        initDouble: widget.stream.userScore,
        onFieldChangedDouble: (double? term) {
          widget.stream.userScore = term ?? 0;
        });
  }

  Widget _primeDateField() {
    return DatePicker('Premiere date', widget.stream.primeDate, (int date) {
      widget.stream.primeDate = date;
    });
  }

  Widget _countryField() {
    return TextFieldEx(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        maxSymbols: Country.MAX_LENGTH,
        formatters: <TextInputFormatter>[TextFieldFilter.root],
        hintText: 'Country',
        errorText: 'Enter country',
        init: widget.stream.country,
        onFieldChanged: (term) {
          widget.stream.country = term;
        });
  }

  Widget _durationField() {
    final int inSeconds = widget.stream.duration ~/ 1000;
    return NumberTextField.integer(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        minInt: VodDuration.MIN,
        maxInt: VodDuration.MAX,
        hintText: 'Duration (sec)',
        canBeEmpty: false,
        initInt: inSeconds,
        onFieldChangedInt: (term) {
          if (term != null) {
            widget.stream.duration = term * 1000;
          }
        });
  }

  Widget _tmdbField() {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(children: <Widget>[
          const SizedBox(width: 8),
          Expanded(
              child: DropdownButtonEx<VodDetails?>(
                  padding: const EdgeInsets.all(0),
                  hint: 'TMDB Results',
                  value: tmdb,
                  values: tmdbResults,
                  onChanged: (t) async {
                    if (t != null) {
                      await context
                          .read<ServiceStatisticsBloc>()
                          .tmdbSecondRequest(t.id)
                          .then((value) => setState(() {
                                tmdb = value;
                              }));
                      setState(() {
                        widget.stream.userScore = tmdb?.userScore ?? 0;
                        widget.stream.primeDate = tmdb?.primeDate ?? 0;
                        widget.stream.duration = tmdb?.duration ?? 0;
                        widget.stream.country = tmdb?.country ?? '';
                        widget.stream.backgroundUrl =
                            tmdb?.backgroundLogoUrl ?? widget.defaultBackgroundUrl;
                      });
                      widget.onVodDetailsChanged(
                          tmdb?.name ?? '', tmdb?.icon ?? '', tmdb?.description ?? '');
                    }
                  },
                  itemBuilder: (VodDetails? result) {
                    return DropdownMenuItem(child: Text(result!.name!), value: result);
                  })),
          IconButton(
              tooltip: 'Refresh',
              icon: const Icon(Icons.refresh),
              onPressed: () {
                context
                    .read<ServiceStatisticsBloc>()
                    .tmdbRequest(widget.stream.name)
                    .then((result) {
                  tmdbResults = result;
                  setState(() {});
                });
              })
        ]));
  }
}
