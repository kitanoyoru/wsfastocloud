import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/widgets.dart';

class ScanFolder {
  static const DIRECTORY_FIELD = 'directory';
  static const EXTENSIONS_FIELD = 'extensions';

  String directory;
  List<String> extensions;

  ScanFolder({required this.directory, required this.extensions});

  bool isValid() {
    return directory.isNotEmpty && extensions.isNotEmpty;
  }

  factory ScanFolder.fromJson(Map<String, dynamic> json) {
    final directory = json[DIRECTORY_FIELD];
    final extensions = json[EXTENSIONS_FIELD];
    return ScanFolder(directory: directory, extensions: extensions);
  }

  Map<String, dynamic> toJson() {
    return {DIRECTORY_FIELD: directory, EXTENSIONS_FIELD: extensions};
  }
}

class StreamLinkField extends StatelessWidget {
  final PyFastoStream init;

  const StreamLinkField(this.init);

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_preferFiled(), _httpProxyFiled(), _httpsProxyFiled()]);
  }

  Widget _httpProxyFiled() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'Http proxy',
        init: init.httpProxy,
        onFieldChanged: (val) {
          init.httpProxy = val;
        });
  }

  Widget _httpsProxyFiled() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'Https proxy',
        init: init.httpsProxy,
        onFieldChanged: (val) {
          init.httpsProxy = val;
        });
  }

  Widget _preferFiled() {
    return DropdownButtonExTypes<int, QualityPrefer>(
        hint: 'Prefer type',
        value: init.prefer.toInt(),
        values: QualityPrefer.values,
        onChanged: (c) {
          init.prefer = QualityPrefer.fromInt(c);
        },
        itemBuilder: (QualityPrefer value) {
          return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value.toInt());
        });
  }
}

class AesBits {
  final int _value;

  const AesBits._(this._value);

  int toInt() {
    return _value;
  }

  String toHumanReadable() {
    if (_value == 12) {
      return 'AES-128';
    } else if (_value == 24) {
      return 'AES-192';
    }
    return 'AES-265';
  }

  factory AesBits.fromInt(int value) {
    if (value == 12) {
      return AesBits.AES128;
    } else if (value == 24) {
      return AesBits.AES192;
    }
    return AesBits.AES256;
  }

  static List<AesBits> get values => [AES128, AES192, AES256];

  static const AesBits AES128 = AesBits._(12);
  static const AesBits AES192 = AesBits._(24);
  static const AesBits AES256 = AesBits._(32);
}

class SrtKeyField extends StatelessWidget {
  final SrtKey init;

  const SrtKeyField(this.init);

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_passFiled(), _keyLenFiled()]);
  }

  Widget _passFiled() {
    return PassWordTextField(
        hintText: 'Passphrase',
        errorText: 'Enter a passphrase',
        init: init.passphrase,
        onFieldChanged: (term) {
          init.passphrase = term;
        });
  }

  Widget _keyLenFiled() {
    return DropdownButtonExTypes<int, AesBits>(
        hint: 'Encryption type',
        value: init.keyLen,
        values: AesBits.values,
        onChanged: (c) {
          init.keyLen = c;
        },
        itemBuilder: (AesBits value) {
          return DropdownMenuItem(child: Text(value.toHumanReadable()), value: value.toInt());
        });
  }
}

class KVSPropField extends StatelessWidget {
  final KVSProp init;

  const KVSPropField(this.init);

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _streamNameFiled(),
          _accessKeyFiled(),
          _secFiled(),
          _awsFiled(),
          _storageSizeField()
        ]);
  }

  Widget _streamNameFiled() {
    return TextFieldEx(
        hintText: 'Stream name',
        errorText: 'Enter a stream name',
        init: init.streamName,
        onFieldChanged: (val) {
          init.streamName = val;
        });
  }

  Widget _accessKeyFiled() {
    return TextFieldEx(
        hintText: 'Access key',
        errorText: 'Enter a access key',
        init: init.accessKey,
        onFieldChanged: (val) {
          init.accessKey = val;
        });
  }

  Widget _secFiled() {
    return PassWordTextField(
        hintText: 'Secret key',
        errorText: 'Enter a secret key',
        init: init.secretKey,
        onFieldChanged: (term) {
          init.secretKey = term;
        });
  }

  Widget _awsFiled() {
    return TextFieldEx(
        hintText: 'AWS region',
        errorText: 'Enter a AWS region',
        init: init.awsRegion,
        onFieldChanged: (val) {
          init.awsRegion = val;
        });
  }

  Widget _storageSizeField() {
    return NumberTextField.integer(
        hintText: 'Storage size',
        canBeEmpty: false,
        initInt: init.storageSize,
        onFieldChangedInt: (term) {
          if (term != null) init.storageSize = term;
        });
  }
}

class AzurePropField extends StatelessWidget {
  final AzureProp init;

  const AzurePropField(this.init);

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_accountNameFiled(), _accountKeyFiled(), _locationFiled()]);
  }

  Widget _accountNameFiled() {
    return TextFieldEx(
        hintText: 'Account name',
        errorText: 'Enter a account name',
        init: init.accountName,
        onFieldChanged: (val) {
          init.accountName = val;
        });
  }

  Widget _accountKeyFiled() {
    return TextFieldEx(
        hintText: 'Account key',
        errorText: 'Enter a account key',
        init: init.accountKey,
        onFieldChanged: (val) {
          init.accountKey = val;
        });
  }

  Widget _locationFiled() {
    return TextFieldEx(
        hintText: 'Location',
        errorText: 'Enter container location',
        init: init.location,
        onFieldChanged: (term) {
          init.location = term;
        });
  }
}

class GooglePropField extends StatelessWidget {
  final GoogleProp init;

  const GooglePropField(this.init);

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[_accountCredsFiled()]);
  }

  Widget _accountCredsFiled() {
    return TextFieldEx(
        hintText: 'Account credentials',
        errorText: 'Enter a account credentials',
        init: init.accountCreds,
        onFieldChanged: (val) {
          init.accountCreds = val;
        });
  }
}
