import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/widgets.dart';
import 'package:wsfastocloud/presenter/widgets/optional.dart';
import 'package:wsfastocloud/presenter/widgets/urls/base.dart';
import 'package:wsfastocloud/presenter/widgets/urls/scan_folder_dialog.dart';

class InputUrlField<T extends InputUrl> extends StatefulWidget {
  final T init;
  final void Function(T url)? onTest;

  const InputUrlField(this.init, this.onTest);

  @override
  _InputFieldState createState() => _InputFieldState<T, InputUrlField<T>>();
}

class _InputFieldState<T extends InputUrl, S extends InputUrlField<T>> extends State<S> {
  T get _inputUrl => widget.init;

  @override
  Widget build(BuildContext context) {
    return uriRow();
  }

  Widget uriRow() {
    return Row(
        mainAxisSize: MainAxisSize.min, children: [Expanded(child: uriField()), testButton()]);
  }

  Widget uriField() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'URL',
        errorText: 'Enter URL',
        init: widget.init.uri,
        onFieldChanged: (term) {
          _inputUrl.uri = term;
        });
  }

  Widget testButton() {
    return FlatButtonEx.filled(
        text: 'Test',
        onPressed: widget.onTest != null
            ? () {
                widget.onTest?.call(_inputUrl);
              }
            : null);
  }
}

class HttpInputUrlField extends InputUrlField<HttpInputUrl> {
  final bool isEncode;

  const HttpInputUrlField(HttpInputUrl uri, void Function(InputUrl url)? onTest, this.isEncode)
      : super(uri, onTest);

  @override
  _HttpInputFieldState createState() {
    return _HttpInputFieldState();
  }
}

class _HttpInputFieldState extends _InputFieldState<HttpInputUrl, HttpInputUrlField> {
  @override
  Widget build(BuildContext context) {
    final bool isWpe = _inputUrl.wpe != null;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        uriRow(),
        if (!isWpe) ...[
          _userAgentField(),
          _streamLinkField(),
          _httpProxy(),
        ],
        if (widget.isEncode) _wpeField()
      ],
    );
  }

  Widget _httpProxy() {
    return OptionalFieldTile(
        title: 'HTTP proxy',
        init: _inputUrl.proxy != null,
        onChanged: (checked) {
          _inputUrl.proxy = checked ? 'http://username:password@proxy.com:8080' : null;
        },
        builder: () {
          return TextFieldEx(
              formatters: <TextInputFormatter>[TextFieldFilter.url],
              hintText: 'Http proxy',
              init: _inputUrl.proxy,
              onFieldChanged: (val) {
                _inputUrl.proxy = val;
              });
        });
  }

  Widget _userAgentField() {
    return DropdownButtonEx<UserAgent?>(
        hint: 'User agent',
        value: _inputUrl.userAgent,
        values: UserAgent.values,
        onChanged: (c) {
          setState(() {
            _inputUrl.userAgent = c;
          });
        },
        itemBuilder: (UserAgent? value) {
          return DropdownMenuItem(child: Text(value!.toHumanReadable()), value: value);
        });
  }

  Widget _streamLinkField() {
    return OptionalFieldTile(
        title: 'Streamlink',
        init: _inputUrl.streamLink != null,
        onChanged: (val) {
          _inputUrl.streamLink = val ? PyFastoStream() : null;
        },
        builder: () => StreamLinkField(_inputUrl.streamLink!));
  }

  Widget _wpeField() {
    final bool isWpe = _inputUrl.wpe != null;
    return CheckboxListTile(
      title: const Text('WPE'),
      value: isWpe,
      onChanged: (value) => setState(() {
        _inputUrl.wpe = value == true ? Wpe(gl: false) : null;
        if (value ?? false) {
          _inputUrl.userAgent = null;
          _inputUrl.proxy = null;
          _inputUrl.streamLink = null;
        }
      }),
    );
  }
}

class UdpInputUrlField extends InputUrlField<UdpInputUrl> {
  const UdpInputUrlField(UdpInputUrl uri, void Function(InputUrl url)? onTest) : super(uri, onTest);

  @override
  _UdpInputFieldState createState() {
    return _UdpInputFieldState();
  }
}

class _UdpInputFieldState extends _InputFieldState<UdpInputUrl, UdpInputUrlField> {
  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[uriRow(), _ifaceFiled(), _programNumberFiled()]);
  }

  Widget _ifaceFiled() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'Multicast iface',
        init: _inputUrl.multicastIface,
        onFieldChanged: (term) {
          if (term.isEmpty) {
            _inputUrl.multicastIface = null;
            return;
          }
          _inputUrl.multicastIface = term;
        });
  }

  Widget _programNumberFiled() {
    return NumberTextField.integer(
        hintText: 'Program Number',
        canBeEmpty: false,
        initInt: _inputUrl.programNumber,
        onFieldChangedInt: (term) {
          _inputUrl.programNumber = term;
        });
  }
}

class RtmpInputUrlField extends InputUrlField<RtmpInputUrl> {
  const RtmpInputUrlField(RtmpInputUrl uri, void Function(InputUrl url)? onTest)
      : super(uri, onTest);

  @override
  _RtmpInputFieldState createState() {
    return _RtmpInputFieldState();
  }
}

class _RtmpInputFieldState extends _InputFieldState<RtmpInputUrl, RtmpInputUrlField> {
  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[uriRow(), _RtmpSrcType(_inputUrl)]);
  }
}

class _RtmpSrcType extends DropdownButtonEx<RtmpSrcType?> {
  _RtmpSrcType(RtmpInputUrl value)
      : super(
            hint: 'RTMP Src type',
            value: value.rtmpSrcType,
            values: RtmpSrcType.values,
            onChanged: (c) {
              value.rtmpSrcType = c;
            },
            itemBuilder: (RtmpSrcType? value) {
              return DropdownMenuItem(child: Text(value!.toHumanReadable()), value: value);
            });
}

class SrtInputUrlField extends InputUrlField<SrtInputUrl> {
  const SrtInputUrlField(SrtInputUrl uri, void Function(InputUrl url)? onTest) : super(uri, onTest);

  @override
  _SrtInputFieldState createState() {
    return _SrtInputFieldState();
  }
}

class _SrtInputFieldState extends _InputFieldState<SrtInputUrl, SrtInputUrlField> {
  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[uriRow(), _modeField(), _srtKeyField()]);
  }

  Widget _modeField() {
    return DropdownButtonEx<SrtMode?>(
        hint: 'SRT mode',
        value: _inputUrl.mode,
        values: SrtMode.values,
        onChanged: (c) {
          _inputUrl.mode = c;
        },
        itemBuilder: (SrtMode? value) {
          return DropdownMenuItem(child: Text(value!.toHumanReadable()), value: value);
        });
  }

  Widget _srtKeyField() {
    return OptionalFieldTile(
        title: 'Key',
        init: _inputUrl.srtKey != null,
        onChanged: (val) {
          _inputUrl.srtKey = val ? SrtKey('', 32) : null;
        },
        builder: () => SrtKeyField(_inputUrl.srtKey!));
  }
}

class FileInputUrlField extends InputUrlField<FileInputUrl> {
  final bool canScan;

  const FileInputUrlField(FileInputUrl uri, void Function(InputUrl url)? onTest, this.canScan)
      : super(uri, onTest);

  @override
  _FileInputFieldState createState() {
    return _FileInputFieldState();
  }
}

class _FileInputFieldState extends _InputFieldState<FileInputUrl, FileInputUrlField> {
  late final TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController(text: _inputUrl.uri);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget uriRow() {
    return Row(mainAxisSize: MainAxisSize.min, children: [
      Expanded(child: uriField()),
      testButton(),
      const SizedBox(width: 4),
      if (widget.canScan) FlatButtonEx.filled(text: 'Scan folder', onPressed: _onScan)
    ]);
  }

  @override
  Widget uriField() {
    return TextFieldEx(
        controller: _controller,
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'File',
        errorText: 'Enter file path',
        init: _inputUrl.uri,
        onFieldChanged: (term) {
          _inputUrl.uri = term;
        });
  }

// private:
  void _onScan() {
    showDialog(
        context: context,
        builder: (context) {
          return ScanFolderDialog();
        }).then((path) {
      if (path == null) {
        return;
      }

      String _fileConversion(String file) {
        return 'file://$file';
      }

      path = _fileConversion(path);

      setState(() {
        _inputUrl.uri = path;
        _controller.text = path;
      });
    });
  }
}
