import 'package:dart_ipify/dart_ipify.dart';
import 'package:fastocloud_dart_models/models.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_common/widgets.dart';
import 'package:pubsub_common/models.dart';
import 'package:pubsub_common/widgets.dart';
import 'package:wsfastocloud/presenter/widgets/optional.dart';
import 'package:wsfastocloud/presenter/widgets/urls/base.dart';
import 'package:wsfastocloud/presenter/widgets/urls/scan_folder_dialog.dart';

typedef Future<String> GetTokenCallback(String host);

class OutputUrlField<T extends OutputUrl> extends StatefulWidget {
  final T init;
  final void Function(T url)? onTest;

  const OutputUrlField(this.init, this.onTest);

  @override
  _OutputFieldState createState() => _OutputFieldState<T, OutputUrlField<T>>();
}

class _OutputFieldState<T extends OutputUrl, S extends OutputUrlField<T>> extends State<S> {
  T get value => widget.init;

  @override
  Widget build(BuildContext context) {
    return _UriField<T>(value, widget.onTest, false);
  }
}

class HttpOutputUrlField extends OutputUrlField<HttpOutputUrl> {
  const HttpOutputUrlField(HttpOutputUrl uri, void Function(OutputUrl url)? onTest)
      : super(uri, onTest);

  @override
  _HttpOutputFieldState createState() => _HttpOutputFieldState();
}

class _HttpOutputFieldState extends _OutputFieldState<HttpOutputUrl, HttpOutputUrlField> {}

class UdpOutputUrlField extends OutputUrlField<UdpOutputUrl> {
  const UdpOutputUrlField(UdpOutputUrl uri, void Function(OutputUrl url)? onTest)
      : super(uri, onTest);

  @override
  _UdpOutputFieldState createState() => _UdpOutputFieldState();
}

class _UdpOutputFieldState extends _OutputFieldState<UdpOutputUrl, UdpOutputUrlField> {
  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisSize: MainAxisSize.min,
        children: [_UriField<UdpOutputUrl>(value, widget.onTest, false), _ifaceFiled()]);
  }

  Widget _ifaceFiled() {
    return TextFieldEx(
        formatters: <TextInputFormatter>[TextFieldFilter.url],
        hintText: 'Multicast iface',
        init: value.multicastIface,
        onFieldChanged: (term) {
          if (term.isEmpty) {
            value.multicastIface = null;
            return;
          }
          value.multicastIface = term;
        });
  }
}

class SrtOutputUrlField extends OutputUrlField<SrtOutputUrl> {
  const SrtOutputUrlField(SrtOutputUrl uri, void Function(OutputUrl url)? onTest)
      : super(uri, onTest);

  @override
  _SrtOutputFieldState createState() => _SrtOutputFieldState();
}

class _SrtOutputFieldState extends _OutputFieldState<SrtOutputUrl, SrtOutputUrlField> {
  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      _UriField<SrtOutputUrl>(value, widget.onTest, false),
      DropdownButtonEx<SrtMode?>(
          hint: 'SRT mode',
          value: value.mode,
          values: SrtMode.values,
          onChanged: (c) {
            value.mode = c;
          },
          itemBuilder: (SrtMode? value) {
            return DropdownMenuItem(child: Text(value!.toHumanReadable()), value: value);
          }),
      OptionalFieldTile(
          title: 'Key',
          init: value.srtKey != null,
          onChanged: (val) {
            value.srtKey = val ? SrtKey('', 32) : null;
          },
          builder: () => SrtKeyField(value.srtKey!))
    ]);
  }
}

class GoogleOutputUrlField extends OutputUrlField<GoogleOutputUrl> {
  const GoogleOutputUrlField(GoogleOutputUrl uri, void Function(OutputUrl url)? onTest)
      : super(uri, onTest);

  @override
  _GoogleOutputFieldState createState() => _GoogleOutputFieldState();
}

class _GoogleOutputFieldState extends _OutputFieldState<GoogleOutputUrl, GoogleOutputUrlField> {
  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      _UriField<GoogleOutputUrl>(value, widget.onTest, false),
      GooglePropField(value.google)
    ]);
  }
}

class UnknownOutputUrlField extends OutputUrlField<UnknownOutputUrl> {
  const UnknownOutputUrlField(UnknownOutputUrl uri, void Function(OutputUrl url)? onTest)
      : super(uri, onTest);

  @override
  _UnknownOutputFieldState createState() => _UnknownOutputFieldState();
}

class _UnknownOutputFieldState extends _OutputFieldState<UnknownOutputUrl, UnknownOutputUrlField> {
  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      _UriField<UnknownOutputUrl>(value, widget.onTest, false),
      OptionalFieldTile(
          title: 'KVS',
          init: value.kvs != null,
          onChanged: (val) {
            value.kvs = val ? KVSProp.createDefault() : null;
          },
          builder: () => KVSPropField(value.kvs!)),
      OptionalFieldTile(
          title: 'Azure',
          init: value.azure != null,
          onChanged: (val) {
            value.azure = val ? AzureProp.createDefault() : null;
          },
          builder: () => AzurePropField(value.azure!))
    ]);
  }
}

class RtmpOutputUrlField extends OutputUrlField<RtmpOutputUrl> {
  const RtmpOutputUrlField(RtmpOutputUrl uri, void Function(OutputUrl url)? onTest)
      : super(uri, onTest);

  @override
  _RtmpOutputFieldState createState() => _RtmpOutputFieldState();
}

class _RtmpOutputFieldState extends _OutputFieldState<RtmpOutputUrl, RtmpOutputUrlField> {
  late final TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController(text: value.uri);
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
        Expanded(child: _UriField<RtmpOutputUrl>(value, widget.onTest, false, _controller)),
        IconButton(tooltip: 'Choose template', icon: const Icon(Icons.source), onPressed: addUrl)
      ]),
      _RtmpSinkType(value)
    ]);
  }

  void addUrl() async {
    final IRtmpOutputUrl? output = await showDialog<IRtmpOutputUrl>(
        context: context,
        builder: (context) {
          return IRtmpPickerDialog(value.id);
        });
    if (output != null) {
      showDialog<EditResult<IRtmpOutputUrl>>(
          context: context,
          builder: (context) {
            return RtmpOutputDialog.add(output: output);
          }).then((result) {
        if (result!.action == EditType.SAVE) {
          setState(() {
            _controller.text = result.value.uri;
            value.uri = result.value.uri;
          });
        }
      });
    }
  }
}

class HLSOutputUrlField extends OutputUrlField<HttpOutputUrl> {
  final String? proxyDirectory;
  final String Function(String file)? onPrepareOutputTemplate;
  final bool isExternal;
  final GetTokenCallback? onGetToken;

  const HLSOutputUrlField(HttpOutputUrl uri, void Function(OutputUrl url)? onTest, this.onGetToken,
      {this.proxyDirectory, this.onPrepareOutputTemplate, this.isExternal = false})
      : super(uri, onTest);

  @override
  _HLSOutputUrlFieldState createState() {
    return _HLSOutputUrlFieldState();
  }
}

class _HLSOutputUrlFieldState<T extends HLSOutputUrlField>
    extends _OutputFieldState<HttpOutputUrl, T> {
  late final TextEditingController _controller;

  bool get canScan => (widget.proxyDirectory != null);

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController(text: value.uri);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isExternal) {
      return _UriFieldScan(value, widget.onTest, canScan ? scan : null, null, _controller, false);
    }

    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      _UriFieldScan(
          value, widget.onTest, canScan ? scan : null, widget.onGetToken, _controller, false),
      _ChunkDuration(value),
      _HttpPlaylist(value),
      _HlsSinkType(value),
      _HlsType(value)
    ]);
  }

  void scan() {
    showDialog(
        context: context,
        builder: (context) {
          return ScanFolderDialog(path: widget.proxyDirectory!, visiblePath: false);
        }).then((path) {
      if (path == null) {
        return;
      }

      if (widget.onPrepareOutputTemplate != null) {
        path = widget.onPrepareOutputTemplate!.call(path);
      }

      setState(() {
        value.uri = path;
        _controller.text = path;
      });
    });
  }
}

class GoHttpOutputUrlField extends HLSOutputUrlField {
  const GoHttpOutputUrlField(
      HttpOutputUrl uri, void Function(OutputUrl url)? onTest, GetTokenCallback onGetToken)
      : super(uri, onTest, onGetToken, isExternal: false);

  @override
  _GoHttpOutputUrlFieldState createState() {
    return _GoHttpOutputUrlFieldState();
  }
}

class _GoHttpOutputUrlFieldState<T extends GoHttpOutputUrlField>
    extends _HLSOutputUrlFieldState<T> {
  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      _UriFieldScan(value, widget.onTest, null, widget.onGetToken, _controller, false),
      _ChunkDuration(value),
      _HttpPlaylist(value),
      _HlsSinkType(value),
      _HlsType(value)
    ]);
  }
}

class _UriField<O extends OutputUrl> extends StatefulWidget {
  final O output;
  final void Function(O url)? onTest;
  final TextEditingController? controller;
  final bool readOnly;

  const _UriField(this.output, this.onTest, this.readOnly, [this.controller]);

  @override
  _UriFieldState createState() {
    return _UriFieldState();
  }
}

class _UriFieldState<T extends _UriField> extends State<T> {
  @override
  Widget build(BuildContext context) {
    return Row(mainAxisSize: MainAxisSize.min, children: [
      Expanded(
          child: TextFieldEx(
              readOnly: widget.readOnly,
              controller: widget.controller,
              formatters: <TextInputFormatter>[TextFieldFilter.url],
              hintText: 'URL',
              errorText: 'Enter URL',
              init: widget.output.uri,
              onFieldChanged: (term) {
                widget.output.uri = term;
              })),
      FlatButtonEx.filled(
          text: 'Test',
          onPressed: widget.onTest != null
              ? () {
                  widget.onTest!.call(widget.output);
                }
              : null)
    ]);
  }
}

class _UriFieldScan extends _UriField<HttpOutputUrl> {
  final VoidCallback? onScan;
  final GetTokenCallback? onGetToken;

  _UriFieldScan(HttpOutputUrl output, void Function(HttpOutputUrl url)? onTest, this.onScan,
      this.onGetToken, TextEditingController controller, bool readOnly)
      : super(output, onTest, readOnly, controller);

  @override
  _UriFieldScanState createState() {
    return _UriFieldScanState();
  }
}

class _UriFieldScanState extends _UriFieldState<_UriFieldScan> {
  String? tokenResp;
  final TextEditingController _textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final children = [
      Row(mainAxisSize: MainAxisSize.min, children: [
        Expanded(child: super.build(context)),
        if (widget.onScan != null) const SizedBox(width: 4),
        if (widget.onScan != null) FlatButtonEx.filled(text: 'Scan', onPressed: widget.onScan)
      ])
    ];
    if (widget.onGetToken != null) {
      final initToken = widget.output.token != null ? widget.output.token! : false;
      final cb = initToken
          ? () async {
              final ip = await Ipify.ipv4();
              _textController.text = ip;
              showDialog(
                  context: context,
                  builder: (context) {
                    return _getTokenDialog(context);
                  });
            }
          : null;
      final token = Row(children: [
        Expanded(
            flex: 4,
            child: StateCheckBox(
                title: 'Token',
                init: initToken,
                onChanged: (bool res) {
                  setState(() {
                    widget.output.token = res;
                  });
                })),
        Expanded(child: FlatButtonEx.filled(text: 'Get token', onPressed: cb))
      ]);
      children.add(token);
    }
    return Column(children: children);
  }

  Widget _getTokenDialog(BuildContext context) {
    return SimpleDialog(title: Text('Add IP address'), children: [
      Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(children: [
            TextFieldEx(
                validator: (String text) {
                  return HostAndPort.isValidHost(text) ? null : 'Invalid IP address';
                },
                decoration:
                    InputDecoration(border: OutlineInputBorder(), labelText: 'Input IP Address'),
                controller: _textController),
            SizedBox(height: 5.0),
            if (tokenResp != null) ...{
              Row(mainAxisSize: MainAxisSize.min, children: [
                Flexible(
                    child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                        child: Text('${widget.output.uri}?token=$tokenResp'))),
                FlatButtonEx.filled(
                    text: 'Copy uri',
                    onPressed: () {
                      Clipboard.setData(
                          ClipboardData(text: '${widget.output.uri}?token=$tokenResp'));
                      ScaffoldMessenger.of(context)
                          .showSnackBar(const SnackBar(content: Text('Copied')));
                    })
              ])
            },
            Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
              FlatButtonEx.filled(
                text: 'Close',
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              FlatButtonEx.filled(
                  text: 'Get token',
                  onPressed: () {
                    final host = _textController.text;
                    if (HostAndPort.isValidHost(host)) {
                      setState(() async {
                        tokenResp = await widget.onGetToken!.call(host);
                      });
                    }
                  })
            ])
          ]))
    ]);
  }
}

class _ChunkDuration extends OptionalFieldTile {
  _ChunkDuration(HttpOutputUrl value)
      : super(
            title: 'Chunk duration',
            init: value.chunkDuration != null,
            onChanged: (checked) {
              value.chunkDuration = checked ? 10 : null;
            },
            builder: () {
              return TextFieldEx(
                  formatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.allow(RegExp(r'\d+'))
                  ],
                  hintText: 'Chunk duration',
                  keyboardType: const TextInputType.numberWithOptions(),
                  errorText: 'Input chunk duration',
                  init: value.chunkDuration.toString(),
                  onFieldChanged: (val) {
                    value.chunkDuration = int.tryParse(val);
                  });
            });
}

class _HttpPlaylist extends OptionalFieldTile {
  _HttpPlaylist(HttpOutputUrl value)
      : super(
            title: 'Playlist URL',
            init: value.playlistRoot != null,
            onChanged: (checked) {
              value.playlistRoot = checked ? 'http://0.0.0.0/fastocloud/hls' : null;
            },
            builder: () {
              return TextFieldEx(
                  formatters: <TextInputFormatter>[TextFieldFilter.url],
                  hintText: 'Playlist URL',
                  errorText: 'Input playlist URL',
                  init: value.playlistRoot,
                  onFieldChanged: (val) {
                    value.playlistRoot = val;
                  });
            });
}

class _HlsSinkType extends DropdownButtonEx<HlsSinkType?> {
  _HlsSinkType(HttpOutputUrl value)
      : super(
            hint: 'HLS Sink type',
            value: value.hlsSinkType,
            values: HlsSinkType.values,
            onChanged: (c) {
              value.hlsSinkType = c;
            },
            itemBuilder: (HlsSinkType? value) {
              return DropdownMenuItem(child: Text(value!.toHumanReadable()), value: value);
            });
}

class _HlsType extends DropdownButtonEx<HlsType?> {
  _HlsType(HttpOutputUrl value)
      : super(
            hint: 'HLS type',
            value: value.hlsType,
            values: HlsType.values,
            onChanged: (c) {
              value.hlsType = c;
            },
            itemBuilder: (HlsType? value) {
              return DropdownMenuItem(child: Text(value!.toHumanReadable()), value: value);
            });
}

class _RtmpSinkType extends DropdownButtonEx<RtmpSinkType?> {
  _RtmpSinkType(RtmpOutputUrl value)
      : super(
            hint: 'RTMP Sink type',
            value: value.rtmpSinkType,
            values: RtmpSinkType.values,
            onChanged: (c) {
              value.rtmpSinkType = c;
            },
            itemBuilder: (RtmpSinkType? value) {
              return DropdownMenuItem(child: Text(value!.toHumanReadable()), value: value);
            });
}
