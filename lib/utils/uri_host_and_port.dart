class UriHostAndPort {
  UriHostAndPort();

  String hostAndPort() {
    final scheme = Uri.base.scheme;
    final host = Uri.base.host;
    final port = Uri.base.port;
    if (Uri.base.hasPort) {
      return '$scheme://$host:$port';
    } else {
      return '$scheme://$host';
    }
  }
}
