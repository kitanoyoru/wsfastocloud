#!/usr/bin/env python3

import argparse
import os
import subprocess
import sys
import tarfile

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

BUNDLE_OUT_PATH = 'build/web'

PROJECT_NAME = 'branding'


def make_tarfile(output_filename, source_dir):
    with tarfile.open(output_filename, 'w:gz') as tar:
        tar.add(source_dir, arcname='wsfastocloud_build')


if __name__ == '__main__':
    name = 'last'

    parser = argparse.ArgumentParser(prog=PROJECT_NAME, usage='%(prog)s [options]')
    parser.add_argument('--name', help='name for archive (default: {0})'.format(name), default=name)
    parser.add_argument('--archive', help='whether or not you need to archive the build', default=False)
    argv = parser.parse_args()

    arg_name = argv.name
    arg_archive = argv.archive

    build_project_line = ['flutter', 'build', 'web']
    result = subprocess.call(build_project_line)
    print('Command: {0}, finished with return code: {1}'.format(build_project_line, result))

    if arg_archive:
        tar_path = '{0}.tar.gz'.format(arg_name)
        make_tarfile(tar_path, BUNDLE_OUT_PATH)
        print('Archive ready path: {0}'.format(tar_path))
